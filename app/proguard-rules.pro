# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /var/local/android-sdk-linux/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}


-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgent
-keep public class * extends android.preference.Preference
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.support.v4.app.DialogFragment
-keep public class * extends com.actionbarsherlock.app.SherlockListFragment
-keep public class * extends com.actionbarsherlock.app.SherlockFragment
-keep public class * extends com.actionbarsherlock.app.SherlockFragmentActivity
-keep public class * extends android.app.Fragment
-keep public class com.android.vending.licensing.ILicensingService


-keepclasseswithmembernames class * {
 native <methods>;
}

-keep public class * extends android.view.View {
 public <init>(android.content.Context);
 public <init>(android.content.Context, android.util.AttributeSet);
 public <init>(android.content.Context, android.util.AttributeSet, int);
 public void set*(...);
}


-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep class com.actionbarsherlock.** { *; }
-keep interface com.actionbarsherlock.** { *; }

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version. We know about them, and they are safe.
-dontwarn android.support.**
-dontwarn com.google.ads.**

-keep class com.onesignal.** { *; }


-keep class com.pavelsikun.** { *; }
-keep class com.pavelsikun.vintagechroma.ChromaPreferenceCompat.** { *; }
-keep public class com.pavelsikun.vintagechroma.** { *; }
-dontwarn com.pavelsikun.**


-keep class com.partodesign.onlineparde.activities.AppPlatform{ *; }
-keep class com.partodesign.onlineparde.activities.AppPlatform$UserInfoModel.**  { *; }
-keep class com.partodesign.onlineparde.activities.AppPlatform$UserInfoModel{ *; }
-keep interface com.partodesign.onlineparde.activities.APIService.** { *; }
-keep class * extends com.partodesign.onlineparde.activities.AppPlatform.*{ *; }
-keep class * extends com.partodesign.onlineparde.activities.AppPlatform$UserInfoModel.*{ *; }


-keep public class com.partodesign.onlineparde.activities.AppPlatform.**  { *; }
-keep public class com.partodesign.onlineparde.activities.AppPlatform { *; }
-keep public class com.partodesign.onlineparde.activities.AppPlatform$UserInfoModel.**  { *; }
-keep public class com.partodesign.onlineparde.activities.AppPlatform$UserInfoModel{ *; }
-keep public interface com.partodesign.onlineparde.activiteis.APIService.** { *; }
-keep public class * extends com.partodesign.onlineparde.activities.AppPlatform.*{ *; }
-keep public class * extends com.partodesign.onlineparde.activities.AppPlatform$UserInfoModel.*{ *; }


-keep class com.stephentuso.** { *; }
-keep class com.stephentuso.welcome.** { *; }
-keep class com.stephentuso.welcome.WelcomePreferenceFragment.** { *; }
-dontwarn com.stephentuso.**


-keepclassmembers class * extends com.stephentuso.welcome.WelcomeActivity {
    public static java.lang.String welcomeKey();
}



# Ignore warnings: We are not using DOM model
-dontwarn com.fasterxml.jackson.databind.ext.DOMSerializer
# Ignore warnings: https://github.com/square/okhttp/wiki/FAQs
-dontwarn com.squareup.okhttp.internal.huc.**
# Ignore warnings: https://github.com/square/okio/issues/60
-dontwarn okio.**
# Ignore warnings: https://github.com/square/retrofit/issues/435
-dontwarn com.google.appengine.api.urlfetch.**

# Keep the pojos used by GSON or Jackson
-keep class com.futurice.project.models.pojo.** { *; }

# Keep GSON stuff
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.** { *; }

# Keep Jackson stuff
-keep class org.codehaus.** { *; }
-keep class com.fasterxml.jackson.annotation.** { *; }

# Keep these for GSON and Jackson
-keepattributes Signature
-keepattributes *Annotation*
-keepattributes EnclosingMethod



# Keep Picasso
-keep class com.squareup.picasso.** { *; }
-keepclasseswithmembers class * {
    @com.squareup.picasso.** *;
}
-keepclassmembers class * {
    @com.squareup.picasso.** *;
}


# Keep Retrofit
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.** *;
}
-keepclassmembers class * {
    @retrofit.** *;
}
-dontwarn rx.**
-dontwarn okio.**
-dontwarn com.squareup.okhttp.**
-dontwarn com.squareup.okhttps.**
-keep class com.squareup.okhttp.** { *; }
-keep class com.squareup.okhttps.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttps.** { *; }

-dontwarn android.support.v7.**
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }

-dontwarn com.parse.**
-keep class com.parse.** { *; }

-dontwarn com.daimajia.**
-keep class com.daimajia.** { *; }

-dontwarn com.github.**
-keep class com.github.** { *; }


-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}
-keepclasseswithmembers class * {
    @retrofit2.https.* <methods>;
}
-keepattributes *Annotation*
-keepattributes Signature
-keep class okhttp3.** { *; }
-keep class okhttps3.** { *; }
-keep interface okhttp3.** { *; }
-keep interface okhttps3.** { *; }
-dontwarn okhttp3.**
-dontwarn okhttps3.**
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions
-dontwarn retrofit.**

-dontwarn javax.annotation.**

-dontwarn retrofit2.**
-dontwarn org.codehaus.mojo.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions
-keepattributes *Annotation*

-keepattributes RuntimeVisibleAnnotations
-keepattributes RuntimeInvisibleAnnotations
-keepattributes RuntimeVisibleParameterAnnotations
-keepattributes RuntimeInvisibleParameterAnnotations

-keepattributes EnclosingMethod

-keepclasseswithmembers class * {
    @retrofit2.* <methods>;
}

-keepclasseswithmembers interface * {
    @retrofit2.* <methods>;
}
-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}
-keepclasseswithmembers class * {
    @retrofit2.https.* <methods>;
}
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions



-keep class cn.pedant.SweetAlert.Rotate3dAnimation {
  public <init>(...);
}


-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}





-dontwarn org.apache.http.**
-dontwarn android.net.http.AndroidHttpClient
-dontwarn com.google.android.gms.**
-dontwarn com.android.volley.toolbox.**

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}


