package com.supectco.easyvisa.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class JSONParser {

    static InputStream is         = null;
    static JSONObject jObj       = null;
    static String json       = "";
    public String jsonString = "";
    public boolean setOffline=false;
    public String cacheFile="";
    protected boolean cache=true;



    // constructor
    public JSONParser() {

    }



    public JSONObject getOfflineJSON(String url) {

        if(cacheFile.length()<2){
            cacheFile=md5(url);
        }

        // try parse the string to a JSON object
        try {


            File folder = new File(G.FOLDER);
            if ( !folder.exists()) {
                folder.mkdirs();
            }

            final File file = new File(G.FOLDER + cacheFile + ".json");
            if (file.exists()) {

                StringBuilder text = new StringBuilder();

                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String line;

                    while ((line = br.readLine()) != null) {
                        text.append(line);
                        text.append('\n');
                    }
                    jsonString = text.toString();

                }
                catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                jsonString = "";
            }

            json = jsonString;


            jObj = new JSONObject(json);
        }
        catch (JSONException e) {
            e.printStackTrace();

        }

        // return JSON String
        return jObj;

    }


    public JSONObject getJSONFromUrl(String url) {
        if(cacheFile.length()<2){
            cacheFile=md5(url);
        }
        checkCache(url);

        if ( !(jsonString.length() > 2)) {

            // Making HTTP request
            try {
                // defaultHttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                HttpResponse httpResponse = httpClient.execute(httpPost);
//                Log.i("LOG", url);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

//                OkHttpClient client = new OkHttpClient();
//                Request request = new Request.Builder().url(url).build();
//                Response response = client.newCall(request).execute();
////                return response.body().string();
//                is = response.body().byteStream();

            }
            catch (Exception e) {
                e.printStackTrace();
            }

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                json = sb.toString();

                Log.i("LOG", json);

                //Delete Old Data
                try{
                    File file = new File(G.FOLDER + ".cache/" + cacheFile + ".json");
                    file.delete();
                }catch(Exception e){
                    e.printStackTrace();
                }


                try {
                    BufferedWriter out = new BufferedWriter(new FileWriter(G.FOLDER + cacheFile + ".json", true));
                    out.write(json);
                    out.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

            }
            catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());

            }
        } else {
            json = jsonString;
        }
        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        }
        catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());

        }

        // return JSON String
        return jObj;

    }



    private void checkCache(String url) {
        try {
            if (( !isInternetConnected() || setOffline==true) &&  cache==true) {

                File folder = new File(G.FOLDER);
                boolean success = true;
                if ( !folder.exists()) {
                    success = folder.mkdirs();
                }

                File file = new File(G.FOLDER + cacheFile + ".json");
//                Log.i("LOG", G.FolderAddress + cacheFile + ".json");
                if (file.exists()) {

                    StringBuilder text = new StringBuilder();

                    try {
                        BufferedReader br = new BufferedReader(new FileReader(file));
                        String line;

                        while ((line = br.readLine()) != null) {
                            text.append(line);
                            text.append('\n');
                        }
                        jsonString = text.toString();

                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    jsonString = "";
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }



    private boolean isInternetConnected() {
        try {
            ConnectivityManager conMgr = (ConnectivityManager) G.context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()) {
                return true;
            } else {
                return false;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }



    private static final String md5(String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest: messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}