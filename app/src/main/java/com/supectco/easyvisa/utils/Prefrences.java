package com.supectco.easyvisa.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Administrator on 12/29/2015.
 */
public class Prefrences {


    private static SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(G.context);
    public static boolean getBoolean(String key, boolean defaulValue){
        return prefs.getBoolean(key, defaulValue);
    }
    public static String getString(String key, String defaulValue){
        return prefs.getString(key, defaulValue);
    }
    public static String getString(String key){
        return prefs.getString(key, "");
    }
    public static int getInt(String key, int defaulValue){
        return prefs.getInt(key, defaulValue);
    }
    public static int getInt(String key){
        return prefs.getInt(key, 0);
    }
    public static void putBoolean(String key, boolean value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }
    public static void putString(String key, String value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static void putInt(String key, int value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }
    public static void remove(String key){
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("KeyName").commit();
    }
    public static void removeAll(){
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear().commit();
    }



}
