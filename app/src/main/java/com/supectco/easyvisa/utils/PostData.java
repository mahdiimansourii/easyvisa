package com.supectco.easyvisa.utils;

import android.os.Handler;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 12/24/2015.
 */
public class PostData {

    public String url="";
    private List<NameValuePair> nameValuePairs;
    public Runnable onDoneRunnable=null;
    public String result="";

    PostData(int NumberOFFields){
        nameValuePairs = new ArrayList<NameValuePair>(NumberOFFields);
    }


    public void addField(String key, String value){
        nameValuePairs.add(new BasicNameValuePair(key.trim(), value.trim()));
    }

    public void post(){
        final Handler postHandler = new Handler();
        Runnable postRunnable = new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {

                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(url);

                    try {
                        // Add your data



                        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                        HttpResponse response = httpclient.execute(httppost);
                        final String xresult = EntityUtils.toString(response.getEntity());

//                        Toast(result);
                            //G.Log(xresult);
                        if(onDoneRunnable!=null) {
                            result=xresult;
                            postHandler.post(onDoneRunnable);
                        }

                    }

                    catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };
        Thread mThread = new Thread(postRunnable);
        mThread.start();

    }




}
