package com.supectco.easyvisa.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.supectco.easyvisa.views.RoundedTransformation;
import com.supectco.easyvisa.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;


import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * Created by Administrator on 3/31/2015.
 */
public class G extends Application {


    private static G instance;



    public static G getInstance() {
        return instance;
    }

    public static Typeface typeFace=null;
    public static Typeface font=null;
    public static Context context;
    public static String         FolderAddress = null;
    public static String FOLDER        = Environment.getExternalStorageDirectory() + "/.easvsa/";
    public static String         MAIN_SERVER       = "http://www.supectco.com/apps/easyvisa/handler/";
//    public static String         MAIN_SERVER       = "http://www.appk.ir/app/reminderplus/";
//    public static String         SERVER       = MAIN_SERVER+"handler/";
//    public static String         IMG_PATH      = SERVER + "images/";
//    public static String         IMG_PATH_GRAVATAR      = "http://www.gravatar.com/avatar/";
//    public static String         SLIDES_PATH   = SERVER+"slides/";
//    public static String         ICONS_PATH   = SERVER+"icons/";
    public static String IMG_PATH      =  "http://www.supectco.com/apps/easyvisa/portal/uploads/";
    public static String SLIDES_PATH      = "http://www.supectco.com/apps/easyvisa/portal/uploads/";
    public static String IMAGE      = " ";
    public static String deviceName;
    public static SQLiteDatabase Database;
    public static Handler handler=new Handler();
    public static LayoutInflater inflater;
    public static String value1="";
    public static String value2="";
    public static String value3="";
    public static String textMessage="";
    public static String deviceID;
    public static SQLiteDatabase myDB;
    public static Activity currentActivity;
    public static SharedPreferences prefs;
    public static int gYear=0;
    public static int gMonth=0;
    public static int gDay=0;
    public static Activity activity=null;
    public static Runnable runnable=null;
    public static Runnable runnablex=null;
    public static Runnable totalPriceRunnable=null;
    public static Boolean PermissionToLoadCat=false;
    public static String player_id="";
    public static TextView txtSum;
    public static RelativeLayout nodata;
    public static ArrayList<String> colorIDs = new ArrayList<>();
    public static ArrayList<String> colorFilter = new ArrayList<>();
    public static ArrayList<Integer> chckColorFilter = new ArrayList<>();
    public static ArrayList<String> typeIDs = new ArrayList<>();
    public static ArrayList<String> brandIDs = new ArrayList<>();
    public static ArrayList<String> slidesArrayImage = new ArrayList<>();
    public static ArrayList<String> slidesArrayTitle = new ArrayList<>();
    public static ArrayList<ImageView> colorFilterImageView = new ArrayList<>();
    public static Class cl;
    public static String COLOR=" ";
    public static String str="";

    public static int catIDa;
    public static int subCatIDa;
    public static int listTypea;
    public static String catNamea;
    public static String subCatNamea="دسته بندی";
    public static boolean isAllContentsa;
    public static boolean isClickeda;
    public static double Latitude=0;
    public static double Longitude=0;
    public static LayoutInflater  inflator;
    public static int isSynced=0;
    public static Runnable getRunnable=null;
    public static String CountryImage="";
    public static String CountryImagex="";
    public static String CountryName="";
    public static double lat=0;
    public static double lng=0;







    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();


        context = getApplicationContext();
        instance = this;
        deviceID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        FolderAddress = FOLDER+".cache";
        typeFace = Typeface.createFromAsset(context.getAssets(),"fonts/farsi.ttf");
        font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/farsi2.ttf");
    }


    public static  void creatFolder(){

        try {
            File folders = new File(FolderAddress);
            folders.mkdirs();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void Log(String msg){
        Log.i("LOG", msg);
    }
    public static void Toast(final String msg){
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }



    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }



    public static void loadImage(final String path, final String imagename, ImageView imageview, boolean extra) {
        if(imagename.length()<5){
            imageview.setVisibility(View.GONE);
        }else {
            Glide.with(G.context).load(path + imagename).into(imageview);
        }
    }




    public static boolean isValidContextForGlide(final Context context) {
        if (context == null) {
            return false;
        }
        if (context instanceof Activity) {
            final Activity activity = (Activity) context;
            if (activity.isDestroyed() || activity.isFinishing()) {
                return false;
            }
        }
        return true;
    }




    public static boolean isInternetAvailable() {
        try {
            ConnectivityManager cm = (ConnectivityManager) G.context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();

        } catch (Exception e) {
            return false;
        }

    }


    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static String farsiNumeric(String num) {

        try {
            num = num.replaceAll("1", "۱");
            num = num.replaceAll("2", "۲");
            num = num.replaceAll("3", "۳");
            num = num.replaceAll("4", "۴");
            num = num.replaceAll("5", "۵");
            num = num.replaceAll("6", "۶");
            num = num.replaceAll("7", "۷");
            num = num.replaceAll("8", "۸");
            num = num.replaceAll("9", "۹");
            num = num.replaceAll("0", "۰");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return num;
    }

    public static String decimalNumeric(long i){

        String pattern = "###,###.##";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        decimalFormat.setGroupingSize(3);

        String number = decimalFormat.format(i);
//        System.out.println(number);

        return  number;

    }

    public static String decimalNumericx(float i){

        String pattern = "###,###.##";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        decimalFormat.setGroupingSize(3);

        String number = decimalFormat.format(i);
//        System.out.println(number);

        return  number;

    }


    public static void CToast(final String msg){
        G.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                LayoutInflater inflater = G.activity.getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast, (ViewGroup) G.activity.findViewById(R.id.toast_layout_root));

                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText(msg);


                Toast toast = new Toast(context);
                toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.BOTTOM, 0, 0);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
        });
    }


    public static boolean isInteger(String s) {
        return isInteger(s,10);
    }

    public static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }

}
