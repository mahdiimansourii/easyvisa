package com.supectco.easyvisa.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.supectco.easyvisa.utils.FormatHelper;
import com.supectco.easyvisa.R;

/**
 * Created by 108 on 3/8/2017.
 */

public class SimpleSpinnerAdapter extends BaseAdapter {
    private Context context;
    private String[] str;
    private LayoutInflater mInflater;

    public SimpleSpinnerAdapter(Context context, String[] str) {
        this.context = context;
        this.str = str;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return str.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    //k
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ListContent holder;
        View v = convertView;
        if (v == null) {
            v = mInflater.inflate(R.layout.spinner_style, null);
            holder = new ListContent();

            holder.tv = (TextView) v.findViewById(R.id.tv);

            v.setTag(holder);
        } else {
            holder = (ListContent) v.getTag();
        }
        holder.tv.setText(FormatHelper.toPersianNumber(str[position]));
        return v;
    }

    static class ListContent {
        TextView tv;
    }
}
