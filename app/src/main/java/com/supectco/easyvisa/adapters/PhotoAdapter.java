package com.supectco.easyvisa.adapters;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.models.PhotoModel;
import com.supectco.easyvisa.R;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Administrator on 10/15/2015.
 */
public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PersonViewHolder> {

    public PhotoAdapter(List<PhotoModel.PhotoList> listItem) {
        this.items = listItem;
        notifyDataSetChanged();
    }

    public PhotoAdapter() {
        items = new ArrayList<>();
    }

    public void add(PhotoModel.PhotoList item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public int layoutID = 0;
    public int catID = 0;
    List<PhotoModel.PhotoList> items;
    String TYPE = "";
    String IMAGE = "";
    Handler handler = new Handler();


    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        ImageView Image;
        LinearLayout Gbox;


        PersonViewHolder(View itemView) {
            super(itemView);

            Image = (ImageView) itemView.findViewById(R.id.Image);
            Gbox = (LinearLayout) itemView.findViewById(R.id.Gbox);

        }
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder viewHolder, final int i) {

        G.loadImage(G.IMG_PATH, items.get(i).getImage(), viewHolder.Image, false);


        viewHolder.Gbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new SweetAlertDialog(G.currentActivity, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("آیا از انتخاب این رنگ مطمئنید؟")
                        .setCancelText("خیر")
                        .setConfirmText("بله")
                        .showCancelButton(true)
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {
                                sDialog
                                        .setTitleText("انتخاب شد. ")
                                        .setConfirmText("مرحله بعدی")
                                        .setConfirmClickListener(null)
                                        .showCancelButton(false).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                                        sDialog.cancel();
                                        G.IMAGE = items.get(i).getImage();
                                        G.COLOR = items.get(i).getColor();
                                        handler.post(G.runnablex);
                                        G.currentActivity.finish();

                                    }
                                })
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                            }
                        })
                        .show();

            }
        });


    }


    private int lastPosition = -1;

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.photo_layout, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition)
//        {
//            Animation animation = AnimationUtils.loadAnimation(G.context, android.R.anim.fade_in);
//            animation.setDuration(350);
//            viewToAnimate.startAnimation(animation);
//            lastPosition = position;
//        }
    }

}