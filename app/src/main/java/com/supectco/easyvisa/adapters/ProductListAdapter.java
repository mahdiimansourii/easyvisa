package com.supectco.easyvisa.adapters;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.supectco.easyvisa.activities.ViewProduct;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.activities.DottedImageView;
import com.supectco.easyvisa.models.ProductListModel;
import com.supectco.easyvisa.views.MyTextViewBold;
import com.supectco.easyvisa.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/15/2015.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.PersonViewHolder> {

    public ProductListAdapter(List<ProductListModel.ProductList> listItem) {
        this.items = listItem;
        notifyDataSetChanged();
    }

    public ProductListAdapter() {
        items = new ArrayList<>();
    }

    public void add(ProductListModel.ProductList item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public int layoutID = 0;
    public int catID = 0;
    List<ProductListModel.ProductList> items;
    String TYPE = "";
    String IMAGE = "";
    Handler handler = new Handler();


    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        MyTextView Price, OldPrice, Desc , Type , Brand;
        MyTextViewBold Title;
        ImageView Image;
        CardView clickbtn;
        DottedImageView imgColor;
        LinearLayout lylColor;


        PersonViewHolder(View itemView) {
            super(itemView);

            Title = (MyTextViewBold) itemView.findViewById(R.id.Title);
            Desc = (MyTextView) itemView.findViewById(R.id.Desc);
            Price = (MyTextView) itemView.findViewById(R.id.Price);
            OldPrice = (MyTextView) itemView.findViewById(R.id.OldPrice);
            Type = (MyTextView) itemView.findViewById(R.id.Type);
            Brand = (MyTextView) itemView.findViewById(R.id.Brand);
            Image = (ImageView) itemView.findViewById(R.id.Image);
            imgColor = (DottedImageView) itemView.findViewById(R.id.imgColor);
            clickbtn = (CardView) itemView.findViewById(R.id.clickbtn);
            lylColor = (LinearLayout) itemView.findViewById(R.id.lylColor);

        }
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder viewHolder, final int i) {


        viewHolder.Title.setText(G.farsiNumeric(items.get(i).getTitle()));
        viewHolder.Desc.setText(items.get(i).getDesc());
        viewHolder.Type.setText(items.get(i).getType());
        viewHolder.Brand.setText(items.get(i).getBrand());

        viewHolder.lylColor.removeAllViews();

            String[] separated = items.get(i).getColor().split("\\$");
            for(int j= 0 ; j<separated.length ; j++){
                View colorView = G.inflater.inflate(R.layout.color_layout_adapter, null, false);
                final DottedImageView imgColor = (DottedImageView) colorView.findViewById(R.id.imgColor);
                imgColor.setImageType(DottedImageView.imgTypeOnlySolid);
              //  imgColor.setSolidColor(Color.parseColor(separated[j]));
                viewHolder.lylColor.addView(colorView);
            }







        if (G.isInteger(items.get(i).getPrice()) == true) {
            String str = G.decimalNumeric(Long.parseLong(items.get(i).getPrice()));
            viewHolder.Price.setText(G.farsiNumeric(str + " تومان"));
        } else {
            viewHolder.Price.setText(G.farsiNumeric(items.get(i).getPrice()));
        }

        if (G.isInteger(items.get(i).getOldPrice()) == true) {
            String str = G.decimalNumeric(Long.parseLong(items.get(i).getOldPrice()));
            viewHolder.OldPrice.setText(G.farsiNumeric(str + " تومان"));
        } else {
            viewHolder.OldPrice.setText(G.farsiNumeric(items.get(i).getOldPrice()));
        }

        viewHolder.OldPrice.setPaintFlags(viewHolder.OldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        G.loadImage(G.IMG_PATH, items.get(i).getImage(), viewHolder.Image, false);


        IMAGE = items.get(i).getImage();
        viewHolder.clickbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(G.currentActivity, ViewProduct.class);
                intent.putExtra("image", IMAGE);
                intent.putExtra("id", items.get(i).getID());
                G.currentActivity.startActivity(intent);

            }
        });


    }


    private int lastPosition = -1;

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_list_layout, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition)
//        {
//            Animation animation = AnimationUtils.loadAnimation(G.context, android.R.anim.fade_in);
//            animation.setDuration(350);
//            viewToAnimate.startAnimation(animation);
//            lastPosition = position;
//        }
    }

}