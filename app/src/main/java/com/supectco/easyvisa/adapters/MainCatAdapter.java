package com.supectco.easyvisa.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.supectco.easyvisa.R;
import com.supectco.easyvisa.activities.ChooseCat;
import com.supectco.easyvisa.activities.ProductList;
import com.supectco.easyvisa.models.MainModel;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/15/2015.
 */
public class MainCatAdapter extends RecyclerView.Adapter<MainCatAdapter.PersonViewHolder> {

    public MainCatAdapter(List<MainModel.Cats> listItem) {
        this.items = listItem;
        notifyDataSetChanged();
    }

    public MainCatAdapter() {
        items = new ArrayList<>();
    }

    public void add(MainModel.Cats item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public int layoutID = 0;
    public int catID = 0;
    List<MainModel.Cats> items;
    String TYPE = "";



    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        MyTextView Title;
        LinearLayout Gbox;

        PersonViewHolder(View itemView) {
            super(itemView);
            Title = (MyTextView) itemView.findViewById(R.id.Title);
            Gbox = (LinearLayout) itemView.findViewById(R.id.Gbox);

        }
    }

    @Override
    public void onBindViewHolder(PersonViewHolder viewHolder, final int i) {


        viewHolder.Title.setText(G.farsiNumeric(items.get(i).getTitle()));

        viewHolder.Gbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(items.get(i).getIsFinal()==0){
                    Intent intent = new Intent(G.currentActivity , ChooseCat.class);
                    intent.putExtra("id" ,items.get(i).getiD());
                    intent.putExtra("title" ,items.get(i).getTitle() );
                    intent.putExtra("catLevel" ,2 );
                    G.currentActivity.startActivity(intent);

                }else {

                    G.colorIDs.clear();
                    G.typeIDs.clear();
                    G.brandIDs.clear();
                    Intent intent = new Intent(G.currentActivity, ProductList.class);
                    intent.putExtra("priorityID", 1);
                    intent.putExtra("catName", items.get(i).getTitle());
                    G.currentActivity.startActivity(intent);

                }



            }
        });




    }


    private int lastPosition = -1;

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.maincat_layout, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition)
//        {
//            Animation animation = AnimationUtils.loadAnimation(G.context, android.R.anim.fade_in);
//            animation.setDuration(350);
//            viewToAnimate.startAnimation(animation);
//            lastPosition = position;
//        }
    }

}