package com.supectco.easyvisa.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.supectco.easyvisa.models.Item_myorder;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.R;

import java.util.List;

/**
 * Created by Administrator on 10/15/2015.
 */
public class AdapterItemMyOrders extends RecyclerView.Adapter<AdapterItemMyOrders.PersonViewHolder>{

    public int layoutID=0;

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        TextView date,state,city,address , price,isPayed;
        RelativeLayout clickbtn;
        LinearLayout MainBox2;
        ImageView iconIsPayed;
        LinearLayout lylIsPayed;

        PersonViewHolder(View itemView) {
            super(itemView);
            state = (TextView)itemView.findViewById(R.id.state);
            date = (TextView)itemView.findViewById(R.id.date);
            city = (TextView)itemView.findViewById(R.id.city);
            address = (TextView)itemView.findViewById(R.id.address);
            price = (TextView)itemView.findViewById(R.id.price);
            clickbtn = (RelativeLayout)itemView.findViewById(R.id.clickbtn);
            MainBox2 = (LinearLayout) itemView.findViewById(R.id.MainBox2);
            isPayed = (TextView)itemView.findViewById(R.id.isPayed);
            iconIsPayed = (ImageView)itemView.findViewById(R.id.iconIsPayed);
            lylIsPayed = (LinearLayout)itemView.findViewById(R.id.lylIsPayed);

        }
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder viewHolder, final int i) {

        viewHolder.state.setText(G.farsiNumeric(listItem.get(i).state));
        viewHolder.date.setText(G.farsiNumeric(listItem.get(i).date));
        viewHolder.city.setText(G.farsiNumeric(listItem.get(i).city));
        viewHolder.address.setText(G.farsiNumeric(listItem.get(i).address));
        viewHolder.price.setText(G.farsiNumeric(listItem.get(i).price)+ " تومان");


        if(listItem.get(i).isPayed==0){

            viewHolder.isPayed.setText(G.farsiNumeric("در انتظار تایید"));
            viewHolder.iconIsPayed.setImageResource(R.drawable.icon_wait);
            viewHolder.lylIsPayed.setBackgroundResource(R.drawable.roundedbackground_graypadding);

        }else{

            viewHolder.isPayed.setText(G.farsiNumeric("تایید شده"));
            viewHolder.iconIsPayed.setImageResource(R.drawable.icon_checkmark_whitex);
            viewHolder.lylIsPayed.setBackgroundResource(R.drawable.roundedbackground_gpadding);
        }







    }

    List<Item_myorder> listItem;
    private int lastPosition = -1;


    public AdapterItemMyOrders(List<Item_myorder> listItem){
        this.listItem = listItem;
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(layoutID, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition)
//        {
//            Animation animation = AnimationUtils.loadAnimation(G.context, android.R.anim.fade_in);
//            animation.setDuration(350);
//            viewToAnimate.startAnimation(animation);
//            lastPosition = position;
//        }
    }

}