package com.supectco.easyvisa.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.models.MyOrdersModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/15/2015.
 */
public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.PersonViewHolder> {

    public MyOrdersAdapter(List<MyOrdersModel.MyOrder> listItem) {
        this.items = listItem;
        notifyDataSetChanged();
    }

    public MyOrdersAdapter() {
        items = new ArrayList<>();
    }

    public void add(MyOrdersModel.MyOrder item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public int layoutID = 0;
    public int catID = 0;
    List<MyOrdersModel.MyOrder> items;
    String TYPE = "";
    String IMAGE="";


    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        MyTextView date,state,city,address , price,isPayed , title;
        RelativeLayout clickbtn;
        LinearLayout MainBox2;
        ImageView iconIsPayed;
        LinearLayout lylIsPayed;


        PersonViewHolder(View itemView) {
            super(itemView);
            state = (MyTextView)itemView.findViewById(R.id.state);
            date = (MyTextView)itemView.findViewById(R.id.date);
            city = (MyTextView)itemView.findViewById(R.id.city);
            title = (MyTextView)itemView.findViewById(R.id.title);
            address = (MyTextView)itemView.findViewById(R.id.address);
            price = (MyTextView)itemView.findViewById(R.id.price);
            clickbtn = (RelativeLayout)itemView.findViewById(R.id.clickbtn);
            MainBox2 = (LinearLayout) itemView.findViewById(R.id.MainBox2);
            isPayed = (MyTextView)itemView.findViewById(R.id.isPayed);
            iconIsPayed = (ImageView)itemView.findViewById(R.id.iconIsPayed);
            lylIsPayed = (LinearLayout)itemView.findViewById(R.id.lylIsPayed);

        }
    }

    @Override
    public void onBindViewHolder(PersonViewHolder viewHolder, final int i) {

        viewHolder.state.setText(G.farsiNumeric(items.get(i).getState()));
        viewHolder.date.setText(G.farsiNumeric(items.get(i).getDate()));
        viewHolder.city.setText(G.farsiNumeric(items.get(i).getCity()));
        viewHolder.address.setText(G.farsiNumeric(items.get(i).getAddress()));
        viewHolder.title.setText(G.farsiNumeric(items.get(i).getTitle()));

        String str =  G.decimalNumericx(Float.parseFloat(items.get(i).getPrice()));
        viewHolder.price.setText(G.farsiNumeric(str+ " تومان"));

        if(items.get(i).getIsConfirm().equals("0")){

            viewHolder.isPayed.setText(G.farsiNumeric("در انتظار تایید"));
            viewHolder.iconIsPayed.setImageResource(R.drawable.icon_wait);
            viewHolder.lylIsPayed.setBackgroundResource(R.drawable.roundedbackground_graypadding);

        }else{

            viewHolder.isPayed.setText(G.farsiNumeric("تایید شده"));
            viewHolder.iconIsPayed.setImageResource(R.drawable.icon_checkmark_whitex);
            viewHolder.lylIsPayed.setBackgroundResource(R.drawable.roundedbackground_gpadding);
        }



    }


    private int lastPosition = -1;

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_myorders, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition)
//        {
//            Animation animation = AnimationUtils.loadAnimation(G.context, android.R.anim.fade_in);
//            animation.setDuration(350);
//            viewToAnimate.startAnimation(animation);
//            lastPosition = position;
//        }
    }

}