package com.supectco.easyvisa.adapters;

import android.app.Dialog;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.activities.DottedImageView;
import com.supectco.easyvisa.models.Item_shb;
import com.supectco.easyvisa.views.MyEditText;
import com.supectco.easyvisa.views.MyTextViewBold;

import java.util.List;

/**
 * Created by Administrator on 10/15/2015.
 */
public class AdapterShb extends RecyclerView.Adapter<AdapterShb.PersonViewHolder> {

    public int layoutID = 0;
    public int catID = 0;
    List<Item_shb> itemShb;
    String TYPE = "";
    boolean isExistGift = false;
    float totalValue = 0, totalValuex = 0;
    String METRAJ = "";
    int Count = 0;
    public Handler handler = new Handler();

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        public MyTextView price, desc, nums, totalPrice;
        MyTextViewBold title;
        ImageView image;
        //        ImageView image , edit , delete;
        RelativeLayout clickbtn;
        int p = 0;
        DottedImageView imgColor;
        LinearLayout lylDelete;


        PersonViewHolder(View itemView) {
            super(itemView);
            title = (MyTextViewBold) itemView.findViewById(R.id.Title);
            desc = (MyTextView) itemView.findViewById(R.id.Desc);
            price = (MyTextView) itemView.findViewById(R.id.Price);
            nums = (MyTextView) itemView.findViewById(R.id.nums);
            totalPrice = (MyTextView) itemView.findViewById(R.id.totalPrice);
            image = (ImageView) itemView.findViewById(R.id.image);
//            edit = (ImageView)itemView.findViewById(R.id.edit);
//            delete = (ImageView)itemView.findViewById(R.id.delete);
            clickbtn = (RelativeLayout) itemView.findViewById(R.id.clickbtn);
            imgColor = (DottedImageView) itemView.findViewById(R.id.imgColor);
            lylDelete = (LinearLayout) itemView.findViewById(R.id.lylDelete);

        }
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder viewHolder, final int i) {

        viewHolder.title.setText(G.farsiNumeric(itemShb.get(i).title));
        viewHolder.desc.setText(G.farsiNumeric(itemShb.get(i).desc));
        viewHolder.nums.setText(G.farsiNumeric(itemShb.get(i).metraj + " عدد"));
//


        try{
            viewHolder.imgColor.setImageType(DottedImageView.imgTypeOnlySolid);
        }catch (Exception e){
            e.printStackTrace();
        }
        viewHolder.imgColor.setSolidColor(Color.parseColor(itemShb.get(i).color));

        if (G.isInteger(itemShb.get(i).price) == true) {
            String str = G.decimalNumeric(Long.parseLong(itemShb.get(i).price));
            viewHolder.price.setText(G.farsiNumeric(str + " تومان "));
        } else {
            viewHolder.price.setText(G.farsiNumeric(itemShb.get(i).price));
        }

//        String TP = String.valueOf(Long.parseLong(itemShb.get(i).price)*Float.parseFloat(itemShb.get(i).metraj));
//        viewHolder.totalPrice.setText(G.farsiNumeric(TP + " تومان"));

        String TP = G.decimalNumeric(Long.parseLong(itemShb.get(i).price)*Long.parseLong(itemShb.get(i).metraj));
       
        viewHolder.totalPrice.setText(G.farsiNumeric(TP + " تومان"));

        G.loadImage(G.IMG_PATH, itemShb.get(i).image, viewHolder.image, false);


        viewHolder.nums.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                changeRequireNum("عدد", Integer.parseInt(itemShb.get(i).metraj), i);

            }
        });





        viewHolder.lylDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(G.context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_deletex);
                MyTextView BtnYes = (MyTextView) dialog.findViewById(R.id.yes);
                MyTextView BtnNo = (MyTextView) dialog.findViewById(R.id.no);
                MyTextView txtTitle = (MyTextView) dialog.findViewById(R.id.txtTitle);
                MyTextView txtNote = (MyTextView) dialog.findViewById(R.id.txtNote);
                txtNote.setText("آیا از حذف این محصول مطمئن هستید؟");


                BtnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        totalValue = 0;
                        G.myDB.execSQL("DELETE FROM `ShoppingBag` WHERE `id` = " + itemShb.get(i).ID);
                        itemShb.remove(i);
                        notifyItemRemoved(i);
                        notifyItemRangeChanged(i, itemShb.size());
                        Cursor c = G.myDB.rawQuery("SELECT * FROM `ShoppingBag`", null);
                        c.moveToFirst();
                        if (c.getCount() < 1) {
                            G.nodata.setVisibility(View.VISIBLE);
                        }

                        if (c.moveToFirst()) {
                            do {
                                String price = c.getString(c.getColumnIndex("price"));
                                String nums = c.getString(c.getColumnIndex("nums"));
                                try {
                                    float p = Float.parseFloat(price) * Float.parseFloat(nums);
                                    totalValue = totalValue + p;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } while (c.moveToNext());

                            c.close();
                        }
                        String str = G.decimalNumericx(totalValue);
                        G.txtSum.setText(G.farsiNumeric(str + " تومان"));
                        dialog.dismiss();

                    }
                });

                BtnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }


    private int lastPosition = -1;


    public void changeRequireNum(String Unit, int Num, final int position) {

        final Dialog dialog = new Dialog(G.context);
        dialog.getWindow().requestFeature(1);
        dialog.setContentView(R.layout.require_num);
        MyTextView No = (MyTextView) dialog.findViewById(R.id.No);
        MyTextView Yes = (MyTextView) dialog.findViewById(R.id.Yes);
        final MyEditText num = (MyEditText) dialog.findViewById(R.id.num);
        MyTextView unit = (MyTextView) dialog.findViewById(R.id.unit);


        unit.setText(G.farsiNumeric(Unit));
        num.setText(G.farsiNumeric(Num + ""));
        num.setTypeface(G.font);
        No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }

        });


        Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (num.getText().toString().length() > 0 && G.isInteger(num.getText().toString())) {
//                    G.orders.set(position, Integer.parseInt(num.getText().toString()));
                    G.myDB.execSQL("UPDATE `ShoppingBag` SET `nums` = '" + Integer.parseInt(num.getText().toString()) + "' WHERE `id` = '" + itemShb.get(position).ID + "' ");
                    handler.post(G.getRunnable);
                    dialog.dismiss();
                }

            }

        });

        dialog.show();


    }


    public AdapterShb(List<Item_shb> listItem) {
        this.itemShb = listItem;
    }

    @Override
    public int getItemCount() {
        return itemShb.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(layoutID, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition)
//        {
//            Animation animation = AnimationUtils.loadAnimation(G.context, android.R.anim.fade_in);
//            animation.setDuration(350);
//            viewToAnimate.startAnimation(animation);
//            lastPosition = position;
//        }
    }


}