package com.supectco.easyvisa.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.R;
import com.supectco.easyvisa.activities.ChooseService;
import com.supectco.easyvisa.models.GetCountriesModel;
import com.supectco.easyvisa.models.MyOrdersModel;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/15/2015.
 */
public class GetCountriesAdapter extends RecyclerView.Adapter<GetCountriesAdapter.PersonViewHolder> {

    public GetCountriesAdapter(List<GetCountriesModel.Country> listItem) {
        this.items = listItem;
        notifyDataSetChanged();
    }

    public GetCountriesAdapter() {
        items = new ArrayList<>();
    }

    public void add(GetCountriesModel.Country item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public int layoutID = 0;
    public int catID = 0;
    List<GetCountriesModel.Country> items;
    String TYPE = "";
    String IMAGE="";


    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        MyTextView title;
        ImageView img , flag;
        LinearLayout placeBtn;


        PersonViewHolder(View itemView) {
            super(itemView);
            title = (MyTextView) itemView.findViewById(R.id.title);
            img = (ImageView) itemView.findViewById(R.id.image);
            flag = (ImageView) itemView.findViewById(R.id.flag);
            placeBtn = (LinearLayout) itemView.findViewById(R.id.placeBtn);

        }
    }

    @Override
    public void onBindViewHolder(PersonViewHolder viewHolder, final int i) {

        viewHolder.title.setText(G.farsiNumeric(items.get(i).getTitle()));
        G.loadImage(G.SLIDES_PATH, items.get(i).getImage(), viewHolder.img, false);
        G.loadImage(G.IMG_PATH, items.get(i).getFlag(), viewHolder.flag, false);

        G.Log("IDIDIDID:   "  + items.get(i).getId() );

        viewHolder.placeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.currentActivity , ChooseService.class);
                intent.putExtra("ID" , items.get(i).getId());
                G.CountryImage=items.get(i).getFlag();
                G.CountryImagex=items.get(i).getImage();
                G.CountryName=items.get(i).getTitle();
                G.currentActivity.startActivity(intent);
            }
        });


    }


    private int lastPosition = -1;

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_countries, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition)
//        {
//            Animation animation = AnimationUtils.loadAnimation(G.context, android.R.anim.fade_in);
//            animation.setDuration(350);
//            viewToAnimate.startAnimation(animation);
//            lastPosition = position;
//        }
    }

}