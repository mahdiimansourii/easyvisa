package com.supectco.easyvisa.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.R;

import java.util.ArrayList;

public class ImageSlider_Adapterx extends PagerAdapter {

    private ArrayList<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;

    public ImageSlider_Adapterx(Context context, ArrayList<String> IMAGES) {
        this.context = context;
        this.IMAGES = IMAGES;
        inflater = LayoutInflater.from(G.context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {

        View imageLayout = inflater.inflate(R.layout.viewpager_slidingimages, view, false);

        assert imageLayout != null;

        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);

        G.loadImage(G.IMG_PATH, IMAGES.get(position), imageView, false);
//        G.Log(G.IMG_PATH+IMAGES.get(position));
//            G.Log("position: " + position);
//        try{
//            if(position>0){
//            G.IMAGE = IMAGES.get(position-1);
//              }
//        }catch (Exception e){
//            e.printStackTrace();
//        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}