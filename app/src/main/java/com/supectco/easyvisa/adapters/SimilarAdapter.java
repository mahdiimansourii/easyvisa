package com.supectco.easyvisa.adapters;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.supectco.easyvisa.activities.ViewProduct;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.models.ViewProductModel;
import com.supectco.easyvisa.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/15/2015.
 */
public class SimilarAdapter extends RecyclerView.Adapter<SimilarAdapter.PersonViewHolder> {

    public SimilarAdapter(List<ViewProductModel.SimilarProduct> listItem) {
        this.items = listItem;
        notifyDataSetChanged();
    }

    public SimilarAdapter() {
        items = new ArrayList<>();
    }

    public void add(ViewProductModel.SimilarProduct item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public int layoutID = 0;
    public int catID = 0;
    List<ViewProductModel.SimilarProduct> items;
    String TYPE = "";



    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        MyTextView Title, Price, OldPrice;
        ImageView Image;
        LinearLayout Gbox;

        PersonViewHolder(View itemView) {
            super(itemView);
            Title = (MyTextView) itemView.findViewById(R.id.Title);
            Price = (MyTextView) itemView.findViewById(R.id.Price);
            OldPrice = (MyTextView) itemView.findViewById(R.id.OldPrice);
            Image = (ImageView) itemView.findViewById(R.id.Image);
            Gbox = (LinearLayout) itemView.findViewById(R.id.Gbox);

        }
    }

    @Override
    public void onBindViewHolder(PersonViewHolder viewHolder, final int i) {


        viewHolder.Title.setText(G.farsiNumeric(items.get(i).getTitle()));


        if(G.isInteger(items.get(i).getPrice())==true){
            String str =  G.decimalNumeric(Long.parseLong(items.get(i).getPrice()));
            viewHolder.Price.setText(G.farsiNumeric(str + " تومان"));
        }else{
            viewHolder.Price.setText(G.farsiNumeric(items.get(i).getPrice()));
        }

        if(G.isInteger(items.get(i).getOldPrice())==true){
            String str =  G.decimalNumeric(Long.parseLong(items.get(i).getOldPrice()));
            viewHolder.OldPrice.setText(G.farsiNumeric(str + " تومان"));
        }else{
            viewHolder.OldPrice.setText(G.farsiNumeric(items.get(i).getOldPrice()));
        }

        viewHolder.OldPrice.setPaintFlags(viewHolder.OldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        G.loadImage(G.IMG_PATH, items.get(i).getImage(), viewHolder.Image, false);



        viewHolder.Gbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(G.currentActivity , ViewProduct.class);
                intent.putExtra("image" , items.get(i).getImage());
                intent.putExtra("id", items.get(i).getID());
                G.currentActivity.startActivity(intent);

            }
        });




    }


    private int lastPosition = -1;

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.newest_layout, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition)
//        {
//            Animation animation = AnimationUtils.loadAnimation(G.context, android.R.anim.fade_in);
//            animation.setDuration(350);
//            viewToAnimate.startAnimation(animation);
//            lastPosition = position;
//        }
    }

}