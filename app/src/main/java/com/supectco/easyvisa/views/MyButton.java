package com.supectco.easyvisa.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.supectco.easyvisa.utils.G;

/**
 * Created by Administrator on 12/15/2015.
 */
public class MyButton extends Button {

    public MyButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setType(context);
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setType(context);
    }

    public MyButton(Context context) {
        super(context);
        setType(context);
    }

    private void setType(Context context){
        this.setTypeface(G.typeFace);

//        this.setShadowLayer(1.5f, 5, 5, getContext().getResources().getColor(R.color.black_shadow));
    }
}