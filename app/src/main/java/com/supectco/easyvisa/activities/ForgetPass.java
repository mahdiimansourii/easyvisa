package com.supectco.easyvisa.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.models.ConfirmUserModel;
import com.supectco.easyvisa.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPass extends Master {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_pass);

        Bundle extrass = getIntent().getExtras();
        if(extrass!=null){
            PHONE = extrass.getString("mobile");
            TYPE = extrass.getString("type");
        }
        initializeToolbarx();
        Initializelayout();
        setStatusBarColor();


    }

    String PHONE="" , TYPE="";
    RelativeLayout loading;
    CardView btnsubmit;
    EditText phone;
    ConfirmUserModel confirmUserModel;
    private void Initializelayout(){
        btnsubmit =(CardView) findViewById(R.id.btnsubmit);
        phone=(EditText) findViewById(R.id.phone);
        loading =(RelativeLayout) findViewById(R.id.loading);

        Typeface typeFace = Typeface.createFromAsset(G.context.getAssets(),"fonts/farsi2.ttf");
        phone.setTypeface(typeFace);

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkError();
            }
        });
    }

    public void checkError(){

        if (phone.getText().toString().trim().length() == 0) {
            Toast("لطفاً شماره موبایل خود را وارد کنید.");
        }else if ((phone.getText().toString().trim().length() >= 1) && (phone.getText().toString().trim().length() < 11)) {
            Toast("لطفاً شماره خود را کامل وارد کنید.");
        }else {
            postData();
        }

    }


    RetrofitProvider retrofitProvider;
    APIService apiService;
    private void postData() {

        loading.setVisibility(View.VISIBLE);
        btnsubmit.setVisibility(View.GONE);
        loading.setOnClickListener(null);
        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        Call<ConfirmUserModel> call = apiService.confirmUser(G.deviceID
                , G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd")
                , getTxt(phone));

        G.Log("url: " + call.request().url());
        call.enqueue(new Callback<ConfirmUserModel>() {
            @Override
            public void onResponse(Call<ConfirmUserModel> call, Response<ConfirmUserModel> response) {
                confirmUserModel= response.body();
                int status = confirmUserModel.getStatus();
                if (status == 200) {
                    loading.setVisibility(View.GONE);
                    btnsubmit.setVisibility(View.VISIBLE);
                    Intent intent=new Intent(getApplicationContext(), GetCode.class);
                    intent.putExtra("mobile" , phone.getText().toString());
                    intent.putExtra("type" , "3");
                    startActivity(intent);
                    finish();
                } else {
                    Toast("شماره وارد شده در سامانه ثبت نشده است.");
                    loading.setVisibility(View.GONE);
                    btnsubmit.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ConfirmUserModel> call, Throwable t) {
                loading.setVisibility(View.GONE);
                btnsubmit.setVisibility(View.VISIBLE);
                Toast("خطا در برقرای ارتباط با سرور");
            }
        });
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }



}
