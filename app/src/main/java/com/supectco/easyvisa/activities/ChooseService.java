package com.supectco.easyvisa.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.RelativeLayout;

import com.stephentuso.welcome.WelcomeHelper;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.adapters.ImageSlider_Adapter;
import com.supectco.easyvisa.models.MainModel;
import com.supectco.easyvisa.models.Update;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChooseService extends Master {

    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    RelativeLayout loading, error;
    Handler handler = new Handler();
    MyTextView retry;
    CardView crdSetDate;
    int ID = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_service);

        Bundle extrass = getIntent().getExtras();
        if (extrass != null) {
            ID = extrass.getInt("ID");
        }


        initializeToolbar();
        initializeDrawer();
        initializeLayout();

    }


    public void initializeLayout() {


        loading = (RelativeLayout) findViewById(R.id.loading);
        error = (RelativeLayout) findViewById(R.id.error);
        crdSetDate = (CardView) findViewById(R.id.crdSetDate);


        crdSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(getApplicationContext(), Form.class);
                intent.putExtra("ID" , ID);
                startActivity(intent);


            }
        });


    }


    public void showError() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    error.setVisibility(View.VISIBLE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (G.isInternetAvailable() == false) {
                                showError();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), G.cl);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });
    }


}
