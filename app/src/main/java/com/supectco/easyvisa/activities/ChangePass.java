package com.supectco.easyvisa.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.models.ChangePassModel;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePass extends Master {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_pass);

        Bundle extrass = getIntent().getExtras();
        if(extrass!=null){
            PHONE = extrass.getString("mobile");
            ACTIVE_CODE = extrass.getString("activeCode");
        }

        initializeToolbarx();
        Initializelayout();
        setStatusBarColor();

    }


    RelativeLayout loading;
    CardView btnSubmit;
    EditText password, repassword;
    String PHONE="" , ACTIVE_CODE="";
    ChangePassModel changePassModel;
    private void Initializelayout() {

        btnSubmit = (CardView) findViewById(R.id.btnsubmit);
        loading = (RelativeLayout) findViewById(R.id.loading);
        password = (EditText) findViewById(R.id.password);
        repassword = (EditText) findViewById(R.id.repassword);


        Typeface typeFace = Typeface.createFromAsset(G.context.getAssets(), "fonts/farsi2.ttf");
        password.setTypeface(typeFace);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkError();


            }
        });


    }


    public void checkError() {

        if (password.getText().toString().trim().length() == 0) {

            Toast("لطفاً کلمه عبور خود را وارد کنید.");

        } else if (repassword.getText().toString().trim().length() == 0) {

            Toast("لطفاً تکرار کلمه عبور خود را وارد کنید.");

        } else if (((password.getText().toString().trim().length() >= 1) && (password.getText().toString().trim().length() < 4)) || ((repassword.getText().toString().trim().length() >= 1) && (repassword.getText().toString().trim().length() < 4))) {

            Toast("کلمه عبور نمی تواند کمتر از 4 کاراکتر باشد.");

        } else if (!(password.getText().toString().equals(repassword.getText().toString()))) {

            Toast("کلمه عبور و تکرار کلمه عبور یکسان نمی باشند.");
        } else {
            postData();
        }


    }


    RetrofitProvider retrofitProvider;
    APIService apiService;

    private void postData() {

        loading.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.GONE);
        loading.setOnClickListener(null);

        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();


        Call<ChangePassModel> call = apiService.changePass(G.deviceID
                , G.md5(G.deviceID + "fajeostreet89kl")
                , G.md5(getTxt(password)),PHONE , ACTIVE_CODE );

        call.enqueue(new Callback<ChangePassModel>() {
            @Override
            public void onResponse(Call<ChangePassModel> call, Response<ChangePassModel> response) {
                changePassModel = response.body();
                int status = changePassModel.getStatus();
                if (status == 200) {
                    loading.setVisibility(View.GONE);
                    btnSubmit.setVisibility(View.VISIBLE);
                    Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast("رمز عبور شما با موفقیت تغییر کرد.");
                } else {
                    Toast("خطا در برقرای ارتباط با سرور");
                    loading.setVisibility(View.GONE);
                    btnSubmit.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ChangePassModel> call, Throwable t) {
                loading.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.VISIBLE);
                Toast("خطا در برقرای ارتباط با سرور");
            }
        });
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
