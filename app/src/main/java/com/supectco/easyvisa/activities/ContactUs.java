package com.supectco.easyvisa.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.models.ContactUsModel;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.views.MyTextViewBold;
import com.supectco.easyvisa.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUs extends Master {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us);

        initializeToolbarx();
        Initializelayout();
        setStatusBarColor();
        initializeData();


    }

    RelativeLayout loading, error;
    Handler handler=new Handler();
    MyTextView retry ;
    MyTextViewBold txtContent;
    ContactUsModel contactUsModel;
    private void Initializelayout(){

        loading = (RelativeLayout) findViewById(R.id.loading);
        error = (RelativeLayout) findViewById(R.id.error);
        retry = (MyTextView) findViewById(R.id.retry);
        txtContent = (MyTextViewBold) findViewById(R.id.txtContent);

        txtContent.setAutoLinkMask(Linkify.PHONE_NUMBERS);
        txtContent.setMovementMethod(LinkMovementMethod.getInstance());
    }



    RetrofitProvider retrofitProvider;
    APIService apiService;
    private void initializeData() {

        loading.setVisibility(View.VISIBLE);
        loading.setOnClickListener(null);

        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();


        Call<ContactUsModel> call = apiService.contactUs(G.deviceID
                , G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd")
        );

        call.enqueue(new Callback<ContactUsModel>() {
            @Override
            public void onResponse(Call<ContactUsModel> call, Response<ContactUsModel> response) {
                contactUsModel = response.body();
                String content = contactUsModel.getContent();
                loading.setVisibility(View.GONE);
                txtContent.setText(G.farsiNumeric(content));

            }

            @Override
            public void onFailure(Call<ContactUsModel> call, Throwable t) {
                showError();
            }
        });


    }

    public void showError(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                try{
                    error.setVisibility(View.VISIBLE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (G.isInternetAvailable() == false) {
                                showError();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), G.cl);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                }catch (Exception e){

                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        G.cl = this.getClass();
        G.currentActivity = this;
        if(G.isInternetAvailable()==false){
            showError();
        }
    }
}

