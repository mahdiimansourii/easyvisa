package com.supectco.easyvisa.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.models.Item_myorder;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.adapters.MyOrdersAdapter;
import com.supectco.easyvisa.models.MyOrdersModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyOrders extends Master {
    LinearLayoutManager llm;
    List<Item_myorder> item_myorder = new ArrayList<>();
    MyOrdersAdapter myOrdersAdapter;
    RecyclerView rv;
    int page=1;
    int ID;
    RelativeLayout  loading , nodata , error;
    String name,desc;
    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    private MyOrdersModel myOrdersModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_orders);


        Bundle extrass = getIntent().getExtras();
        if(extrass!=null){
            ID = extrass.getInt("id");
        }


            initializeToolbar();
            initializeDrawer();
            InitializeLayout();
            initializeData();


    }


    private void InitializeLayout(){


        loading = (RelativeLayout) findViewById(R.id.loading);
        nodata = (RelativeLayout) findViewById(R.id.nodata);
        error = (RelativeLayout) findViewById(R.id.error);
        rv = (RecyclerView)findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        llm = new LinearLayoutManager(G.context.getApplicationContext());
        rv.setLayoutManager(llm);

        myOrdersAdapter = new MyOrdersAdapter();
        rv.setAdapter(myOrdersAdapter);
        runLayoutAnimation(rv);


    }



    private void initializeData() {

        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        loading.setVisibility(View.VISIBLE);
        Call<MyOrdersModel> call = apiService.getDataMyOrders(G.deviceID , G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd") , G.prefs.getString("mobile" , ""));


        call.enqueue(new Callback<MyOrdersModel>() {
            @Override
            public void onResponse(Call<MyOrdersModel> call, Response<MyOrdersModel> response) {

                myOrdersModel = response.body();
                myOrdersAdapter.clear();

                if(response.body().myOrder!=null){

                    for(MyOrdersModel.MyOrder item : response.body().myOrder){
                        myOrdersAdapter.add(item);
                    }
                }else {

                    nodata.setVisibility(View.VISIBLE);
                }
                loading.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<MyOrdersModel> call, Throwable t) {

            }
        });

    }



     private void runLayoutAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }


    @Override
    protected void onResume() {
        super.onResume();
        G.context = this;
        if (G.isInternetAvailable() == false) {
            error.setVisibility(View.VISIBLE);
        }


    }
}
