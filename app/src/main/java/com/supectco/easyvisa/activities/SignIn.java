package com.supectco.easyvisa.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.models.SignInModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignIn extends Master {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.signinx);
        initializeToolbarx();
        Initializelayout();
        setStatusBarColor();


    }

    RelativeLayout loading;
    TextView btnforgot, btnregister;
    CardView btnsubmit;
    Button loginbtn, cancelbtn;
    EditText phone,password;
    SignInModel signInModel;
    CheckBox showPass;
    private void Initializelayout(){
        btnsubmit =(CardView) findViewById(R.id.btnsubmit);
        btnforgot=(TextView) findViewById(R.id.btnforgot);
        btnregister=(TextView) findViewById(R.id.btnregister);
        phone=(EditText) findViewById(R.id.phone);
        password=(EditText) findViewById(R.id.password);
        loading =(RelativeLayout) findViewById(R.id.loading);
        showPass =(CheckBox) findViewById(R.id.showPass);

        Typeface typeFace = Typeface.createFromAsset(G.context.getAssets(),"fonts/farsi2.ttf");
        phone.setTypeface(typeFace);
        password.setTypeface(typeFace);



        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(), SignUp.class);
                startActivity(intent);
                finish();
            }
        });


        btnforgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), ForgetPass.class);
                    startActivity(intent);
                    finish();
            }
        });




        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkError();
            }
        });


        showPass.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {


                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }

            }
        });



    }

    public void checkError(){

        if (phone.getText().toString().trim().length() == 0) {

            Toast("لطفاً شماره موبایل خود را وارد کنید.");

        }else if ((phone.getText().toString().trim().length() >= 1) && (phone.getText().toString().trim().length() < 11)) {

            Toast("لطفاً شماره خود را کامل وارد کنید.");
        }else if(password.getText().toString().trim().length()==0){

            Toast("لطفاً کلمه عبور خود را وارد کنید.");

        }else if(password.getText().toString().trim().length()>=1 && password.getText().toString().trim().length()<4) {
            Toast("کلمه عبور نمی تواند کمتر از 4 کاراکتر باشد.");
        }else{

            postData();

        }
    }



    RetrofitProvider retrofitProvider;
    APIService apiService;
    private void postData() {

        loading.setVisibility(View.VISIBLE);
        btnsubmit.setVisibility(View.GONE);
        loading.setOnClickListener(null);

        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();


        Call<SignInModel> call = apiService.doSignIn(G.deviceID
                , G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd")
                , getTxt(phone), G.md5(getTxt(password)));

        call.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                signInModel = response.body();
                int status = signInModel.getStatus();
                if (status == 200) {
                    loading.setVisibility(View.GONE);
                    btnsubmit.setVisibility(View.VISIBLE);
                    SharedPreferences.Editor editor = G.prefs.edit();
                    editor.putBoolean("isLogin", true);
                    editor.putString("mobile", phone.getText().toString());
                    editor.commit();
                    Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast("شما با موفقیت وارد شدید.");
                } else if(status==300) {
                    Intent intent = new Intent(getApplicationContext(), GetCode.class);
                    intent.putExtra("mobile" , phone.getText().toString());
                    intent.putExtra("type" , "10");
                    startActivity(intent);
                    finish();
                    loading.setVisibility(View.GONE);
                    btnsubmit.setVisibility(View.VISIBLE);
                }else if(status==400) {
                    Toast("شماره و یا رمز عبور صحیح نمی باشد!");
                    loading.setVisibility(View.GONE);
                    btnsubmit.setVisibility(View.VISIBLE);
                }else if(status==500) {
                    Toast("شماره وارد شده و یا رمز عبور معتبر نمی باشد.");
                    loading.setVisibility(View.GONE);
                    btnsubmit.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                loading.setVisibility(View.GONE);
                btnsubmit.setVisibility(View.VISIBLE);
                G.Log("t: " + t.getMessage());
                Toast("خطا در برقرای ارتباط با سرور");
            }
        });
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }



}
