package com.supectco.easyvisa.activities;

/**
 * Created by Administrator on 12/24/2015.
 */
public class AppPlatform {

//    private static LocationManager locationManager;
//    private static double Latitude, Longtitude;
//    private static PackageManager manager;
//    private static PackageInfo info;
//    private static String VERSION_NAME;
//    private static int VERSION_CODE;
//    public static Context context;
//    public static Activity activity;
//    public static JSONObject slidesJson;
//    public static String validUser="";
//    public static String message;
//    public static boolean updateAvailable;
//    public static boolean mandatoryUpdate;
//    public static String link;
//    public static RetrofitProvider retrofitProvider;
//    public static APIService apiService;
//    public static UserInfoModel userInfoModel;
//
//
//    public static void check(final Context cntx, final Activity actvty, String deviceID,String API, String mobile, String username, String email, String playerID, String market) {
//
//        context = cntx;
//        activity = actvty;
//
//        if (isInternetAvailable()) {
//            try {
//
//                manager = context.getApplicationContext().getPackageManager();
//                info = manager.getPackageInfo(context.getApplicationContext().getPackageName(), 0);
//                VERSION_CODE = info.versionCode;
//                VERSION_NAME = info.versionName;
//                locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
////                Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                Location locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//                if (Latitude == 0 && Longtitude == 0) {
//                    Latitude = locationNet.getLatitude();
//                    Longtitude = locationNet.getLongitude();
//                }
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            retrofitProvider = new RetrofitProvider();
//            apiService = retrofitProvider.getAPIService();
//
//            Call<UserInfoModel> call = apiService.req(md5(deviceID + "lkdfvmovm4958v49m94j98jldkfvmd")
//                    , mobile, username, email, API , "1", deviceID, VERSION_CODE + "", VERSION_NAME + ""
//                    , android.os.Build.VERSION.RELEASE, android.os.Build.MODEL, String.valueOf(android.os.Build.VERSION.SDK_INT)
//                    , Latitude + "", Longtitude + "", market, playerID);
//
//            call.enqueue(new Callback<UserInfoModel>() {
//                @Override
//                public void onResponse(Call<UserInfoModel> call, Response<UserInfoModel> response) {
//
//                    try{
//
//
//                        userInfoModel = response.body();
//                        validUser = userInfoModel.validUser;
//                        updateAvailable = userInfoModel.updateAvailable;
//                        mandatoryUpdate = userInfoModel.mandatoryUpdate;
//                        message = userInfoModel.message;
//                        link = userInfoModel.link;
//
//
//                        if (response.isSuccessful()) {
//
//                            if(activity!=null){
//
//                                if (validUser.equals("block")) {
//                                    updateApp("خطا!", message, mandatoryUpdate, false, true, "");
//                                }
//                                if (updateAvailable) {
//                                    updateApp("بروزرسانی", message, mandatoryUpdate, true, true, link);
//                                }
//                                if (mandatoryUpdate) {
//                                    updateApp("بروزرسانی", message, mandatoryUpdate, false, true, link);
//                                }
//
//                            }
//
//                        }
//                    }catch (Exception e){
//                        e.printStackTrace();
//
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<UserInfoModel> call, Throwable t) {
//                }
//            });
//        }
//
//    }
//
//
//    private static boolean isInternetAvailable() {
//        try {
//            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//
//    private static void updateApp(String Title, String Message, boolean mandatoryUpdate, boolean isShowNegativeButton, boolean isShowPositiveButton, final String link) {
//
//        final Dialog dialog = new Dialog(activity);
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.message);
//        TextView title = (TextView) dialog.findViewById(R.id.title);
//        TextView message = (TextView) dialog.findViewById(R.id.message);
//        TextView yes = (TextView) dialog.findViewById(R.id.yes);
//        TextView no = (TextView) dialog.findViewById(R.id.no);
//        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/farsi.ttf");
//        title.setTypeface(typeFace);
//        message.setTypeface(typeFace);
//        yes.setTypeface(typeFace);
//        no.setTypeface(typeFace);
//
//        title.setText(Title);
//        message.setText(Message);
//
//
//
//
//        no.setText("انصراف");
//
//        if(validUser.equals("block")){
//            yes.setText("خروج");
//        }else {
//            yes.setText("بروزرسانی");
//        }
//
//
//        if (isShowNegativeButton) {
//            no.setVisibility(View.VISIBLE);
//        } else {
//            no.setVisibility(View.GONE);
//        }
//
//
//        if (isShowPositiveButton) {
//            yes.setVisibility(View.VISIBLE);
//        } else {
//            yes.setVisibility(View.GONE);
//        }
//
//
//        if (mandatoryUpdate) {
//            dialog.setCancelable(false);
//            no.setVisibility(View.GONE);
//        } else {
//            dialog.setCancelable(true);
//        }
//
//        if(validUser.equals("block")){
//            dialog.setCancelable(false);
//            no.setVisibility(View.GONE);
//        }
//
//
//
//        no.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
//
//        yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if(validUser.equals("block")){
//                    activity.finish();
//                }else {
//                    Intent browserIntent = new Intent("android.intent.action.VIEW",
//                            Uri.parse(link));
//                    activity.startActivity(browserIntent);
//                }
//
//            }
//        });
//
//        dialog.show();
//    }
//
//    public static final String md5(final String s) {
//        final String MD5 = "MD5";
//        try {
//            // Create MD5 Hash
//            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
//            digest.update(s.getBytes());
//            byte messageDigest[] = digest.digest();
//
//            // Create Hex String
//            StringBuilder hexString = new StringBuilder();
//            for (byte aMessageDigest : messageDigest) {
//                String h = Integer.toHexString(0xFF & aMessageDigest);
//                while (h.length() < 2)
//                    h = "0" + h;
//                hexString.append(h);
//            }
//            return hexString.toString();
//
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        return "";
//    }
//
//
//    public class UserInfoModel {
//        @SerializedName("validUser")
//        @Expose
//        private String validUser;
//        @SerializedName("updateAvailable")
//        @Expose
//        private boolean updateAvailable;
//        @SerializedName("mandatoryUpdate")
//        @Expose
//        private boolean mandatoryUpdate;
//        @SerializedName("link")
//        @Expose
//        private String link;
//        @SerializedName("message")
//        @Expose
//        private String message;
//
//        public String isValidUser() {
//            return validUser;
//        }
//
//        public void setValidUser(String validUser) {
//            this.validUser = validUser;
//        }
//
//        public boolean isUpdateAvailable() {
//            return updateAvailable;
//        }
//
//        public void setUpdateAvailable(boolean updateAvailable) {
//            this.updateAvailable = updateAvailable;
//        }
//
//        public boolean isMandatoryUpdate() {
//            return mandatoryUpdate;
//        }
//
//        public void setMandatoryUpdate(boolean mandatoryUpdate) {
//            this.mandatoryUpdate = mandatoryUpdate;
//        }
//
//        public String getMessage() {
//            return message;
//        }
//
//        public void setMessage(String message) {
//            this.message = message;
//        }
//
//        public String getLink() {
//            return link;
//        }
//
//        public void setLink(String link) {
//            this.link = link;
//        }
//    }

}
