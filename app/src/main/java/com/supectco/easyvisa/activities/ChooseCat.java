package com.supectco.easyvisa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.R;
import com.supectco.easyvisa.adapters.ChooseCatAdapter;
import com.supectco.easyvisa.models.ChooseCatModel;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChooseCat extends Master {

    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    int ID=0 , catLevel=0;
    String title="";
    RelativeLayout loading, error;
    ChooseCatModel chooseCatModel;
    ChooseCatAdapter chooseCatAdapter;
    RecyclerView rv;
    private LinearLayoutManager llm;
    Handler handler = new Handler();
    MyTextView retry;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choosecat);



        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ID = extras.getInt("id");
            catLevel = extras.getInt("catLevel");
            title = extras.getString("title");
        }



        initializeToolbarx();
        txtTitle.setText(title);
        Initializelayout();
        setStatusBarColor();
        initializeData();



    }




    private void Initializelayout() {

        loading = (RelativeLayout) findViewById(R.id.loading);
        error = (RelativeLayout) findViewById(R.id.error);
        retry = (MyTextView) findViewById(R.id.retry);
        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        llm = new LinearLayoutManager(G.context.getApplicationContext());
        rv.setLayoutManager(llm);




    }


    private void initializeData() {

        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        loading.setVisibility(View.VISIBLE);

        Call<ChooseCatModel> call = apiService.getCats(G.deviceID, G.md5(G.deviceID + "ncase8934f49909") , catLevel , ID);

        G.Log("call:" + call.request().url());


        call.enqueue(new Callback<ChooseCatModel>() {
            @Override
            public void onResponse(Call<ChooseCatModel> call, Response<ChooseCatModel> response) {

                if (response.isSuccessful()) {
                    chooseCatModel = response.body();
                    chooseCatAdapter = new ChooseCatAdapter();
                    rv.setAdapter(chooseCatAdapter);


                    if (response.body().getCats()!= null) {
                        for (ChooseCatModel.Cat item : response.body().getCats()) {
                            chooseCatAdapter.add(item);
                        }
                    }

                    runLayoutAnimation(rv);
                    loading.setVisibility(View.GONE);
                }


            }

            @Override
            public void onFailure(Call<ChooseCatModel> call, Throwable t) {
                showError();
            }
        });




    }

    private void runLayoutAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }


    public void showError() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    error.setVisibility(View.VISIBLE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (G.isInternetAvailable() == false) {
                                showError();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), ChooseCat.class);
                                intent.putExtra("id" ,ID);
                                intent.putExtra("title" ,title);
                                intent.putExtra("catLevel" ,catLevel );
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        G.currentActivity=this;
    }
}
