package com.supectco.easyvisa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.R;
import com.supectco.easyvisa.adapters.GetCountriesAdapter;
import com.supectco.easyvisa.adapters.MyOrdersAdapter;
import com.supectco.easyvisa.adapters.ProductListAdapter;
import com.supectco.easyvisa.models.GetCountriesModel;
import com.supectco.easyvisa.models.Item_myorder;
import com.supectco.easyvisa.models.MyOrdersModel;
import com.supectco.easyvisa.models.ProductListModel;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Countries extends Master {
    LinearLayoutManager llm;
    List<Item_myorder> item_myorder = new ArrayList<>();
    GetCountriesAdapter getCountriesAdapter;
    RecyclerView rv;
    int page = 1;
    int ID;
    RelativeLayout loading, nodata, error;
    String name, desc;
    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    private GetCountriesModel getCountriesModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.countries);


        Bundle extrass = getIntent().getExtras();
        if (extrass != null) {
            ID = extrass.getInt("ID");
        }


        initializeToolbar();
        initializeDrawer();
        InitializeLayout();
        initializeData();


    }


    private void InitializeLayout() {


        loading = (RelativeLayout) findViewById(R.id.loading);
        nodata = (RelativeLayout) findViewById(R.id.nodata);
        error = (RelativeLayout) findViewById(R.id.error);
        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        llm = new LinearLayoutManager(G.context.getApplicationContext());
        rv.setLayoutManager(llm);

        getCountriesAdapter = new GetCountriesAdapter();
        rv.setAdapter(getCountriesAdapter);
        runLayoutAnimation(rv);


    }


    private void initializeData() {

        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        loading.setVisibility(View.VISIBLE);
        Call<GetCountriesModel> call = apiService.getCountries(G.deviceID, G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd"));


        call.enqueue(new Callback<GetCountriesModel>() {
            @Override
            public void onResponse(Call<GetCountriesModel> call, Response<GetCountriesModel> response) {

                getCountriesModel = response.body();
                getCountriesAdapter.clear();

                if (response.body().countries != null) {

                    for (GetCountriesModel.Country item : response.body().countries) {
                        getCountriesAdapter.add(item);
                    }
                } else {

                    nodata.setVisibility(View.VISIBLE);
                }

                runLayoutAnimation(rv);
                loading.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<GetCountriesModel> call, Throwable t) {

            }
        });


    }


    private void runLayoutAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }


    @Override
    protected void onResume() {
        super.onResume();
        G.context = this;
        if (G.isInternetAvailable() == false) {
            error.setVisibility(View.VISIBLE);
        }


    }
}
