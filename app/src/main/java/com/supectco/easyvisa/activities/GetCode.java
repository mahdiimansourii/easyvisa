package com.supectco.easyvisa.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.utils.G;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.models.GetCodeModel;
import com.supectco.easyvisa.views.MyTextViewBold;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetCode extends Master {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.getcode);

        Bundle extrass = getIntent().getExtras();
        if(extrass!=null){
            PHONE = extrass.getString("mobile");
            TYPE = extrass.getString("type");
        }

        initializeToolbarx();
        Initializelayout();
        setStatusBarColor();


    }

    String PHONE="" , TYPE="";
    RelativeLayout loading;
    CardView btnsubmit;
    EditText code;
    MyTextViewBold title;
    MyTextViewBold countDownTimer;
    CircularProgressBar circularProgressBar;
    Handler handler = new Handler();
    GetCodeModel getCodeModel;
    private void Initializelayout(){
        btnsubmit =(CardView) findViewById(R.id.btnsubmit);
        code=(EditText) findViewById(R.id.code);
        title=(MyTextViewBold) findViewById(R.id.title);
        countDownTimer=(MyTextViewBold) findViewById(R.id.countDownTimer);
        loading =(RelativeLayout) findViewById(R.id.loading);
        circularProgressBar = (CircularProgressBar)findViewById(R.id.progressBar);
        circularProgressBar.setColor(ContextCompat.getColor(this, R.color.ColorPrimary));
        circularProgressBar.setBackgroundColor(ContextCompat.getColor(this, R.color.ColorPrimaryLight));

        Typeface typeFace = Typeface.createFromAsset(G.context.getAssets(),"fonts/farsi2.ttf");
        code.setTypeface(typeFace);

        countDownTimer.setEnabled(false);


        countDownTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCodeAnimation();
                sendCodeAgain();
            }
        });




        if(TYPE.equals("3")){
            title.setText(getResources().getString(R.string.forgetPassComplete));
        }else if(TYPE.equals("1")){
            title.setText("لطفا برای ورود به برنامه، کد دریافتی را وارد کنید.");
        }

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkError();
            }
        });
        sendCodeAnimation();


    }


    public void sendCodeAnimation(){
        circularProgressBar.setColor(ContextCompat.getColor(this, R.color.ColorPrimary));
        circularProgressBar.setProgress(0);
        countDownTimer.setText(G.farsiNumeric("0"));
        countDownTimer.setTextSize(30);
        new CountDownTimer(60000, 50) {

            public void onTick(long millisUntilFinished) {
                float ratio = (60000 - millisUntilFinished) / (float) 60000;
                float progress = ratio * 100;
                circularProgressBar.setProgressWithAnimation(progress);
                int second = Math.round((60000 * (1 - ratio)) / 1000);
                countDownTimer.setText(G.farsiNumeric("" + second));

            }

            public void onFinish() {
                circularProgressBar.setProgressWithAnimation(100);
                countDownTimer.setEnabled(true);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        circularProgressBar.setColor(ContextCompat.getColor(GetCode.this, R.color.green_light3));
                        countDownTimer.setText("ارسال دوباره کد");
                        countDownTimer.setTextSize(14);
                    }
                },1200);


            }
        }.start();


    }

    public void checkError(){

        if(code.getText().toString().length()<4){
            Toast(getResources().getString(R.string.codeError));
        }else if(G.isInternetAvailable()==false){
            Toast(getResources().getString(R.string.internetError));
        }else {
            postData();

        }

    }


    RetrofitProvider retrofitProvider;
    APIService apiService;
    private void postData() {

        loading.setVisibility(View.VISIBLE);
        btnsubmit.setVisibility(View.GONE);
        loading.setOnClickListener(null);
        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        Call<GetCodeModel> call = apiService.getCode(G.deviceID , G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd") , PHONE ,code.getText().toString());


        call.enqueue(new Callback<GetCodeModel>() {
            @Override
            public void onResponse(Call<GetCodeModel> call, Response<GetCodeModel> response) {

                getCodeModel = response.body();
                int status = getCodeModel.getStatus();
                if (status == 200) {
                    loading.setVisibility(View.GONE);
                    btnsubmit.setVisibility(View.VISIBLE);
                    if((TYPE.equals("3"))){
                        Intent intent=new Intent(getApplicationContext(), ChangePass.class);
                        intent.putExtra("mobile" , PHONE);
                        intent.putExtra("activeCode" , code.getText().toString());
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }else{
                        SharedPreferences.Editor editor = G.prefs.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("mobile", PHONE);
                        editor.commit();
                        Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        if(TYPE.equals("10")){
                            Toast("شما با موفقیت وارد شدید.");
                        }else if(TYPE.equals("2")){
                            Toast("ثبت نام شما با موفقیت انجام شد.");
                        }
                    }
                } else {
                    Toast("کد ارسالی صحیح نمی باشد.");
                    loading.setVisibility(View.GONE);
                    btnsubmit.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onFailure(Call<GetCodeModel> call, Throwable t) {
                loading.setVisibility(View.GONE);
                btnsubmit.setVisibility(View.VISIBLE);
                Toast("خطا در برقرای ارتباط با سرور");
            }
        });

    }


    public void sendCodeAgain(){
        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        Call<Boolean> call = apiService.sendCodeAgain(PHONE, G.deviceID , G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd"));
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
            }
        });
    }



    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }



}
