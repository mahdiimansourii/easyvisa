package com.supectco.easyvisa.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.adapters.ProductListAdapter;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.models.ProductListModel;
import com.supectco.easyvisa.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductList extends Master {

    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    private ProductListModel productListModel;
    RelativeLayout loading, error, noData;
    RecyclerView rv;
    private LinearLayoutManager llm;
    ProductListAdapter productListAdapter;
    int page_number = 1, priorityID = 0, sortID = 0;
    String page = "1",catName="" , spinnerValue3 = "", spinnerValue2 = "", spinnerValue1 = "", colorIds = "", typeIds = "", brandIds = "", QUARY = "";
    LinearLayout lylFilter;
    private BottomSheetBehavior mBottomSheetBehavior;
    BottomSheetDialogFragment bottomSheetDialogFragment;
    View bottomSheet;
    int POSITION = 0;
    boolean isFirstRun = true;
    int mPageNumber = 1;
    MyTextView retrybtn;
    Handler handler = new Handler();
    MyTextView retry , sortName;
    boolean permisionToChange3 = true, permisionToChange2 = true;
    LinearLayout lylSort , lylCat ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_list);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            priorityID = extras.getInt("priorityID");
            sortID = extras.getInt("priorityID");
            catName = extras.getString("catName");
        }






        initializeToolbar();
        initializeDrawer();
        initializeLayout();
        setStatusBarColor();
        initializeData(1);
        pageScrollListener();



        G.runnable = new Runnable() {
            @Override
            public void run() {
                productListAdapter.clear();
                initializeData(1);
            }
        };



    }


    public void initializeLayout() {

        rv = (RecyclerView) findViewById(R.id.rv);
        loading = (RelativeLayout) findViewById(R.id.loading);
        error = (RelativeLayout) findViewById(R.id.error);
        noData = (RelativeLayout) findViewById(R.id.noData);
        retrybtn = (MyTextView) findViewById(R.id.retrybtn);
        lylFilter = (LinearLayout) findViewById(R.id.lylFilter);
        retry = (MyTextView) findViewById(R.id.retry);
        sortName = (MyTextView) findViewById(R.id.sortName);
        lylSort = (LinearLayout) findViewById(R.id.lylSort);
        lylCat = (LinearLayout) findViewById(R.id.lylCat);
        lylFilter = (LinearLayout) findViewById(R.id.lylFilter);
        bottomSheet = findViewById(R.id.bottom_sheet);

        txtTitle.setText(catName);


        bottomSheetDialogFragment = new TutsPlusBottomSheetDialogFragment();


        rv.setHasFixedSize(true);
//        llm = new LinearLayoutManager(G.context.getApplicationContext());
        llm = new GridLayoutManager(G.context , 2);
        rv.setLayoutManager(llm);
        productListAdapter = new ProductListAdapter();
        productListAdapter.clear();
        rv.setAdapter(productListAdapter);





        retrybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                G.colorIDs.clear();
                G.typeIDs.clear();
                G.brandIDs.clear();
                open(ProductList.class, true);

            }
        });



        lylSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sortList();

            }
        });


        lylCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                sortList();

//                Toast("kjhsd");

//                Intent intent = new Intent(getApplicationContext() , ChooseCategory.class);
//                startActivity(intent);


            }
        });



        lylFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        });



        switch (sortID){
            case 1:
                sortName.setText("پر بازدیدترین");
                break;
            case 2:
                sortName.setText("پر فروش ترین");
                break;
            case 3:
                sortName.setText("پیشنهاد ویژه");
                break;
            case 4:
                sortName.setText("قیمت از زیاد به کم");
                break;
            case 5:
                sortName.setText("قیمت از کم به زیاد");
                break;
            case 6:
                sortName.setText("جدیدترین");
                break;


        }



    }


    private void initializeData(final int pageNumber) {

//

        colorIds = "";
        typeIds = "";
        brandIds = "";

        for (int i = 0; i < G.colorIDs.size(); i++) {
            colorIds = colorIds + "," + G.colorIDs.get(i);
        }

        for (int i = 0; i < G.typeIDs.size(); i++) {
            typeIds = typeIds + "," + G.typeIDs.get(i);
        }

        for (int i = 0; i < G.brandIDs.size(); i++) {
            brandIds = brandIds + "," + G.brandIDs.get(i);
        }


        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        if (pageNumber == 1) {
            loading.setVisibility(View.VISIBLE);
        }
        Call<ProductListModel> call = apiService.getDataProduct(G.deviceID, G.md5(G.deviceID + "ncase8934f49909"), String.valueOf(pageNumber), colorIds, typeIds, brandIds, sortID, priorityID,0 , 0, QUARY);

        G.Log("call: " + call.request().url());

        call.enqueue(new Callback<ProductListModel>() {
            @Override
            public void onResponse(Call<ProductListModel> call, Response<ProductListModel> response) {

                if (response.body() == null) {
                    hasMorePages = false;
                    return;
                }

                productListModel = response.body();
//                productListAdapter.clear();
                if (response.body().productLists != null) {
                    for (ProductListModel.ProductList item : response.body().productLists) {
                        productListAdapter.add(item);
                    }
                    if (pageNumber == 1) {
                        runLayoutAnimation(rv);
                    }
                } else {
                    if (pageNumber == 1) {
                        noData.setVisibility(View.VISIBLE);
                    }
                }
                isFirstRun = false;
                loading.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<ProductListModel> call, Throwable t) {

            }
        });

    }


    private void sortList() {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.sort_list);

        final RadioButton radio_gozine1 = (RadioButton) dialog.findViewById(R.id.radio_gozine1);
        final RadioButton radio_gozine2 = (RadioButton) dialog.findViewById(R.id.radio_gozine2);
        final RadioButton radio_gozine3 = (RadioButton) dialog.findViewById(R.id.radio_gozine3);
        final RadioButton radio_gozine4 = (RadioButton) dialog.findViewById(R.id.radio_gozine4);
        final RadioButton radio_gozine5 = (RadioButton) dialog.findViewById(R.id.radio_gozine5);
        final RadioButton radio_gozine6 = (RadioButton) dialog.findViewById(R.id.radio_gozine6);
        final RadioGroup radio_group = (RadioGroup) dialog.findViewById(R.id.radio_group);
        MyTextView chooseCat = (MyTextView) dialog.findViewById(R.id.chooseCat);
        MyTextView txt1 = (MyTextView) dialog.findViewById(R.id.txt1);
        MyTextView txt2 = (MyTextView) dialog.findViewById(R.id.txt2);
        MyTextView txt3 = (MyTextView) dialog.findViewById(R.id.txt3);
        MyTextView txt4 = (MyTextView) dialog.findViewById(R.id.txt4);
        MyTextView txt5 = (MyTextView) dialog.findViewById(R.id.txt5);
        MyTextView txt6 = (MyTextView) dialog.findViewById(R.id.txt6);
        final RelativeLayout loadingx = (RelativeLayout) dialog.findViewById(R.id.loading);




        chooseCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radio_gozine1.isChecked() == true) {
                    sortID = 1;
                } else if (radio_gozine2.isChecked() == true) {
                    sortID = 2;
                } else if (radio_gozine3.isChecked() == true) {
                    sortID = 3;
                }else if (radio_gozine4.isChecked() == true) {
                    sortID = 4;
                }else if (radio_gozine5.isChecked() == true) {
                    sortID = 5;
                }else if (radio_gozine6.isChecked() == true) {
                    sortID = 6;
                }

                initializeData(1);
                pageScrollListener();
                dialog.dismiss();


                switch (sortID){
                    case 1:
                        sortName.setText("پر بازدیدترین");
                        break;
                    case 2:
                        sortName.setText("پر فروش ترین");
                        break;
                    case 3:
                        sortName.setText("پیشنهاد ویژه");
                        break;
                    case 4:
                        sortName.setText("قیمت از زیاد به کم");
                        break;
                    case 5:
                        sortName.setText("قیمت از کم به زیاد");
                        break;
                    case 6:
                        sortName.setText("جدیدترین");
                        break;


                }



            }



        });

        dialog.show();

    }








    boolean hasMorePages = true;

    public boolean hasMorePages() {
        return hasMorePages;
    }

    private void pageScrollListener() {
        rv.addOnScrollListener(new EndlessScrollListener());
    }

    private class EndlessScrollListener extends RecyclerView.OnScrollListener {

        private LinearLayoutManager mLinearLayoutManager;
        private int mCurrentPage;
        private boolean mLoading;
        private int mLastItemsCount;
        private int mVisibleThreshold = 1;

        private EndlessScrollListener() {
            mLoading = true;
            mCurrentPage = 1;
            mLastItemsCount = 0;
        }


        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (mLinearLayoutManager == null) {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null && layoutManager instanceof LinearLayoutManager) {
                    mLinearLayoutManager = (LinearLayoutManager) layoutManager;
                }
            } else {
                if (dy > 0) {
                    if (!hasMorePages()) return;
                    if (mLoading) {
                        if (mLastItemsCount < productListAdapter.getItemCount()) {
                            mLoading = false;
                            mLastItemsCount = productListAdapter.getItemCount();
                        }
                    } else {
                        int totalItemCount = mLinearLayoutManager.getItemCount();
                        int lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();
                        if (totalItemCount <= (lastVisibleItem + mVisibleThreshold)) {
                            mLoading = true;
                            mCurrentPage++;
                            mPageNumber = mCurrentPage;
                            initializeData(mCurrentPage);
                        }
                    }
                }
            }
        }
    }


    private void runLayoutAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }


    public void showError() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    error.setVisibility(View.VISIBLE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (G.isInternetAvailable() == false) {
                                showError();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), G.cl);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onResume() {
        super.onResume();
        G.cl = this.getClass();
        G.currentActivity = this;
        if (G.isInternetAvailable() == false) {
            showError();
        }
    }
}
