package com.supectco.easyvisa.activities;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sajjad on 10/14/2016.
 */
public class RetrofitProvider {

    APIService apiService;

    public RetrofitProvider() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.supectco.com/apps/easyvisa/handler/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(APIService.class);
    }

    public APIService getAPIService() {
        return apiService;
    }
}
