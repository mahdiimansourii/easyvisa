package com.supectco.easyvisa.activities;


import android.app.Dialog;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.R;


public class ChooseColorBottomSheetDialogFragment extends BottomSheetDialogFragment {
    View contentView;
    Handler handler = new Handler();
    Handler handlerx = new Handler();

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {

        super.setupDialog(dialog, style);



        G.colorFilterImageView.clear();
        G.COLOR="";

        contentView = View.inflate(getContext(), R.layout.color_fragment_bottom_sheet, null);
        dialog.setContentView(contentView);
        initalizeLayout();
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();


        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }


    public void initalizeLayout() {

        LinearLayout lylColor = (LinearLayout) contentView.findViewById(R.id.lylColor);
        CardView crdDoFilter = (CardView) contentView.findViewById(R.id.crdDoFilter);


        for (int i = 0; i < G.slidesArrayImage.size(); i++) {
            final View colorView = G.inflater.inflate(R.layout.choose_image, null, false);
            final DottedImageView Image = (DottedImageView) colorView.findViewById(R.id.Image);
            final ImageView imgCheck = (ImageView) colorView.findViewById(R.id.imgCheck);
            G.colorFilterImageView.add(imgCheck);
            Image.setImageType(DottedImageView.imgTypeOnlySolid);
//            Image.setSolidColor(Color.parseColor(G.colorFilter.get(i)));
            Image.setSolidColor(Color.parseColor("#ff00da"));
            lylColor.addView(colorView);
            Image.setTag(i);


            Image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    handlerx.post(new Runnable() {
                        @Override
                        public void run() {
//                            final int  x  = (int)Image.getTag();
//                            if (G.chckColorFilter.get(x)==0) {
//                                G.chckColorFilter.set(x , 1);
//                                G.COLOR = G.colorFilter.get(x);
//                                G.colorFilterImageView.get(x).setVisibility(View.VISIBLE);
//                                for (int k = 0; k < G.colorFilterImageView.size(); k++) {
//                                    if(k!=x){
//                                        G.chckColorFilter.set(k, 0);
//                                        G.colorFilterImageView.get(k).setVisibility(View.GONE);
//                                    }
//                                }
//                            } else {
//                                G.chckColorFilter.set(x , 0);
//                                G.COLOR="";
//                                G.colorFilterImageView.get(x).setVisibility(View.GONE);
//                            }
                        }
                    });

                }
            });
        }



        crdDoFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(G.COLOR.toString().length()<5){
                 G.Toast("هیچ رنگی انتخاب نشده است.");
                }else {
                dismiss();
                handler.post(G.runnablex);
                }
            }
        });


    }

}