package com.supectco.easyvisa.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.R;

public class SearchActivity extends Master {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        initializeToolbarx();
        Initializelayout();
        setStatusBarColor();


    }

    String PHONE="" , TYPE="";
    RelativeLayout loading;
    CardView btnsubmit;
    EditText searchBox;
    private void Initializelayout(){
        btnsubmit =(CardView) findViewById(R.id.btnsubmit);
        searchBox=(EditText) findViewById(R.id.searchBox);
        loading =(RelativeLayout) findViewById(R.id.loading);
        Typeface typeFace = Typeface.createFromAsset(G.context.getAssets(),"fonts/farsi2.ttf");
        searchBox.setTypeface(typeFace);
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkError();
            }
        });
    }

    public void checkError(){

        if (searchBox.getText().toString().trim().length() < 3) {
            Toast(G.farsiNumeric("عبارت مورد نظر برای جستجو نمی تواند کمتر از 3 حرف باشد."));
        }else {
            Intent intent=new Intent(getApplicationContext(), SearchResult.class);
            intent.putExtra("q" , searchBox.getText().toString());
            startActivity(intent);
            finish();
        }

    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }



}
