package com.supectco.easyvisa.activities;


import android.app.Dialog;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.R;
import com.supectco.easyvisa.models.FilterModel;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TutsPlusBottomSheetDialogFragment extends BottomSheetDialogFragment {
    View contentView;
    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    RelativeLayout loading;
    LinearLayout lylColor, lylKind, lylBrand;
    CardView crdDoFilter;
    Handler handler = new Handler();
    FilterModel filterModel;
    HorizontalScrollView hsv1, hsv2, hsv3;
    //    String[] colors = new String[]{"#FF0000", "#a20000", "#460000", "#910088", "#390036", "#5a00ff"
//            , "#300087", "#00aeff", "#006391", "#808000", "#003046", "#009a21", "#0dbb00",
//            "#076200", "#88cf00", "#3d5d00", "#fffc00", "#9d9b00", "#88cf00", "#9d9b00", "#c16400"};
//
//
//    String[] kinds = new String[]{"کتان", "لینین", "پلی استر", "ساتن", "نخ", "لینین", "کتان", "ساتن"};
//    String[] brands = new String[]{"پرووال", "لیندر", "دکوریما", "رنگارنگ", "زبرا", "شید رول", "لیندر", "پرووال"};
    ArrayList<String> colors = new ArrayList<>();
    ArrayList<String> colorIDs = new ArrayList<>();
    ArrayList<String> types = new ArrayList<>();
    ArrayList<String> typeIDs = new ArrayList<>();
    ArrayList<String> brands = new ArrayList<>();
    ArrayList<String> brandIDs = new ArrayList<>();


    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();


                for (int i = 0; i < G.colorIDs.size(); i++) {
                    G.Log("G.colorIDs: " + G.colorIDs.get(i));
                }

                for (int i = 0; i < G.typeIDs.size(); i++) {
                    G.Log("G.typeIDs: " + G.typeIDs.get(i));
                }

                for (int i = 0; i < G.brandIDs.size(); i++) {
                    G.Log("G.brandIDs: " + G.brandIDs.get(i));
                }


            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

//            G.Log("onSlide");
        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {

        G.colorIDs.clear();
        G.typeIDs.clear();
        G.brandIDs.clear();
        colors.clear();
        colorIDs.clear();
        types.clear();
        typeIDs.clear();
        brands.clear();
        brandIDs.clear();

        super.setupDialog(dialog, style);
        contentView = View.inflate(getContext(), R.layout.fragment_bottom_sheet, null);
        dialog.setContentView(contentView);

        initalizeLayout();


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();


        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }


    public void initalizeLayout() {

        lylColor = (LinearLayout) contentView.findViewById(R.id.lylColor);
        lylKind = (LinearLayout) contentView.findViewById(R.id.lylKind);
        lylBrand = (LinearLayout) contentView.findViewById(R.id.lylBrand);
        crdDoFilter = (CardView) contentView.findViewById(R.id.crdDoFilter);
        loading = (RelativeLayout) contentView.findViewById(R.id.loading);
        hsv1 = (HorizontalScrollView) contentView.findViewById(R.id.hsv1);
        hsv2 = (HorizontalScrollView) contentView.findViewById(R.id.hsv2);
        hsv2 = (HorizontalScrollView) contentView.findViewById(R.id.hsv2);

        initializeDate();


        crdDoFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
                handler.post(G.runnable);

            }
        });


    }


    public void initializeDate() {


        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        loading.setVisibility(View.VISIBLE);

        Call<FilterModel> call = apiService.getTypeList(G.deviceID, G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd"));

        call.enqueue(new Callback<FilterModel>() {
            @Override
            public void onResponse(Call<FilterModel> call, Response<FilterModel> response) {
                if (response.isSuccessful()) {
                    filterModel = response.body();
                    for (int i = 0; i < filterModel.getColors().size(); i++) {
                        colors.add(filterModel.getColors().get(i).getTitle());
                        colorIDs.add(filterModel.getColors().get(i).getID());
                    }

                    for (int i = 0; i < filterModel.getBrands().size(); i++) {
                        brands.add(filterModel.getBrands().get(i).getTitle());
                        brandIDs.add(filterModel.getBrands().get(i).getID());
                    }

                    for (int i = 0; i < filterModel.getTypes().size(); i++) {
                        types.add(filterModel.getTypes().get(i).getTitle());
                        typeIDs.add(filterModel.getTypes().get(i).getID());
                    }


                    for (int i = 0; i < colors.size(); i++) {
                        View colorView = G.inflater.inflate(R.layout.color_layout, null, false);
                        final DottedImageView Image = (DottedImageView) colorView.findViewById(R.id.Image);
                        final ImageView imgCheck = (ImageView) colorView.findViewById(R.id.imgCheck);
                        Image.setImageType(DottedImageView.imgTypeOnlySolid);
                        Image.setSolidColor(Color.parseColor(colors.get(i)));
                        lylColor.addView(colorView);
                        Image.setTag(colors.get(i));

                        Image.setOnClickListener(new View.OnClickListener() {
                            boolean bl = true;

                            @Override
                            public void onClick(View view) {
                                if (bl) {
                                    imgCheck.setVisibility(View.VISIBLE);
                                    G.colorIDs.add(String.valueOf(Image.getTag()));
                                    bl = !bl;
                                } else {
                                    imgCheck.setVisibility(View.GONE);
                                    G.colorIDs.remove(String.valueOf(Image.getTag()));
                                    bl = !bl;
                                }
                            }
                        });
                    }


                    for (int i = 0; i < types.size(); i++) {
                        View colorView = G.inflater.inflate(R.layout.brand_layout, null, false);
                        final DottedImageView Image = (DottedImageView) colorView.findViewById(R.id.Image);
                        final ImageView imgCheck = (ImageView) colorView.findViewById(R.id.imgCheck);
                        final MyTextView Title = (MyTextView) colorView.findViewById(R.id.Title);
                        Image.setImageType(DottedImageView.imgTypeOnlyDash);
                        Title.setText(types.get(i));
                        Title.setTextColor(Color.GRAY);
                        Image.setTag(Integer.parseInt(typeIDs.get(i)));
                        Image.setOnClickListener(new View.OnClickListener() {
                            boolean bl = true;

                            @Override
                            public void onClick(View view) {
                                if (bl) {
                                    Title.setTextColor(Color.BLACK);
                                    Image.setPathEffectDashPaint(35, 15);
                                    Image.setDottedColor(Color.parseColor("#009e0b"));
                                    int x = (int) Image.getTag();
                                    G.Log("x: " + x);
                                    G.typeIDs.add(String.valueOf(x));
                                    bl = !bl;
                                } else {
                                    Title.setTextColor(Color.GRAY);
                                    Image.setPathEffectDashPaint(10, 10);
                                    Image.setDottedColor(Color.GRAY);
                                    int x = (int) Image.getTag();
                                    G.Log("x: " + x);
                                    G.typeIDs.remove(String.valueOf(x));
                                    bl = !bl;
                                }

                            }
                        });


                        lylKind.addView(colorView);


                    }

                    for (int i = 0; i < brands.size(); i++) {
                        View colorView = G.inflater.inflate(R.layout.brand_layout, null, false);
                        final DottedImageView Image = (DottedImageView) colorView.findViewById(R.id.Image);
                        final ImageView imgCheck = (ImageView) colorView.findViewById(R.id.imgCheck);
                        final MyTextView Title = (MyTextView) colorView.findViewById(R.id.Title);
                        Image.setImageType(DottedImageView.imgTypeOnlyDash);
                        Title.setTextColor(Color.GRAY);
                        Image.setTag(Integer.parseInt(brandIDs.get(i)));
                        Image.setOnClickListener(new View.OnClickListener() {
                            boolean bl = true;

                            @Override
                            public void onClick(View view) {
                                if (bl) {
                                    Image.setImageType(DottedImageView.imgTypeOnlySolid);
                                    Title.setTextColor(Color.WHITE);
                                    Image.setSolidColor(Color.parseColor("#ff00902d"));
                                    int x = (int) Image.getTag();
                                    G.Log("x: " + x);
                                    G.brandIDs.add(String.valueOf(x));
                                    bl = !bl;
                                } else {
                                    Image.setImageType(DottedImageView.imgTypeOnlyDash);
                                    Title.setTextColor(Color.GRAY);
                                    Image.setSolidColor(Color.GREEN);
                                    int x = (int) Image.getTag();
                                    G.Log("x: " + x);
                                    G.brandIDs.remove(String.valueOf(x));
                                    bl = !bl;
                                }

                            }
                        });
                        Title.setText(brands.get(i));
                        lylBrand.addView(colorView);
                    }

                    handler.postDelayed(new Runnable() {
                        public void run() {
                            hsv1.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                        }
                    }, 100L);

                    handler.postDelayed(new Runnable() {
                        public void run() {
                            hsv1.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                        }
                    }, 100L);


                    handler.postDelayed(new Runnable() {
                        public void run() {
                            hsv1.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                        }
                    }, 100L);

                    loading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<FilterModel> call, Throwable t) {

            }
        });

    }


}