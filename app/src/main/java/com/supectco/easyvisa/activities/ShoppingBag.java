package com.supectco.easyvisa.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.supectco.easyvisa.adapters.AdapterShb;
import com.supectco.easyvisa.models.Item_shb;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.views.MyTextView;

import java.util.ArrayList;
import java.util.List;


public class ShoppingBag extends Master {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shopping_bag);

        Bundle extrass = getIntent().getExtras();
        if (extrass != null) {


        }

        initializeToolbarx();
        initializeLayout();
        setStatusBarColor();


    }

    LinearLayoutManager llm;
    RelativeLayout noData;
    List<Item_shb> item_shb = new ArrayList<>();
    AdapterShb adapter;
    RecyclerView rv;
    int Count = 0;
    float totalValue = 0 , totalValuex=0;
    float p = 0;
    TextView Sum;
    CardView shoppingBag;
    String FINAL_PRICE = "";
    float FINAL_PRICEX=0;
    Handler handler = new Handler();
    MyTextView txtValues;

    public void initializeLayout() {

        noData = (RelativeLayout) findViewById(R.id.nodata);
        shoppingBag = (CardView) findViewById(R.id.shoppingBag);
        txtValues = (MyTextView) findViewById(R.id.txtValues);
        Sum = (TextView) findViewById(R.id.Sum);
        G.txtSum = Sum;
        G.nodata = noData;
        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        llm = new LinearLayoutManager(G.context.getApplicationContext());
        rv.setLayoutManager(llm);
        adapter = new AdapterShb(item_shb);
        adapter.layoutID = R.layout.item_shbx;
        rv.setAdapter(adapter);

        Cursor c = G.myDB.rawQuery("SELECT * FROM `ShoppingBag`", null);
        Count = c.getCount();

        if(Count>0){
            txtValues.setText("نهایی سازی خرید");
        }else {
            txtValues.setText("ثبت سفارش");
        }

        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                int id = c.getInt(c.getColumnIndex("id"));
                int productID = c.getInt(c.getColumnIndex("productID"));
                String title = c.getString(c.getColumnIndex("title"));
                String desc = c.getString(c.getColumnIndex("desc"));
                String image = c.getString(c.getColumnIndex("image"));
                String price = c.getString(c.getColumnIndex("price"));
                String nums = c.getString(c.getColumnIndex("nums"));
                String color = c.getString(c.getColumnIndex("color"));
                try {
                    p = Float.parseFloat(price) * Float.parseFloat(nums);
                    totalValue = totalValue + p;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                item_shb.add(new Item_shb(id, productID, title, desc, image, nums, price, color));
            } while (c.moveToNext());

            c.close();
        }

        adapter.notifyDataSetChanged();




        G.getRunnable = new Runnable() {
            @Override
            public void run() {

                totalValue=0;
                p=0;
                item_shb.clear();
                Cursor c = G.myDB.rawQuery("SELECT * FROM `ShoppingBag`", null);
                Count = c.getCount();
                c.moveToFirst();
                if (c.moveToFirst()) {
                    do {
                        int id = c.getInt(c.getColumnIndex("id"));
                        int productID = c.getInt(c.getColumnIndex("productID"));
                        String title = c.getString(c.getColumnIndex("title"));
                        String desc = c.getString(c.getColumnIndex("desc"));
                        String image = c.getString(c.getColumnIndex("image"));
                        String price = c.getString(c.getColumnIndex("price"));
                        String nums = c.getString(c.getColumnIndex("nums"));
                        String color = c.getString(c.getColumnIndex("color"));
                        try {
                            p = Float.parseFloat(price) * Float.parseFloat(nums);
                            totalValue = totalValue + p;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        item_shb.add(new Item_shb(id, productID, title, desc, image, nums, price, color));
                    } while (c.moveToNext());

                    c.close();
                }

                adapter.notifyDataSetChanged();

                //        String str = G.decimalNumeric(totalValue);
                String str = G.decimalNumericx(totalValue);
                FINAL_PRICEX= totalValue;
                FINAL_PRICE=str;
                Sum.setText(G.farsiNumeric(str + " تومان"));


            }
        };


//        String str = G.decimalNumeric(totalValue);
        String str = G.decimalNumericx(totalValue);
        FINAL_PRICEX= totalValue;
        FINAL_PRICE=str;
        Sum.setText(G.farsiNumeric(str + " تومان"));



        if (Count == 0) {
            noData.setVisibility(View.VISIBLE);
        } else {
            noData.setVisibility(View.GONE);
        }


        shoppingBag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Cursor c = G.myDB.rawQuery("SELECT * FROM `ShoppingBag`", null);
                int Count = c.getCount();
                if (Count > 0) {
                    if (G.prefs.getBoolean("isLogin", false) == true) {


                        Cursor cc = G.myDB.rawQuery("SELECT * FROM `ShoppingBag`", null);
                        cc.moveToFirst();
                        if (cc.moveToFirst()) {
                            do {
                                String price = cc.getString(cc.getColumnIndex("price"));
                                String nums = cc.getString(cc.getColumnIndex("nums"));
                                try {
                                    float p = Float.parseFloat(price) * Float.parseFloat(nums);
                                    totalValuex = totalValuex + p;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } while (cc.moveToNext());

                            cc.close();
                        }
                        FINAL_PRICEX = totalValuex;

                        if (G.prefs.getString("P_address", " ").length() < 5) {
//                            Intent intent=new Intent(getApplicationContext(), CompleteSignUp.class);
//                            startActivity(intent);
                            Intent intent = new Intent(getApplicationContext(), FinalConfirm.class);
                            intent.putExtra("isCompleteSignUp", false);
                            intent.putExtra("finalPrice", FINAL_PRICE);
                            intent.putExtra("finalPricex", FINAL_PRICEX);
                            startActivity(intent);
                            finish();

                        } else {
                            Intent intent = new Intent(getApplicationContext(), FinalConfirm.class);
                            intent.putExtra("isCompleteSignUp", true);
                            intent.putExtra("finalPrice", FINAL_PRICE);
                            intent.putExtra("finalPricex", FINAL_PRICEX);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        Intent intent = new Intent(getApplicationContext(), SignIn.class);
                        startActivity(intent);
                    }
                } else {
                    G.colorIDs.clear();
                    G.typeIDs.clear();
                    G.brandIDs.clear();
                    Intent intent = new Intent(getApplicationContext(), ProductList.class);
                    intent.putExtra("priorityID", 1);
                    intent.putExtra("catName", "همه محصولات");
                    startActivity(intent);
                }
            }
        });


    }




    @Override
    protected void onResume() {
        super.onResume();

        G.context = this;
        G.currentActivity = this;
    }
}
