package com.supectco.easyvisa.activities;


import com.supectco.easyvisa.models.ChangePassModel;
import com.supectco.easyvisa.models.ChooseCatModel;
import com.supectco.easyvisa.models.GetCountriesModel;
import com.supectco.easyvisa.models.MainModel;
import com.supectco.easyvisa.models.Update;
import com.supectco.easyvisa.models.AboutUsModel;
import com.supectco.easyvisa.models.BuyReqModel;
import com.supectco.easyvisa.models.ConfirmUserModel;
import com.supectco.easyvisa.models.PhotoModel;
import com.supectco.easyvisa.models.ProductListModel;
import com.supectco.easyvisa.models.SignInModel;
import com.supectco.easyvisa.models.SignUpModel;
import com.supectco.easyvisa.models.UpdateModel;
import com.supectco.easyvisa.models.ViewProductModel;
import com.supectco.easyvisa.models.ContactUsModel;
import com.supectco.easyvisa.models.DiscountModel;
import com.supectco.easyvisa.models.FilterModel;
import com.supectco.easyvisa.models.GetCodeModel;
import com.supectco.easyvisa.models.MyOrdersModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Sajjad on 9/20/2016.
 */

public interface APIService {










//    @FormUrlEncoded
//    @POST("getTypeList.php")
//    Call<FilterModel> getTypeList(@Field("device") String c,
//                                  @Field("code") String method
//    );



    @GET("getTypeList.php")
    Call<FilterModel> getTypeList(@Query("device") String c,
                                  @Query("code") String method
    );




//    @FormUrlEncoded
//    @POST("getDataProductList.php")
//    Call<ProductListModel> getDataProduct(@Field("device") String c,
//                                          @Field("code") String method,
//                                          @Field("page") String page,
//                                          @Field("colorIds") String colorIds,
//                                          @Field("typeIds") String typeIds,
//                                          @Field("brandIds") String brandIds,
//                                          @Field("sortID") int sortID,
//                                          @Field("priorityID") int priorityID,
//                                          @Field("query") String quary
//    );


    @GET("getDataProductList.php")
    Call<ProductListModel> getDataProduct(@Query("device") String c,
                                          @Query("code") String method,
                                          @Query("page") String page,
                                          @Query("colorIds") String colorIds,
                                          @Query("typeIds") String typeIds,
                                          @Query("brandIds") String brandIds,
                                          @Query("sortID") int sortID,
                                          @Query("priorityID") int priorityID,
                                          @Query("catID") int catID,
                                          @Query("catLevel") int catLevel,
                                          @Query("query") String quary
    );





    @GET("getCats.php")
    Call<ChooseCatModel> getCats(@Query("device") String device,
                                 @Query("code") String code,
                                 @Query("ID") int ID,
                                 @Query("catLevel") int catLevel
    );





//    @FormUrlEncoded
//    @POST("getPhoto.php")
//    Call<PhotoModel> getPhoto(@Field("ID") int ID,
//                              @Field("device") String device,
//                              @Field("code") String method,
//                              @Field("page") String page
//
//    );



    @GET("getPhoto.php")
    Call<PhotoModel> getPhoto(@Query("ID") int ID,
                              @Query("device") String device,
                              @Query("code") String method,
                              @Query("page") String page

    );






//    @FormUrlEncoded
//    @POST("getDataMyOrders.php")
//    Call<MyOrdersModel> getDataMyOrders(@Field("device") String c,
//                                        @Field("code") String method,
//                                        @Field("user") String user
//    );


    @GET("getDatsaMyOrders.php")
    Call<MyOrdersModel> getDataMyOrders(@Query("device") String c,
                                        @Query("code") String method,
                                        @Query("user") String user
    );









//    @FormUrlEncoded
//    @POST("getCode.php")
//    Call<GetCodeModel> getCode(@Field("device") String device,
//                               @Field("code") String code,
//                               @Field("user") String user,
//                               @Field("activeCode") String activeCode
//    );




    @GET("getCode.php")
    Call<GetCodeModel> getCode(@Query("device") String device,
                               @Query("code") String code,
                               @Query("user") String user,
                               @Query("activeCode") String activeCode
    );




//    @FormUrlEncoded
//    @POST("sendCodeAgain.php")
//    Call<Boolean> sendCodeAgain(@Field("mobile") String device,
//                                @Field("device") String code,
//                                @Field("code") String user
//    );



    @GET("sendCodeAgain.php")
    Call<Boolean> sendCodeAgain(@Query("mobile") String device,
                                @Query("device") String code,
                                @Query("code") String user
    );




//    @FormUrlEncoded
//    @POST("viewProduct.php")
//    Call<ViewProductModel> getDataViewProduct(@Field("id") String id,
//                                              @Field("code") String method,
//                                              @Field("device") String device
//    );



    @GET("viewProduct.php")
    Call<ViewProductModel> getDataViewProduct(@Query("id") String id,
                                              @Query("code") String method,
                                              @Query("device") String device
    );



//    @FormUrlEncoded
//    @POST("buyRequest.php")
//    Call<BuyReqModel> req(@Field("device") String device,
//                          @Field("code") String code,
//                          @Field("fullname") String fullname,
//                          @Field("mobile") String mobile,
////                          @Field("email") String email,
////                          @Field("state") String state,
////                          @Field("city") String city,
//                          @Field("address") String address,
////                          @Field("postalCode") String postalCode,
////                          @Field("phone") String phone,
////                          @Field("phone2") String phone2,
//                          @Field("discount") String discount,
//                          @Field("ids") String ids,
//                          @Field("metraj") String metraj,
//                          @Field("imageNames") String imageNames
//    );




    @GET("buyRequest.php")
    Call<BuyReqModel> req(@Query("device") String device,
                          @Query("code") String code,
                          @Query("fullname") String fullname,
                          @Query("mobile") String mobile,
//                          @Query("email") String email,
                          @Query("state") String state,
                          @Query("city") String city,
                          @Query("address") String address,
                          @Query("postalCode") String postalCode,
//                          @Query("phone") String phone,
//                          @Query("phone2") String phone2,
                          @Query("discount") String discount,
                          @Query("ids") String ids,
                          @Query("metraj") String metraj,
                          @Query("imageNames") String imageNames
    );







//    @FormUrlEncoded
//    @POST("update.php")
//    Call<Update> checkNewVersion(@Field("code") String var1,
//                                 @Field("mobile") String var2,
//                                 @Field("username") String var3,
//                                 @Field("email") String var4,
//                                 @Field("api") String var5,
//                                 @Field("type") String var6,
//                                 @Field("device") String var7,
//                                 @Field("versionCode") String var8,
//                                 @Field("versionName") String var9,
//                                 @Field("osVersion") String var10,
//                                 @Field("model") String var11,
//                                 @Field("apiLevel") String var12,
//                                 @Field("lat") String var13,
//                                 @Field("lng") String var14,
//                                 @Field("market") int var15,
//                                 @Field("playerID") String var16);


    @GET("update.php")
    Call<Update> checkNewVersion(@Query("code") String var1,
                                 @Query("mobile") String var2,
                                 @Query("username") String var3,
                                 @Query("email") String var4,
                                 @Query("api") String var5,
                                 @Query("type") String var6,
                                 @Query("device") String var7,
                                 @Query("versionCode") String var8,
                                 @Query("versionName") String var9,
                                 @Query("osVersion") String var10,
                                 @Query("model") String var11,
                                 @Query("apiLevel") String var12,
                                 @Query("lat") String var13,
                                 @Query("lng") String var14,
                                 @Query("market") int var15,
                                 @Query("playerID") String var16);






//    @FormUrlEncoded
//    @POST("doSignIn.php")
//    Call<SignInModel> doSignIn(@Field("device") String device,
//                               @Field("code") String code,
//                               @Field("phone") String phone,
//                               @Field("password") String password
//    );



    @GET("doSignIn.php")
    Call<SignInModel> doSignIn(@Query("device") String device,
                               @Query("code") String code,
                               @Query("phone") String phone,
                               @Query("password") String password
    );





//    @FormUrlEncoded
//    @POST("aboutUs.php")
//    Call<AboutUsModel> aboutUs(@Field("device") String device,
//                               @Field("code") String code
//    );



    @GET("aboutUs.php")
    Call<AboutUsModel> aboutUs(@Query("device") String device,
                               @Query("code") String code
    );





    @FormUrlEncoded
    @POST("contactUs.php")
    Call<ContactUsModel> contactUs(@Field("device") String device,
                                   @Field("code") String code
    );


//    @FormUrlEncoded
//    @POST("doSignUp.php")
//    Call<SignUpModel> doSignUp(@Field("device") String device,
//                               @Field("code") String code,
//                               @Field("phone") String phone,
//                               @Field("password") String password
//    );



    @GET("doSignUp.php")
    Call<SignUpModel> doSignUp(@Query("device") String device,
                               @Query("code") String code,
                               @Query("phone") String phone,
                               @Query("password") String password
    );







    @GET("confirmUser.php")
    Call<ConfirmUserModel> confirmUser(@Query("device") String device,
                                       @Query("code") String code,
                                       @Query("mobile") String mobile

    );




//    @FormUrlEncoded
//    @POST("getDiscount.php")
//    Call<DiscountModel> getDiscount(@Field("device") String device,
//                                    @Field("code") String code,
//                                    @Field("discountCode") String discountCode,
//                                    @Field("mobile") String mobile
//    );


    @GET("getDiscount.php")
    Call<DiscountModel> getDiscount(@Query("device") String device,
                                    @Query("code") String code,
                                    @Query("discountCode") String discountCode,
                                    @Query("mobile") String mobile
    );





//
//    @FormUrlEncoded
//    @POST("changePass.php")
//    Call<ChangePassModel> changePass(@Field("device") String device,
//                                     @Field("code") String code,
//                                     @Field("password") String password,
//                                     @Field("mobile") String mobile,
//                                     @Field("activate_code") String activate_code
//    );


    @GET("changePass.php")
    Call<ChangePassModel> changePass(@Query("device") String device,
                                     @Query("code") String code,
                                     @Query("password") String password,
                                     @Query("mobile") String mobile,
                                     @Query("activate_code") String activate_code
    );



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////


    @GET("getMainData.php")
    Call<MainModel> getData(@Query("device") String device,
                            @Query("code") String code
    );


    @GET("getCountries.php")
    Call<GetCountriesModel> getCountries(@Query("device") String c,
                                         @Query("code") String method
    );



    @GET("update.php")
    Call<UpdateModel> update(@Query("device") String device,
                             @Query("code") String code,
                             @Query("model") String model,
                             @Query("osVersion") String osVersion,
                             @Query("minSdk") int minSdk,
                             @Query("versionCode") int versionCode,
                             @Query("versionName") String versionName,
                             @Query("os") int os,
                             @Query("mobile") String mobile,
                             @Query("token") String token,
                             @Query("latitude") double latitude,
                             @Query("longitude") double longitude


    );



}