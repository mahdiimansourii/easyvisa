package com.supectco.easyvisa.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.adapters.SimpleSpinnerAdapter;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyEditText;
import com.supectco.easyvisa.views.MyTextView;

import java.util.Arrays;

public class Form extends Master {

    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    RelativeLayout loading, error;
    Handler handler = new Handler();
    MyTextView retry;
    CardView crdSetDate;
    int ID = 0;
    ImageView flag;
    ImageView imageback;
    MyEditText fullName, passNumber, nationality, phoneNumber , COMD;
    ImageView imgPositionFullName, imgPositionCOMD, imgPositioncrdPPFS, imgPositionEVE ,imgPositionPassNumber, imgPositionPassNationality, imgPositionGender, imgPositionPassExpireDate, imgPositionBirthDate, imgPositionPhoneNumber, imgPositionDateOfTravel, imgPositionPrefrredAppoinment;
    MyTextView txtErrorFullName, txtErrorCOMD , txtErrorcrdPPFS , txtErrorEVE , txtErrorPassNumber, txtErrorPassNationality, txtErrorGender, txtErrorPassExpireDate, txtErrorBirthDate, txtErrorPhoneNumber, txtErrorDateOfTravel, txtErrorPrefrredAppoinment;
    ImageView imgFlag;
    Spinner ExDateSpinner1, ExDateSpinner2, ExDateSpinner3, BirthDateSpinner1, BirthDateSpinner2, BirthDateSpinner3, TravelSpinner1, TravelSpinner2, TravelSpinner3, PDOASpinner1, PDOASpinner2, PDOASpinner3;
    public final static String LATIN_STRING = "a A b B c C d D e E f F g G h H i I j k K l L m M n N o O p P q Q r R s S t T u U v V w W x X y Y z Z";
    ScrollView ScrMain;
    RadioButton man, woman , place1 , place2 , dest1, dest2;
    RadioGroup radio_group , radio_group_palce , radio_group_destnation;
    boolean isFirstpassExDate = true, isFirstBirthDate = true;
    int intExDatePos = 0, intBirthDatePos = 0;
    RadioButton rdot1, rdot2, rdot3, rdot4, rdot5, rdot6, rdot7, rdot8, rdot9, rdot10;
    MyTextView txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10;
    CardView crdCOMD , crdPPFS , crdEVE;
    LinearLayout checkCOMD , lylDestnation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formx);

        Bundle extrass = getIntent().getExtras();
        if (extrass != null) {
            ID = extrass.getInt("ID");
        }


        initializeToolbarx();
        initializeDrawer();
        initializeLayout();
        confirmPrice();


    }


    public void initializeLayout() {
        imageback = findViewById(R.id.backdrop);
        imgFlag = (ImageView) findViewById(R.id.imgFlag);
        Picasso.with(getApplicationContext()).load(G.SLIDES_PATH + G.CountryImagex).fit().centerCrop().into(imageback);
        Picasso.with(getApplicationContext()).load(G.SLIDES_PATH + G.CountryImage).fit().centerCrop().into(imgFlag);
        txtTitle.setText("دریافت وقت مصاحبه سفارت " + G.CountryName);


        txtErrorFullName = (MyTextView) findViewById(R.id.txtErrorFullName);
        txtErrorPassNumber = (MyTextView) findViewById(R.id.txtErrorPassNumber);
        txtErrorPassNationality = (MyTextView) findViewById(R.id.txtErrorNationality);
        txtErrorGender = (MyTextView) findViewById(R.id.txtErrorGender);
        txtErrorPassExpireDate = (MyTextView) findViewById(R.id.txtErrorPassExpireDate);
        txtErrorBirthDate = (MyTextView) findViewById(R.id.txtErrorBirthDate);
        txtErrorPhoneNumber = (MyTextView) findViewById(R.id.txtErrorPhoneNumber);
        txtErrorDateOfTravel = (MyTextView) findViewById(R.id.txtErrorDateOfTravel);
        txtErrorPrefrredAppoinment = (MyTextView) findViewById(R.id.txtErrorPrefrredAppoinment);
        txtErrorCOMD = (MyTextView) findViewById(R.id.txtErrorCOMD);
        txtErrorEVE = (MyTextView) findViewById(R.id.txtErrorEVE);


        txt1 = (MyTextView) findViewById(R.id.txt1);
        txt2 = (MyTextView) findViewById(R.id.txt2);
        txt3 = (MyTextView) findViewById(R.id.txt3);
        txt4 = (MyTextView) findViewById(R.id.txt4);
        txt5 = (MyTextView) findViewById(R.id.txt5);
        txt6 = (MyTextView) findViewById(R.id.txt6);
        txt7 = (MyTextView) findViewById(R.id.txt7);
        txt8 = (MyTextView) findViewById(R.id.txt8);
        txt9 = (MyTextView) findViewById(R.id.txt9);
        txt10 = (MyTextView) findViewById(R.id.txt10);


        rdot1 = (RadioButton) findViewById(R.id.rdot1);
        rdot2 = (RadioButton) findViewById(R.id.rdot2);
        rdot3 = (RadioButton) findViewById(R.id.rdot3);
        rdot4 = (RadioButton) findViewById(R.id.rdot4);
        rdot5 = (RadioButton) findViewById(R.id.rdot5);
        rdot6 = (RadioButton) findViewById(R.id.rdot6);
        rdot7 = (RadioButton) findViewById(R.id.rdot7);
        rdot8 = (RadioButton) findViewById(R.id.rdot8);
        rdot9 = (RadioButton) findViewById(R.id.rdot9);
        rdot10 = (RadioButton) findViewById(R.id.rdot10);


        fullName = (MyEditText) findViewById(R.id.fullName);
        passNumber = (MyEditText) findViewById(R.id.passNumber);
        nationality = (MyEditText) findViewById(R.id.nationality);
        phoneNumber = (MyEditText) findViewById(R.id.phoneNumber);
        COMD = (MyEditText) findViewById(R.id.COMD);

        imgPositionFullName = (ImageView) findViewById(R.id.imgPositionFullName);
        imgPositionPassNumber = (ImageView) findViewById(R.id.imgPositionPassNumber);
        imgPositionPassNationality = (ImageView) findViewById(R.id.imgPositionNationality);
        imgPositionGender = (ImageView) findViewById(R.id.imgPositionGender);
        imgPositionPassExpireDate = (ImageView) findViewById(R.id.imgPositionPassExpireDate);
        imgPositionBirthDate = (ImageView) findViewById(R.id.imgPositionBirthDate);
        imgPositionPhoneNumber = (ImageView) findViewById(R.id.imgPositionPhoneNumber);
        imgPositionDateOfTravel = (ImageView) findViewById(R.id.imgPositionDateOfTravel);
        imgPositionPrefrredAppoinment = (ImageView) findViewById(R.id.imgPositionPrefrredAppoinment);
        imgPositionCOMD = (ImageView) findViewById(R.id.imgPositionCOMD);
        imgPositioncrdPPFS = (ImageView) findViewById(R.id.imgPositioncrdPPFS);
        imgPositionEVE = (ImageView) findViewById(R.id.imgPositionEVE);

        ExDateSpinner1 = (Spinner) findViewById(R.id.ExDateSpinner1);
        ExDateSpinner2 = (Spinner) findViewById(R.id.ExDateSpinner2);
        ExDateSpinner3 = (Spinner) findViewById(R.id.ExDateSpinner3);
        BirthDateSpinner1 = (Spinner) findViewById(R.id.BirthDateSpinner1);
        BirthDateSpinner2 = (Spinner) findViewById(R.id.BirthDateSpinner2);
        BirthDateSpinner3 = (Spinner) findViewById(R.id.BirthDateSpinner3);
        TravelSpinner1 = (Spinner) findViewById(R.id.TravelSpinner1);
        TravelSpinner2 = (Spinner) findViewById(R.id.TravelSpinner2);
        TravelSpinner3 = (Spinner) findViewById(R.id.TravelSpinner3);
        PDOASpinner1 = (Spinner) findViewById(R.id.PDOASpinner1);
        PDOASpinner2 = (Spinner) findViewById(R.id.PDOASpinner2);
        PDOASpinner3 = (Spinner) findViewById(R.id.PDOASpinner3);


        ScrMain = (ScrollView) findViewById(R.id.ScrMain);

        man = (RadioButton) findViewById(R.id.man);
        woman = (RadioButton) findViewById(R.id.woman);
        place1 = (RadioButton) findViewById(R.id.place1);
        place2 = (RadioButton) findViewById(R.id.place2);
        dest1 = (RadioButton) findViewById(R.id.dest1);
        dest2 = (RadioButton) findViewById(R.id.dest2);
        radio_group = (RadioGroup) findViewById(R.id.radio_group);
        radio_group_palce = (RadioGroup) findViewById(R.id.radio_group_palce);
        radio_group_destnation = (RadioGroup) findViewById(R.id.radio_group_destnation);


        crdCOMD = (CardView) findViewById(R.id.crdCOMD);
        crdPPFS = (CardView) findViewById(R.id.crdPPFS);
        crdEVE = (CardView) findViewById(R.id.crdEVE);

        checkCOMD = (LinearLayout) findViewById(R.id.checkCOMD);
        lylDestnation = (LinearLayout) findViewById(R.id.lylDestnation);


        fullName.addTextChangedListener(new GenericTextWatcher(fullName));
        passNumber.addTextChangedListener(new GenericTextWatcher(passNumber));
        nationality.addTextChangedListener(new GenericTextWatcher(nationality));
        phoneNumber.addTextChangedListener(new GenericTextWatcher(phoneNumber));


        radio_group.clearCheck();

        man.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((RadioButton) v).isChecked()) {

                    imgPositionGender.setVisibility(View.VISIBLE);
                    txtErrorGender.setVisibility(View.GONE);
                    imgPositionGender.setImageResource(R.drawable.icon_success);

                }
            }
        });

        woman.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((RadioButton) v).isChecked()) {

                    imgPositionGender.setVisibility(View.VISIBLE);
                    txtErrorGender.setVisibility(View.GONE);
                    imgPositionGender.setImageResource(R.drawable.icon_success);

                }
            }
        });


        ExDateSpinner1.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_value1)));
        ExDateSpinner1.setSelection(0);
        ExDateSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgPositionPassExpireDate.setVisibility(View.VISIBLE);
                txtErrorPassExpireDate.setVisibility(View.GONE);
                imgPositionPassExpireDate.setImageResource(R.drawable.icon_success);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        ExDateSpinner2.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_value2)));
        ExDateSpinner2.setSelection(0);
        ExDateSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgPositionPassExpireDate.setVisibility(View.VISIBLE);
                txtErrorPassExpireDate.setVisibility(View.GONE);
                imgPositionPassExpireDate.setImageResource(R.drawable.icon_success);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        ExDateSpinner3.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_value3)));
        ExDateSpinner3.setSelection(0);
        ExDateSpinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgPositionPassExpireDate.setVisibility(View.VISIBLE);
                txtErrorPassExpireDate.setVisibility(View.GONE);
                imgPositionPassExpireDate.setImageResource(R.drawable.icon_success);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        BirthDateSpinner1.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_birthday_value1)));
        BirthDateSpinner1.setSelection(0);
        BirthDateSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                imgPositionBirthDate.setVisibility(View.VISIBLE);
                txtErrorBirthDate.setVisibility(View.GONE);
                imgPositionBirthDate.setImageResource(R.drawable.icon_success);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        BirthDateSpinner2.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_value2)));
        BirthDateSpinner2.setSelection(0);
        BirthDateSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgPositionBirthDate.setVisibility(View.VISIBLE);
                txtErrorBirthDate.setVisibility(View.GONE);
                imgPositionBirthDate.setImageResource(R.drawable.icon_success);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        BirthDateSpinner3.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_value3)));
        BirthDateSpinner3.setSelection(0);
        BirthDateSpinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgPositionBirthDate.setVisibility(View.VISIBLE);
                txtErrorBirthDate.setVisibility(View.GONE);
                imgPositionBirthDate.setImageResource(R.drawable.icon_success);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        TravelSpinner1.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_value1)));
        TravelSpinner1.setSelection(0);
        TravelSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgPositionDateOfTravel.setVisibility(View.VISIBLE);
                txtErrorDateOfTravel.setVisibility(View.GONE);
                imgPositionDateOfTravel.setImageResource(R.drawable.icon_success);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        TravelSpinner2.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_value2)));
        TravelSpinner2.setSelection(0);
        TravelSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgPositionDateOfTravel.setVisibility(View.VISIBLE);
                txtErrorDateOfTravel.setVisibility(View.GONE);
                imgPositionDateOfTravel.setImageResource(R.drawable.icon_success);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        TravelSpinner3.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_value3)));
        TravelSpinner3.setSelection(0);
        TravelSpinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgPositionDateOfTravel.setVisibility(View.VISIBLE);
                txtErrorDateOfTravel.setVisibility(View.GONE);
                imgPositionDateOfTravel.setImageResource(R.drawable.icon_success);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        PDOASpinner1.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_value1)));
        PDOASpinner1.setSelection(0);
        PDOASpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgPositionPrefrredAppoinment.setVisibility(View.VISIBLE);
                txtErrorPrefrredAppoinment.setVisibility(View.GONE);
                imgPositionPrefrredAppoinment.setImageResource(R.drawable.icon_success);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        PDOASpinner2.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_value2)));
        PDOASpinner2.setSelection(0);
        PDOASpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgPositionPrefrredAppoinment.setVisibility(View.VISIBLE);
                txtErrorPrefrredAppoinment.setVisibility(View.GONE);
                imgPositionPrefrredAppoinment.setImageResource(R.drawable.icon_success);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        PDOASpinner3.setAdapter(new SimpleSpinnerAdapter(getApplicationContext(), getResources().getStringArray(R.array.spinner_value3)));
        PDOASpinner3.setSelection(0);
        PDOASpinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                imgPositionPrefrredAppoinment.setVisibility(View.VISIBLE);
                txtErrorPrefrredAppoinment.setVisibility(View.GONE);
                imgPositionPrefrredAppoinment.setImageResource(R.drawable.icon_success);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        switch (ID) {

            case 1: {


                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.VISIBLE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.VISIBLE);
                COMD.addTextChangedListener(new GenericTextWatcher(COMD));
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.GONE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.VISIBLE);
                rdot5.setVisibility(View.VISIBLE);
                rdot6.setVisibility(View.VISIBLE);
                rdot7.setVisibility(View.GONE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("Tourism");
                txt2.setText("Business");
                txt3.setText("Visiting Family And Friends");
                txt4.setText("Transit");
                txt5.setText("Return Visa");
                txt6.setText("Family Members Of Eu/Eeanationals");

                break;
            }
            case 2: {


                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.GONE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.VISIBLE);
                COMD.addTextChangedListener(new GenericTextWatcher(COMD));
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.GONE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.VISIBLE);
                rdot5.setVisibility(View.VISIBLE);
                rdot6.setVisibility(View.VISIBLE);
                rdot7.setVisibility(View.GONE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("France");
                txt2.setText("France");
                txt3.setText("France");
                txt4.setText("France");
                txt5.setText("France");
                txt6.setText("France");


                break;
            }

            case 3: {


                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.GONE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.VISIBLE);
                COMD.addTextChangedListener(new GenericTextWatcher(COMD));
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.GONE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.VISIBLE);
                rdot5.setVisibility(View.VISIBLE);
                rdot6.setVisibility(View.VISIBLE);
                rdot7.setVisibility(View.GONE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("Canada");
                txt2.setText("Canada");
                txt3.setText("Canada");
                txt4.setText("Canada");
                txt5.setText("Canada");
                txt6.setText("Canada");

                break;
            }

            case 4: {

                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.GONE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.VISIBLE);
                COMD.addTextChangedListener(new GenericTextWatcher(COMD));
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.GONE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.VISIBLE);
                rdot5.setVisibility(View.VISIBLE);
                rdot6.setVisibility(View.VISIBLE);
                rdot7.setVisibility(View.GONE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("Australia");
                txt2.setText("Australia");
                txt3.setText("Australia");
                txt4.setText("Australia");
                txt5.setText("Australia");
                txt6.setText("Australia");

                break;
            }

            case 5: {

                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.GONE);
                crdEVE.setVisibility(View.VISIBLE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.VISIBLE);
                COMD.addTextChangedListener(new GenericTextWatcher(COMD));
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.GONE);
                txt5.setVisibility(View.GONE);
                txt6.setVisibility(View.GONE);
                txt7.setVisibility(View.GONE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.GONE);
                rdot5.setVisibility(View.GONE);
                rdot6.setVisibility(View.GONE);
                rdot7.setVisibility(View.GONE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("Business");
                txt2.setText("Tourism/ Others");
                txt3.setText("Visiting Family and Friends");
                break;
            }

            case 6: {


                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.GONE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.GONE);
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.VISIBLE);
                txt8.setVisibility(View.VISIBLE);
                txt9.setVisibility(View.VISIBLE);
                txt10.setVisibility(View.VISIBLE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.VISIBLE);
                rdot5.setVisibility(View.VISIBLE);
                rdot6.setVisibility(View.VISIBLE);
                rdot7.setVisibility(View.VISIBLE);
                rdot8.setVisibility(View.VISIBLE);
                rdot9.setVisibility(View.VISIBLE);
                rdot10.setVisibility(View.VISIBLE);
                txt1.setText("Business");
                txt2.setText("EEA/EU Nationals");
                txt3.setText("Minors");
                txt4.setText("Conference");
                txt5.setText("Cultural and Sports");
                txt6.setText("Educational, Scientific and Professional");
                txt7.setText("Training");
                txt8.setText("Visit");
                txt9.setText("Visiting Students");
                txt10.setText("Parents Accompanying");

                break;
            }

            case 7: {

                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.GONE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.GONE);
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.VISIBLE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.VISIBLE);
                rdot5.setVisibility(View.VISIBLE);
                rdot6.setVisibility(View.VISIBLE);
                rdot7.setVisibility(View.VISIBLE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("Business (Short Stay)");
                txt2.setText("Tourism / Others (Short Stay)");
                txt3.setText("Visiting Family and Friends (Short Stay)");
                txt4.setText("Study Visa (Short Stay)");
                txt5.setText("Family Reunion (Residence Permit – Long Stay)");
                txt6.setText("Work Permit (Residence Permit – Long Stay)");
                txt7.setText("Study Visa (Residence Permit – Long Stay)");


                break;
            }
            case 8: {

                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.GONE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.GONE);
                txt1.setVisibility(View.GONE);
                txt2.setVisibility(View.GONE);
                txt3.setVisibility(View.GONE);
                txt4.setVisibility(View.GONE);
                txt5.setVisibility(View.GONE);
                txt6.setVisibility(View.GONE);
                txt7.setVisibility(View.GONE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.GONE);
                rdot4.setVisibility(View.GONE);
                rdot5.setVisibility(View.GONE);
                rdot6.setVisibility(View.GONE);
                rdot7.setVisibility(View.GONE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("VISA TYPE C (TOURISM, UP TO 90 DAYS WITHIN 6 MONTHS)");
                txt2.setText("VISA TYPE D (SPECIFIC PURPOSES LIMITED TO GREECE ONLY)");


                break;
            }
            case 9: {

                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.GONE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.GONE);
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.GONE);
                txt7.setVisibility(View.GONE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.VISIBLE);
                rdot5.setVisibility(View.VISIBLE);
                rdot6.setVisibility(View.GONE);
                rdot7.setVisibility(View.GONE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("Business");
                txt2.setText("Tourism");
                txt3.setText("Visiting Family and Friends");
                txt4.setText("Sports");
                txt5.setText("Cultural");

                break;
            }
            case 10: {
                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.GONE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.GONE);
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.GONE);
                txt7.setVisibility(View.GONE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.VISIBLE);
                rdot5.setVisibility(View.VISIBLE);
                rdot6.setVisibility(View.GONE);
                rdot7.setVisibility(View.GONE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("South Africa");
                txt2.setText("South Africa");
                txt3.setText("South Africa");
                txt4.setText("South Africa");
                txt5.setText("South Africa");

                break;
            }
            case 11: {

                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.GONE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.GONE);
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.GONE);
                txt5.setVisibility(View.GONE);
                txt6.setVisibility(View.GONE);
                txt7.setVisibility(View.GONE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.GONE);
                rdot5.setVisibility(View.GONE);
                rdot6.setVisibility(View.GONE);
                rdot7.setVisibility(View.GONE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("Non Family Visit");
                txt2.setText("Family Visit");
                txt3.setText("Residence Permit");

                break;
            }
            case 12: {

                COMD.setVisibility(View.GONE);
                lylDestnation.setVisibility(View.GONE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.VISIBLE);
                checkCOMD.setVisibility(View.VISIBLE);
                crdCOMD.setVisibility(View.GONE);
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.GONE);
                txt6.setVisibility(View.GONE);
                txt7.setVisibility(View.GONE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.VISIBLE);
                rdot5.setVisibility(View.GONE);
                rdot6.setVisibility(View.GONE);
                rdot7.setVisibility(View.GONE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("Business / Official");
                txt2.setText("Tourism");
                txt3.setText("Private Visit");
                txt4.setText("Conference / Scientific  Research & Study");


                radio_group_palce.clearCheck();

                place1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (((RadioButton) v).isChecked()) {

                            imgPositioncrdPPFS.setVisibility(View.VISIBLE);
                            txtErrorcrdPPFS.setVisibility(View.GONE);
                            imgPositioncrdPPFS.setImageResource(R.drawable.icon_success);

                        }
                    }
                });

                place1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (((RadioButton) v).isChecked()) {

                            imgPositioncrdPPFS.setVisibility(View.VISIBLE);
                            txtErrorcrdPPFS.setVisibility(View.GONE);
                            imgPositioncrdPPFS.setImageResource(R.drawable.icon_success);

                        }
                    }
                });


                break;
            }
            case 13: {

                COMD.setVisibility(View.GONE);
                crdEVE.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.GONE);
                crdPPFS.setVisibility(View.GONE);
                checkCOMD.setVisibility(View.GONE);
                crdCOMD.setVisibility(View.GONE);
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.GONE);
                txt7.setVisibility(View.GONE);
                txt8.setVisibility(View.GONE);
                txt9.setVisibility(View.GONE);
                txt10.setVisibility(View.GONE);
                rdot1.setVisibility(View.VISIBLE);
                rdot2.setVisibility(View.VISIBLE);
                rdot3.setVisibility(View.VISIBLE);
                rdot4.setVisibility(View.VISIBLE);
                rdot5.setVisibility(View.VISIBLE);
                rdot6.setVisibility(View.GONE);
                rdot7.setVisibility(View.GONE);
                rdot8.setVisibility(View.GONE);
                rdot9.setVisibility(View.GONE);
                rdot10.setVisibility(View.GONE);
                txt1.setText("Cyprus");
                txt2.setText("Cyprus");
                txt3.setText("Cyprus");
                txt4.setText("Cyprus");
                txt5.setText("Cyprus");

                break;
            }


        }


    }


    public void confirmPrice() {


        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirm_price);


        final MyTextView yes = (MyTextView) dialog.findViewById(R.id.yes);
        MyTextView no = (MyTextView) dialog.findViewById(R.id.no);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });


        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        dialog.setCancelable(false);
        dialog.show();


    }


    String mobilenumber = "";

    public class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }


        public boolean latinChar(String str) {

            if (str.length() > 0) {
                if (LATIN_STRING.contains(str.charAt(0) + "")) {

                    return true;
                } else {

                    return false;
                }
            } else {

                return false;
            }

        }


        public void doValidate(CharSequence cs, int num, ImageView img, TextView txt) {


            if (cs.toString().length() < num || !latinChar(cs.toString())) {

                img.setVisibility(View.VISIBLE);
                txt.setVisibility(View.VISIBLE);
                img.setImageResource(R.drawable.icon_error);

            } else {

                img.setVisibility(View.VISIBLE);
                txt.setVisibility(View.GONE);
                img.setImageResource(R.drawable.icon_success);
            }


        }


        public void doValidateNumber(CharSequence cs, int num, ImageView img, TextView txt) {


            if (cs.toString().length() > num) {
                img.setVisibility(View.VISIBLE);
                txt.setVisibility(View.GONE);
                img.setImageResource(R.drawable.icon_success);
            } else {
                img.setVisibility(View.VISIBLE);
                txt.setVisibility(View.VISIBLE);
                img.setImageResource(R.drawable.icon_error);

            }


        }


        public void doValidatex(CharSequence cs, ImageView img, MyTextView txt) {

            final String[] nums = {"0"};
            if (cs.toString().length() > 0) {
                mobilenumber = cs.toString().substring(0, 1);
            }

            if ((cs.toString().length() >= 1 && cs.toString().length() < 11) || (!Arrays.asList(nums).contains(mobilenumber))) {

                img.setVisibility(View.VISIBLE);
                txt.setVisibility(View.VISIBLE);
                img.setImageResource(R.drawable.icon_error);

            } else {

                img.setVisibility(View.VISIBLE);
                txt.setVisibility(View.GONE);
                img.setImageResource(R.drawable.icon_success);
            }


        }


        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            switch (view.getId()) {
                case R.id.fullName:
                    doValidate(charSequence, 4, imgPositionFullName, txtErrorFullName);
                    break;
                case R.id.passNumber:
                    doValidateNumber(charSequence, 8, imgPositionPassNumber, txtErrorPassNumber);
                    break;
                case R.id.nationality:
                    doValidate(charSequence, 2, imgPositionPassNationality, txtErrorPassNationality);
                    break;
                case R.id.phoneNumber:
                    doValidateNumber(charSequence, 11, imgPositionPhoneNumber, txtErrorPhoneNumber);
                    break;
                case R.id.COMD:
                    doValidate(charSequence, 2, imgPositionCOMD, txtErrorCOMD);
                    break;
//                case R.id.address:
//                    doValidate(charSequence, 8, imgPositionAddress, txtErrorAddress);
//                    break;
//                case R.id.phone:
//                    doValidatex(charSequence,imgPositionPhone,txtErrorPhone);
//                    break;
//                case R.id.phone2:
//                    doValidatex(charSequence,imgPositionPhone2,txtErrorPhone2);
//                    break;
//                case R.id.phone3:
//                    doValidatex(charSequence,imgPositionPhone3,txtErrorPhone3);
//                    break;
//                case R.id.telegram:
//                    doValidate(charSequence, 6, imgPositionTelegram, txtErrorTelegram);
//                    break;
//                case R.id.insta:
//                    doValidate(charSequence, 6, imgPositionInsta, txtErrorInsta);
//                    break;
//                case R.id.email:
//                    if ((G.isEmailValid(charSequence.toString()) == false) || (charSequence.toString().length() <= 10)) {
//                        imgPositionEmail.setVisibility(View.VISIBLE);
//                        txtErrorEmail.setVisibility(View.VISIBLE);
//                        imgPositionEmail.setImageResource(R.drawable.icon_error);
//                    } else {
//                        imgPositionEmail.setVisibility(View.VISIBLE);
//                        txtErrorEmail.setVisibility(View.GONE);
//                        imgPositionEmail.setImageResource(R.drawable.icon_success);
//                    }
//                    break;
//                case R.id.website:
//                    if ((!(charSequence.toString().contains("."))) || (charSequence.toString().length() <= 8)) {
//                        imgPositionWebSite.setVisibility(View.VISIBLE);
//                        txtErrorWebSite.setVisibility(View.VISIBLE);
//                        imgPositionWebSite.setImageResource(R.drawable.icon_error);
//                    } else {
//                        imgPositionWebSite.setVisibility(View.VISIBLE);
//                        txtErrorWebSite.setVisibility(View.GONE);
//                        imgPositionWebSite.setImageResource(R.drawable.icon_success);
//                    }
//                    break;
            }

        }

        public void afterTextChanged(Editable editable) {

        }

    }


    public void showError() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    error.setVisibility(View.VISIBLE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (G.isInternetAvailable() == false) {
                                showError();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), G.cl);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });
    }


}
