package com.supectco.easyvisa.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stephentuso.welcome.WelcomeHelper;
import com.supectco.easyvisa.adapters.BestsellerAdapter;
import com.supectco.easyvisa.adapters.ImageSlider_Adapter;
import com.supectco.easyvisa.adapters.MainCatAdapter;
import com.supectco.easyvisa.adapters.NewestAdapter;
import com.supectco.easyvisa.adapters.SpecialAdapter;
import com.supectco.easyvisa.models.MainModel;
import com.supectco.easyvisa.models.Update;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.R;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends Master {

     WelcomeHelper welcomeScreen;
    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    //    String[] slides = new String[5];
    ArrayList<String> slidesArray = new ArrayList<>();
    ImageSlider_Adapter imageSlider_adapter;
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private int currentPage = 1, NUM_PAGES;
    private Timer swipeTimer = null;
    private MainModel mainModel;
    RelativeLayout loading, error;
    RecyclerView newest_rv, bestseller_rv, special_rv , cat_rv;
    private LinearLayoutManager llm, llmx, llmxx , llmCat;
    NewestAdapter rv_Adapter;
    BestsellerAdapter rvx_Adapter;
    SpecialAdapter rvxx_Adapter;
    MainCatAdapter mainCatAdapter;
    LinearLayout lyl1, lyl2, lyl3;
    Handler handler = new Handler();
    MyTextView retry;
    CardView crdMore;
    Handler handlerx = new Handler();
    LocationManager locationManager;
    double Latitude, Longtitude;
    PackageManager manager;
    PackageInfo info;
    String VERSION_NAME;
    int VERSION_CODE;
    Update userInfoModel;
    boolean validUser, isShowMessage, mandatoryUpdate;
    String message, link, title, buttonText1, buttonText2;
    ImageView imgBanner1, imgBanner2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        welcomeScreen = new WelcomeHelper(this, Intro.class);
        welcomeScreen.show(savedInstanceState);
        initializeToolbar();
        initializeDrawer();
        initializeLayout();
        initializeData();

//        handlerx.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//        checkNewVersion();
//            }
//        },500);


    }


    public void initializeLayout() {


        newest_rv = (RecyclerView) findViewById(R.id.newest_rv);
        bestseller_rv = (RecyclerView) findViewById(R.id.bestseller_rv);
        special_rv = (RecyclerView) findViewById(R.id.special_rv);
        cat_rv = (RecyclerView) findViewById(R.id.cat_rv);
        loading = (RelativeLayout) findViewById(R.id.loading);
        error = (RelativeLayout) findViewById(R.id.error);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        lyl1 = (LinearLayout) findViewById(R.id.lyl1);
        lyl2 = (LinearLayout) findViewById(R.id.lyl2);
        lyl3 = (LinearLayout) findViewById(R.id.lyl3);
        retry = (MyTextView) findViewById(R.id.retry);
        crdMore = (CardView) findViewById(R.id.crdMore);
        imgBanner1 = (ImageView) findViewById(R.id.imgBanner1);
        imgBanner2 = (ImageView) findViewById(R.id.imgBanner2);


        newest_rv.setHasFixedSize(true);
        llm = new LinearLayoutManager(G.context.getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        newest_rv.setLayoutManager(llm);

        bestseller_rv.setHasFixedSize(true);
        llmx = new LinearLayoutManager(G.context.getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        bestseller_rv.setLayoutManager(llmx);


        special_rv.setHasFixedSize(true);
        llmxx = new LinearLayoutManager(G.context.getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        special_rv.setLayoutManager(llmxx);


        cat_rv.setHasFixedSize(true);
        llmCat = new LinearLayoutManager(G.context.getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        cat_rv.setLayoutManager(llmCat);



        crdMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                G.colorIDs.clear();
                G.typeIDs.clear();
                G.brandIDs.clear();
                Intent intent = new Intent(getApplicationContext(), ProductList.class);
                intent.putExtra("priorityID", 1);
                intent.putExtra("catName", "همه محصولات");
                startActivity(intent);

            }
        });


        lyl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                G.colorIDs.clear();
                G.typeIDs.clear();
                G.brandIDs.clear();
                Intent intent = new Intent(getApplicationContext(), ProductList.class);
                intent.putExtra("priorityID", 6);
                intent.putExtra("catName", "جدیدترین");
                startActivity(intent);

            }
        });


        lyl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                G.colorIDs.clear();
                G.typeIDs.clear();
                G.brandIDs.clear();
                Intent intent = new Intent(getApplicationContext(), ProductList.class);
                intent.putExtra("priorityID", 2);
                intent.putExtra("catName", "پر فروش ترین");
                startActivity(intent);

            }
        });


        lyl3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                G.colorIDs.clear();
                G.typeIDs.clear();
                G.brandIDs.clear();
                Intent intent = new Intent(getApplicationContext(), ProductList.class);
                intent.putExtra("priorityID", 3);
                intent.putExtra("catName", "پیشنهاد ویژه");
                startActivity(intent);

            }
        });


    }


    private void initializeData() {

        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        loading.setVisibility(View.VISIBLE);
        slidesArray.clear();

        Call<MainModel> call = apiService.getData(G.deviceID, G.md5(G.deviceID + "ncase8934f49909"));

        G.Log("call:" + call.request().url());

        call.enqueue(new Callback<MainModel>() {
            @Override
            public void onResponse(Call<MainModel> call, final Response<MainModel> response) {
                G.Log(" S request address : " + call.request().toString());
                if (response.isSuccessful()) {
                    G.Log("initilizeData  isSuccessful ");
                    mainModel = response.body();
                    rv_Adapter = new NewestAdapter();
                    rvx_Adapter = new BestsellerAdapter();
                    rvxx_Adapter = new SpecialAdapter();
                    mainCatAdapter = new MainCatAdapter();
                    newest_rv.setAdapter(rv_Adapter);
                    bestseller_rv.setAdapter(rvx_Adapter);
                    special_rv.setAdapter(rvxx_Adapter);
                    cat_rv.setAdapter(mainCatAdapter);
                    llmCat.setReverseLayout(true);



                    if (response.body().newest != null) {
                        for (MainModel.Newest item : response.body().newest) {
                            rv_Adapter.add(item);
                        }
                    }


                    if (response.body().bestseller != null) {
                        for (MainModel.Bestseller item : response.body().bestseller) {
                            rvx_Adapter.add(item);
                        }
                    }

                    if (response.body().specialOffers != null) {
                        for (MainModel.SpecialOffer item : response.body().specialOffers) {
                            rvxx_Adapter.add(item);
                        }
                    }


                    if (response.body().cats != null) {
                        for (MainModel.Cats item : response.body().cats) {
                            mainCatAdapter.add(item);
                        }
                    }




//                    handlerx.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//
//                        }
//                    },1000);

                    G.loadImage(G.IMG_PATH, mainModel.getBanner1(), imgBanner1, false);
                    G.loadImage(G.IMG_PATH, mainModel.getBanner2(), imgBanner2, false);

                    loading.setVisibility(View.GONE);

                    try {
                        for (int i = 0; i < mainModel.getSlides().size(); i++) {
                            slidesArray.add(mainModel.getSlides().get(i).getImage());
                            G.Log(mainModel.getSlides().get(i).getImage());
//                            slides[i] = viewProductModel.getSlides().get(i).getImage();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        G.Log("CRASH FROM VIEW PAGER");
                    }
//                    try{
//                        for(int i =0 ; i<mainModel.getSlides().size() ; i++){
//                            slides[i] =  mainModel.getSlides().get(i).getImage();
//                        }
//                    }catch (Exception e){
//                        e.printStackTrace();
//                        G.Log("CRASH FROM VIEW PAGER");
//                    }
                    viewPager();


                }


            }

            @Override
            public void onFailure(Call<MainModel> call, Throwable t) {
                showError();
            }
        });


    }


    public void viewPager() {

        ArrayList<String> ImagesArraySlide = new ArrayList<>();

        for (int i = 0; i < slidesArray.size(); i++) {
            if (slidesArray.get(i).length() > 5) {
                ImagesArraySlide.add(slidesArray.get(i));
            }
        }

//        for(int i = 0 ; i<5 ; i++){
//            if(slides[i].length()>10) {
//                ImagesArraySlide.add(slides[i]);
//            }
//        }

        imageSlider_adapter = new ImageSlider_Adapter(MainActivity.this, ImagesArraySlide);
        viewPager.setAdapter(imageSlider_adapter);
        indicator.setViewPager(viewPager);
        try {
            NUM_PAGES = ImagesArraySlide.size();
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    viewPager.setCurrentItem(currentPage++, true);
                }
            };
            if (swipeTimer == null) {
                swipeTimer = new Timer();
                swipeTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(Update);
                    }
                }, 3000, 3000);
            }
            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;
                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int pos) {
                }
            });
        } catch (Exception e) {
        }

    }


    public void showError() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    error.setVisibility(View.VISIBLE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (G.isInternetAvailable() == false) {
                                showError();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), G.cl);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });
    }


    public void checkNewVersion() {

        try {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            //Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (Latitude == 0 && Longtitude == 0) {
                Latitude = locationNet.getLatitude();
                Longtitude = locationNet.getLongitude();
            }
            manager = getApplicationContext().getPackageManager();
            info = manager.getPackageInfo(getApplicationContext().getPackageName(), 0);
            VERSION_CODE = info.versionCode;
            VERSION_NAME = info.versionName;


        } catch (Exception e) {
            e.printStackTrace();
        }


        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        Call<Update> call = apiService.checkNewVersion(G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd"), "", "", "", "", "1", G.deviceID, VERSION_CODE + "", VERSION_NAME + "", Build.VERSION.RELEASE, Build.MODEL, String.valueOf(Build.VERSION.SDK_INT), Latitude + "", Longtitude + "", 101, "");

        G.Log("call: " + call.request().url());

        call.enqueue(new Callback<Update>() {
            @Override
            public void onResponse(Call<Update> call, Response<Update> response) {

                try {
                    userInfoModel = response.body();

                    isShowMessage = userInfoModel.isShowMessage();
                    mandatoryUpdate = userInfoModel.isMandatoryUpdate();
                    message = userInfoModel.getMessage();
                    title = userInfoModel.getTitle();
                    buttonText1 = userInfoModel.getButtonText1();
                    buttonText2 = userInfoModel.getButtonText2();
                    link = userInfoModel.getLink();

                    if (response.isSuccessful()) {
                        if (isShowMessage) {
                            updateApp(title, message, mandatoryUpdate, buttonText1, buttonText2, link);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void onFailure(Call<Update> call, Throwable t) {
            }
        });

    }


    public void updateApp(String Title, String Message, boolean mandatoryUpdate, String buttonText1, String buttonText2, final String link) {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.message);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        TextView message = (TextView) dialog.findViewById(R.id.message);
        TextView yes = (TextView) dialog.findViewById(R.id.yes);
        TextView no = (TextView) dialog.findViewById(R.id.no);
        Typeface typeFace = Typeface.createFromAsset(getApplicationContext()
                .getAssets(), "fonts/farsi.ttf");
        title.setTypeface(typeFace);
        message.setTypeface(typeFace);
        yes.setTypeface(typeFace);
        no.setTypeface(typeFace);

        title.setText(Title);
        message.setText(Message);
        yes.setText(buttonText1);
        no.setText(buttonText2);

        if (buttonText2.toString().length() < 2) {
            no.setVisibility(View.GONE);
        }

        if (mandatoryUpdate) {
            dialog.setCancelable(false);
        } else {
            dialog.setCancelable(true);
        }

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (link.toString().length() > 5) {
                    Intent browserIntent = new Intent("android.intent.action.VIEW",
                            Uri.parse(link));
                    startActivity(browserIntent);
                } else {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (swipeTimer != null) {
            swipeTimer.cancel();
            swipeTimer = null;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        G.cl = this.getClass();
        G.currentActivity = this;
        if (G.isInternetAvailable() == false) {
            showError();
        }

    }


    private long backPressedTime = 0;

    @Override
    public void onBackPressed() {
        try {
            long t = System.currentTimeMillis();
            if (t - backPressedTime > 2000) {    // 2 secs
                backPressedTime = t;
                Toast("لطفاً برای خارج شدن از برنامه مجدداً دکمه خروج را بزنید.");
            } else {

                super.onBackPressed();
            }
        } catch (Exception e) {

        }


    }

}
