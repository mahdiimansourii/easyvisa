package com.supectco.easyvisa.activities;

/**
 * Created by Administrator on 10/15/2015.
 */
public class Item_cat {
    int ID;
    String category;
    String color;
    String image;
    int position;


    Item_cat(int ID, String category, String color , String image , int position) {
        this.ID = ID;
        this.category = category;
        this.color = color;
        this.image = image;
        this.position = position;
    }
}
