package com.supectco.easyvisa.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.supectco.easyvisa.models.BuyRequest;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;

public class Cong extends Master {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.congx);

        Bundle extrass = getIntent().getExtras();
        if(extrass!=null){
            BasketNum = extrass.getString("basketNum");
        }


        initializeToolbarx();
        Initializelayout();
        setStatusBarColor();


    }

    CardView btnHome,btnCallUS;
    RelativeLayout loading;
    TextView state, city, postalCode, phone, phone2, txtChangeData, sendPrice, Price;
    RetrofitProvider retrofitProvider;
    APIService apiService;
    String BasketNum="";
    String IDS = "";
    BuyRequest buyRequest;
    int PRICE;
    MyTextView basketNum, price , buyStatus , orderStatus ,  deliverName , address;

    private void Initializelayout() {


        basketNum = (MyTextView) findViewById(R.id.basketNum);
        price = (MyTextView) findViewById(R.id.price);
        buyStatus = (MyTextView) findViewById(R.id.buyStatus);
        orderStatus = (MyTextView) findViewById(R.id.orderStatus);
        deliverName = (MyTextView) findViewById(R.id.deliverName);
        address = (MyTextView) findViewById(R.id.address);
        btnHome = (CardView) findViewById(R.id.btnHome);
        btnCallUS = (CardView) findViewById(R.id.btnCallUS);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext() , MainActivity.class);
                startActivity(i);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();

            }
        });


        btnCallUS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext() , ContactUs.class);
                startActivity(i);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();

            }
        });


        if(G.prefs.getFloat("finalPricex" , 0)>0){
            String str = G.decimalNumericx(G.prefs.getFloat("finalPricex" , 0));
            price.setText(G.farsiNumeric(str + " تومان"));
        }


        basketNum.setText(BasketNum);

        buyStatus.setText("پرداخت کامل");
        orderStatus.setText("در صف بررسی");

        deliverName.setText(G.prefs.getString("fullname", " "));
        address.setText(G.prefs.getString("P_address", " "));



    }
}
