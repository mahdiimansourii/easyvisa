package com.supectco.easyvisa.activities;

import com.supectco.easyvisa.R;
import com.stephentuso.welcome.BasicPage;
import com.stephentuso.welcome.WelcomeActivity;
import com.stephentuso.welcome.WelcomeConfiguration;

/**
 * Created by PartoDesign04 on 10/10/2017.
 */

public class Intro extends WelcomeActivity {

    @Override
    protected WelcomeConfiguration configuration() {
        return new WelcomeConfiguration.Builder(this)
                .defaultBackgroundColor(R.color.ColorPrimary)
                .page(new BasicPage(R.drawable.icon_setdate , "با ایزی ویزا خیلی راحت می تونی برای هر سفارتی که میخوای وقت بگیری." , "").background(R.color.color_intro_1))
                .page(new BasicPage(R.drawable.icon_translate , "با ایزی ویزا خیلی راحت می تونی همه مدارکتو ترجمه کنی." , "").background(R.color.color_intro_2))
                .page(new BasicPage(R.drawable.icon_counseling , "با ایزی ویزا می تونی همه سوالاتو از مشاورای متخصص بپرسی." , "").background(R.color.color_intro_3))
                .page(new BasicPage(R.drawable.icon_migration , "با ایزی ویزا می تونی از شرایط مهاجرت و تحصیل به کشورای دیگه مطلع بشی" , "").background(R.color.color_intro_4))
                .page(new BasicPage(R.drawable.icon_booking , "با ایزی ویزا می تونی تو هر کشوری که بخوای هتل و خدمات رزرو کنی." , "").background(R.color.color_intro_5))
                .page(new BasicPage(R.drawable.logo , "بزن بریم" , "").background(R.color.color_intro_6))
                .defaultHeaderTypefacePath("fonts/farsi.ttf")
                .defaultDescriptionTypefacePath("fonts/farsi.ttf")
                .swipeToDismiss(true)
                .build();


    }



}
