package com.supectco.easyvisa.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.supectco.easyvisa.models.SignUpModel;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends Master {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signupx);

        initializeToolbarx();
        Initializelayout();
        setStatusBarColor();

    }


    RelativeLayout loading;
    TextView btnSignIn;
    CardView btnSubmit;
    EditText password, repassword, phone;
    SignUpModel signUpModel;

    private void Initializelayout() {

        btnSignIn = (TextView) findViewById(R.id.btnSignIn);
        btnSubmit = (CardView) findViewById(R.id.btnsubmit);
        loading = (RelativeLayout) findViewById(R.id.loading);
        password = (EditText) findViewById(R.id.password);
        repassword = (EditText) findViewById(R.id.repassword);
        phone = (EditText) findViewById(R.id.phone);


        Typeface typeFace = Typeface.createFromAsset(G.context.getAssets(),"fonts/farsi2.ttf");
        phone.setTypeface(typeFace);
        password.setTypeface(typeFace);




        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), SignIn.class);
                startActivity(intent);
                finish();


            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkError();
            }
        });




    }


    public void checkError() {

        if (phone.getText().toString().trim().length() == 0) {

            Toast("لطفاً شماره موبایل خود را وارد کنید.");

        } else if (password.getText().toString().trim().length() == 0) {

            Toast("لطفاً کلمه عبور خود را وارد کنید.");

        } else if (repassword.getText().toString().trim().length() == 0) {

            Toast("لطفاً تکرار کلمه عبور خود را وارد کنید.");

        } else if ((phone.getText().toString().trim().length() >= 1) && (phone.getText().toString().trim().length() < 11)) {

            Toast("لطفاً شماره خود را کامل وارد کنید.");
        } else if (((password.getText().toString().trim().length() >= 1) && (password.getText().toString().trim().length() < 4)) || ((repassword.getText().toString().trim().length() >= 1) && (repassword.getText().toString().trim().length() < 4))) {

            Toast("کلمه عبور نمی تواند کمتر از 4 کاراکتر باشد.");

        } else if (!(password.getText().toString().equals(repassword.getText().toString()))) {

            Toast("کلمه عبور و تکرار کلمه عبور یکسان نمی باشند.");
        } else {
            postData();
        }


    }



    RetrofitProvider retrofitProvider;
    APIService apiService;
    private void postData() {

        loading.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.GONE);
        loading.setOnClickListener(null);

        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();


        Call<SignUpModel> call = apiService.doSignUp(G.deviceID
                , G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd")
                , getTxt(phone), G.md5(getTxt(password)));

        call.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {

               signUpModel = response.body();
                int status = signUpModel.getStatus();
                if (status == 200) {
                    loading.setVisibility(View.GONE);
                    btnSubmit.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(getApplicationContext(), GetCode.class);
                    intent.putExtra("mobile" , phone.getText().toString());
                    intent.putExtra("type" , "2");
                    startActivity(intent);
                    finish();

                } else  if (status==300){
                    Toast("شماره وارد شده قبلا در سامانه ثبت شده است.");
                    loading.setVisibility(View.GONE);
                    btnSubmit.setVisibility(View.VISIBLE);
                }else if(status==400){
                    Toast("شماره وارد شده و یا رمز عبور معتبر نمی باشد.");
                    loading.setVisibility(View.GONE);
                    btnSubmit.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                loading.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.VISIBLE);
                Toast("خطا در برقرای ارتباط با سرور");
            }
        });
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
