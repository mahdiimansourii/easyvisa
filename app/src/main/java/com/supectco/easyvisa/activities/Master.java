package com.supectco.easyvisa.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.supectco.easyvisa.utils.FontsOverride;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.R;

import java.io.File;

/**
 * Created by Administrator on 10/18/2015.
 */
public class Master extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ClassName = getClass().getSimpleName();
        G.creatFolder();
        G.myDB = SQLiteDatabase.openOrCreateDatabase(G.FOLDER + File.separator + "shopbg.sqlite", null);
        G.myDB.execSQL("CREATE TABLE IF NOT EXISTS `ShoppingBag` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `productID` INTEGER, `title` LONGTEXT, `desc` LONGTEXT, `image` LONGTEXT, `nums` LONGTEXT, `price` LONGTEXT, `color` LONGTEXT);");


    }

    Toolbar toolbar, toolbarx , toolbarxx;
    DrawerLayout Drawer;
    public ImageView imgBack, imgSearch, imgShoppingBag, imgMenu ,imgFlag;
    String ClassName = "";
    public MyTextView fullname,txtCount,txtAlarmCat, txtTitle, txtTitleTrans  , cellNumber , txtSignIn   ,aboutUs, callUs;
    CardView crdSignIn;
    LinearLayout ShoppingBag,txtSearch ,myOrder , Home;



    public void initializeToolbar() {

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.getMenu().clear();
        toolbar.setContentInsetsAbsolute(0, 0);
        imgSearch = (ImageView) findViewById(R.id.imgSearch);
        imgShoppingBag = (ImageView) findViewById(R.id.imgShoppingBag);
        imgMenu = (ImageView) findViewById(R.id.imgMenu);
        txtAlarmCat = (MyTextView) findViewById(R.id.txtAlarmCat);
        Home = (LinearLayout) findViewById(R.id.Home);
        ShoppingBag = (LinearLayout) findViewById(R.id.ShoppingBag);
        txtCount = (MyTextView) findViewById(R.id.txtCount);
        cellNumber = (MyTextView) findViewById(R.id.cellNumber);
        txtSignIn = (MyTextView) findViewById(R.id.txtSignIn);
        txtSearch = (LinearLayout) findViewById(R.id.txtSearch);
        myOrder = (LinearLayout) findViewById(R.id.myOrder);
        aboutUs = (MyTextView) findViewById(R.id.aboutUs);
        callUs = (MyTextView) findViewById(R.id.callUs);
        fullname = (MyTextView) findViewById(R.id.fullname);
        crdSignIn = (CardView) findViewById(R.id.crdSignIn);
        txtTitle = (MyTextView) findViewById(R.id.txtTitle);
        LinearLayout menui=(LinearLayout) findViewById(R.id.menui);
        LinearLayout menuii=(LinearLayout) findViewById(R.id.menuii);
        LinearLayout Exit=(LinearLayout) findViewById(R.id.Exit);
        LinearLayout Profile=(LinearLayout) findViewById(R.id.Profile);





//
//        if (ClassName.equals("ProductList")) {
//            imgMenu.setImageResource(R.drawable.ic_arrow_forward_black_24dp);
//            imgMenu.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    finish();
//                }
//            });
//
//
//        }else {
//            imgMenu.setImageResource(R.drawable.ic_menu_white_24dp);
//            imgMenu.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Drawer.openDrawer(Gravity.RIGHT);
//                }
//            });
//
//
//        }


        if (G.prefs.getBoolean("isLogin", false) == true) {
//            cellNumber.setText(G.farsiNumeric(G.prefs.getString("mobile", "")));
//            txtSignIn.setText("خروج");
//            myOrder.setVisibility(View.VISIBLE);
//            crdSignIn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    logoutDialog();
//                }
//            });
            menui.setVisibility(View.GONE);
            menuii.setVisibility(View.VISIBLE);
            fullname.setText(G.farsiNumeric(G.prefs.getString("mobile", "")));


        }else {
//            cellNumber.setText("کاربر مهمان");
//            txtSignIn.setText("ورود به برنامه");
//            myOrder.setVisibility(View.GONE);
//            crdSignIn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    open(SignIn.class);
//                }
//            });
            menui.setVisibility(View.VISIBLE);
            menuii.setVisibility(View.GONE);

        }

//
//        Exit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                logoutDialog();
//            }
//        });
//
//
//        Profile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent=new Intent(getApplicationContext(), SignIn.class);
//                Drawer.closeDrawer(Gravity.RIGHT);
//                startActivity(intent);
//            }
//        });
//
//
//
//        imgShoppingBag.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!ClassName.equals("ShoppingBag")) {
//                    open(ShoppingBag.class);
//                }
//            }
//        });
//
//
//
//
//        imgSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
//                Drawer.closeDrawer(Gravity.RIGHT);
//                startActivity(intent);
//            }
//        });
//
//
//        aboutUs.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), AboutUs.class);
//                Drawer.closeDrawer(Gravity.RIGHT);
//                startActivity(intent);
//            }
//        });
//
//
//
//        callUs.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), ContactUs.class);
//                Drawer.closeDrawer(Gravity.RIGHT);
//                startActivity(intent);
//            }
//        });
//
//
//
//
//
//
//        txtSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
//                Drawer.closeDrawer(Gravity.RIGHT);
//                startActivity(intent);
//            }
//        });
//
//
//        myOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (ClassName.equals("MyOrders")) {
//                    Drawer.closeDrawer(Gravity.RIGHT);
//                } else {
//                    Intent intent = new Intent(getApplicationContext(), MyOrders.class);
//                    Drawer.closeDrawer(Gravity.RIGHT);
//                    startActivity(intent);
//                }
//
//            }
//        });
//
//
//        Home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (ClassName.equals("MainActivity")) {
//                    Drawer.closeDrawer(Gravity.RIGHT);
//                } else {
//                    Intent intent = new Intent(getApplicationContext(), Main.class);
//                    Drawer.closeDrawer(Gravity.RIGHT);
//                    startActivity(intent);
//                }
//
//
//            }
//        });
//
//        ShoppingBag.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (ClassName.equals("ShoppingBag")) {
//                    Drawer.closeDrawer(Gravity.RIGHT);
//                } else {
//                    Intent intent = new Intent(getApplicationContext(), ShoppingBag.class);
//                    Drawer.closeDrawer(Gravity.RIGHT);
//                    startActivity(intent);
//                }
//
//
//            }
//        });



    }


    public void initializeToolbarx() {

        toolbarx = (Toolbar) findViewById(R.id.tool_barx);
        txtTitle = (MyTextView) findViewById(R.id.txtTitle);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        toolbarx.getMenu().clear();
        toolbarx.setContentInsetsAbsolute(0, 0);


        if (ClassName.equals("ProductList")) {
            txtTitle.setText("لیست محصولات");
        } else if (ClassName.equals("ShoppingBag")) {
            txtTitle.setText("سبد خرید");
        } else if (ClassName.equals("CompleteSignUp")) {
            txtTitle.setText("تکمیل ثبت نام");
        }else if (ClassName.equals("ForgetPass")) {
            txtTitle.setText("دریافت رمز عبور");
        }else if (ClassName.equals("ChangePass")) {
            txtTitle.setText("دریافت رمز عبور");
        }else if (ClassName.equals("GetCode")) {
            txtTitle.setText("دریافت کد");
        }else if (ClassName.equals("SearchActivity")) {
            txtTitle.setText("جستجو");
        }else if (ClassName.equals("SearchResult")) {
            txtTitle.setText("جستجو");
        }else if (ClassName.equals("ChooseFinalImage")) {
            txtTitle.setText("انتخاب رنگ پرده");
        }else if (ClassName.equals("AboutUs")) {
            txtTitle.setText("درباره ما");
        }else if (ClassName.equals("ContactUs")) {
            txtTitle.setText("تماس با ما");
        } else if (ClassName.equals("ChooseCat")) {
            txtTitle.setText("دسته بندی");
        }else if (ClassName.equals("SignIn")) {
            txtTitle.setText("ورود");
        }else if (ClassName.equals("SignUp")) {
            txtTitle.setText("ثبت نام");
        }else if (ClassName.equals("ShoppingBag")) {
            txtTitle.setText("سبد خرید");
        }else if (ClassName.equals("Cong")) {
            txtTitle.setText("پایان خرید");
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }



    public void initializeToolbarTrans() {

        toolbarxx = (Toolbar) findViewById(R.id.tool_barxx);
        txtTitleTrans = (MyTextView) findViewById(R.id.txtTitle);
        imgFlag = (ImageView) findViewById(R.id.imgFlag);
        toolbarxx.getMenu().clear();
        toolbarxx.setContentInsetsAbsolute(0, 0);





    }





    public void setStatusBarColor() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.ColorPrimaryDark));
        }

    }

    public void initializeDrawer() {
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view
        Drawer.setStatusBarBackgroundColor(ContextCompat.getColor(this, R.color.ColorPrimaryDark));
    }

    public void Toast(final String txt) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast, (ViewGroup) findViewById(R.id.toast_layout_root));

                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText(txt);
                text.setTypeface(G.typeFace);

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.BOTTOM, 0, 0);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
        });
    }



    public void logoutDialog(){

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logoutx);
        TextView yes = (TextView)dialog.findViewById(R.id.yes);
        TextView no = (TextView)dialog.findViewById(R.id.no);
        TextView txtNote = (TextView)dialog.findViewById(R.id.txtNote);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = G.prefs.edit();
                editor.putString("address", "");
                editor.putString("mobile", "");
                editor.putBoolean("isLogin", false);
                editor.commit();
                Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                Toast("شما با موفقیت خارج شدید.");
                finish();


            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();


    }



    public void InitializeFont() {
        if (G.typeFace == null) {
            G.typeFace = Typeface.createFromAsset(getAssets(), "fonts/farsi.ttf");
        }
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/farsi.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/farsi.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/farsi.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/farsi.ttf");

        try {
            LinearLayout lin = (LinearLayout) findViewById(R.id.MainBox);
            FontsOverride.overrideFonts(G.context, lin);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void open(Class cls) {
        Intent intent = new Intent(getApplicationContext(), cls);
        startActivity(intent);
    }

    public void open(Class cls, boolean isfinish) {
        Intent intent = new Intent(getApplicationContext(), cls);
        startActivity(intent);
        finish();
    }

    public String getTxt(EditText field) {
        return field.getText().toString().trim();
    }



    @Override
    protected void onResume() {
        super.onResume();
        try{
            showStar();
        }catch (Exception e){
            G.Log("CRASH!!!!ON");
            e.printStackTrace();
        }

    }


    public void showStar(){

        Cursor c =  G.myDB.rawQuery("SELECT `id` FROM `ShoppingBag`" , null);
        int Count = c.getCount();
        G.Log("Count1 : " + Count);
        if (Count > 0){
            G.Log("Count2 : " + Count);
            imgShoppingBag.setImageResource(R.drawable.icon_baskets);
            txtCount.setVisibility(View.VISIBLE);
            txtCount.setText(G.farsiNumeric(Count+""));
        }else {
            G.Log("Count3 : " + Count);
            imgShoppingBag.setImageResource(R.drawable.icon_basket);
            txtCount.setVisibility(View.GONE);
        }

    }



}
