package com.supectco.easyvisa.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.adapters.ImageSlider_Adapter;
import com.supectco.easyvisa.adapters.SimilarAdapter;
import com.supectco.easyvisa.models.ViewProductModel;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyEditText;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.adapters.ImageSlider_Adapterx;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewProduct extends Master {


    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    // String[] slides = new String[5];
    ArrayList<String> slidesArray = new ArrayList<>();
    ArrayList<String> slidesArrayx = new ArrayList<>();
    ImageSlider_Adapter imageSlider_adapter;
    ImageSlider_Adapterx imageSlider_adapterx;
    private ViewPager viewPager,vpager;
    private CircleIndicator indicator, indicatorx;
    private int currentPage = 1,currentPagex = 1, NUM_PAGES, NUM_PAGESx;
    private Timer swipeTimer = null;
    RelativeLayout loading, error;
    private SimilarAdapter similarAdapter;
    private LinearLayoutManager llm;
    ViewProductModel viewProductModel;
    RecyclerView rv;
    MyTextView title , desc , txtValues , Type , Brand , txtChooseColor;
    CardView shoppingBag;
    int ID=0;
    String IMAGE ,price,oldPrice , TITLE , DESC ;
    LinearLayout morePro;
    LinearLayout lylColor;
    BottomSheetDialogFragment bottomSheetDialogFragment;
    RelativeLayout slidingImage;
    Handler handler=new Handler();
    MyTextView retry;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_product);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ID = extras.getInt("id");
            IMAGE = extras.getString("image");
            G.context = getApplicationContext();

        }

        initializeToolbar();
        initializeDrawer();
        initializeLayout();
        initializeData();


    }


    public void initializeLayout() {


        loading = (RelativeLayout) findViewById(R.id.loading);
        error = (RelativeLayout) findViewById(R.id.error);
        slidingImage = (RelativeLayout) findViewById(R.id.slidingImage);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        vpager = (ViewPager) findViewById(R.id.vpager);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicatorx = (CircleIndicator) findViewById(R.id.indicatorx);
        title = (MyTextView) findViewById(R.id.title);
        desc = (MyTextView) findViewById(R.id.desc);
        txtValues = (MyTextView) findViewById(R.id.txtValues);
        Type = (MyTextView) findViewById(R.id.Type);
        Brand = (MyTextView) findViewById(R.id.Brand);
        txtChooseColor = (MyTextView) findViewById(R.id.txtChooseColor);
        lylColor = (LinearLayout) findViewById(R.id.lylColor);
        rv = (RecyclerView) findViewById(R.id.rv);
        shoppingBag = (CardView) findViewById(R.id.shoppingBag);
        morePro = (LinearLayout) findViewById(R.id.morePro);
        rv.setHasFixedSize(true);
        llm = new LinearLayoutManager(G.context.getApplicationContext(),LinearLayoutManager.HORIZONTAL, false);
        rv.setLayoutManager(llm);

        bottomSheetDialogFragment = new ChooseColorBottomSheetDialogFragment();

        slidingImage.setOnClickListener(null);

        txtChooseColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                new SweetAlertDialog(G.currentActivity, SweetAlertDialog.WARNING_TYPE)
//                        .setTitleText("آیا از انتخاب این رنگ مطمئنید؟")
//                        .setCancelText("خیر")
//                        .setConfirmText("بله")
//                        .showCancelButton(true)
//                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                            @Override
//                            public void onClick(final SweetAlertDialog sDialog) {
//                                sDialog
//                                        .setTitleText("انتخاب شد. ")
//                                        .setConfirmText("مرحله بعدی")
//                                        .setConfirmClickListener(null)
//                                        .showCancelButton(false).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                    @Override
//                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
//
//                                        sDialog.cancel();
//                                        slidingImage.setVisibility(View.GONE);
//
//                                        G.IMAGE = slidesArrayx.get(vpager.getCurrentItem());
//
//                                        insertdbDialog();
//
//                                    }
//                                })
//                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
//                            }
//                        })
//                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                            @Override
//                            public void onClick(SweetAlertDialog sDialog) {
//                                sDialog.cancel();
//                            }
//                        })
//                        .show();

            }
        });




        morePro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext() , ProductList.class);
                startActivity(intent);

            }
        });


        shoppingBag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                G.Log("DESC: " + DESC ) ;


//                G.myDB.execSQL("INSERT INTO `ShoppingBag` (`productID`,`title`,`desc`,`image`, `nums` ,`price`,`color`) VALUES ('" + ID + "','" + TITLE + "','" + DESC+ "','" + IMAGE + "','" + Nums + "','" + price + "','" + G.COLOR + "')");
//                open(ShoppingBag.class , true);

                G.runnablex = new Runnable() {
                    @Override
                    public void run() {
//                        insertdbDialog();
                        G.myDB.execSQL("INSERT INTO `ShoppingBag` (`productID`,`title`,`desc`,`image`, `nums` ,`price`,`color`) VALUES ('" + ID + "','" + TITLE + "','" + DESC+ "','" + IMAGE + "','" + Nums + "','" + price + "','" + G.COLOR + "')");
                        open(ShoppingBag.class , true);
                    }
                };
                for (int i =0 ;i<G.chckColorFilter.size();i++){
                    G.chckColorFilter.set(i,0);
//                    G.colorFilterImageView.set(i,null);
                }

//                slidingImage.setVisibility(View.VISIBLE);
//                shoppingBag.setVisibility(View.GONE);
//                viewPagerx();

                Intent intent = new Intent(ViewProduct.this ,ChooseFinalImage.class);
                intent.putExtra("id" , ID);
                startActivity(intent);

            }
        });





    }


    private void initializeData() {
        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        loading.setVisibility(View.VISIBLE);
        shoppingBag.setVisibility(View.GONE);
        G.colorFilter.clear();
        G.chckColorFilter.clear();
        G.colorFilterImageView.clear();
        slidesArray.clear();
        slidesArrayx.clear();
        G.slidesArrayImage.clear();

        Call<ViewProductModel> call = apiService.getDataViewProduct(ID+"", G.md5(G.deviceID + "ncase8934f49909"), G.deviceID);

        call.enqueue(new Callback<ViewProductModel>() {
            @Override
            public void onResponse(Call<ViewProductModel> call, Response<ViewProductModel> response) {

                if (response.isSuccessful()) {

                    viewProductModel = response.body();
                    similarAdapter = new SimilarAdapter();
                    rv.setAdapter(similarAdapter);

                    if (response.body().similarProduct != null) {

                        for (ViewProductModel.SimilarProduct item : response.body().similarProduct) {
                            similarAdapter.add(item);
                        }

                    }

                    loading.setVisibility(View.GONE);
                    shoppingBag.setVisibility(View.VISIBLE);
                    try {
                        for (int i = 0; i < viewProductModel.getSlides().size(); i++) {
                            slidesArray.add(viewProductModel.getSlides().get(i).getImage());
                            slidesArrayx.add(viewProductModel.getSlides().get(i).getImage());
                            G.slidesArrayImage.add(viewProductModel.getSlides().get(i).getImage());
                            G.slidesArrayTitle.add(viewProductModel.getSlides().get(i).getTitle());
//                            slides[i] = viewProductModel.getSlides().get(i).getImage();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    title.setText(viewProductModel.getTitle());
                    desc.setText(viewProductModel.getDesc());
                    price = viewProductModel.getPrice();
                    oldPrice = viewProductModel.getOldPrice();
                    TITLE = viewProductModel.getTitle();
                    DESC = viewProductModel.getDesc();
                    Type.setText(viewProductModel.getType());
                    Brand.setText(viewProductModel.getBrand());


                    lylColor.removeAllViews();
                    String[] separated = viewProductModel.getColor().split("\\$");
                    for(int j= 0 ; j<separated.length ; j++){
                        View colorView = G.inflater.inflate(R.layout.color_layout_adapter, null, false);
                        final DottedImageView imgColor = (DottedImageView) colorView.findViewById(R.id.imgColor);
                        imgColor.setImageType(DottedImageView.imgTypeOnlySolid);
                        //   imgColor.setSolidColor(Color.parseColor(separated[j]));
                        G.colorFilter.add(separated[j]);
                        G.chckColorFilter.add(0);
                        lylColor.addView(colorView);
                    }


                    if(G.isInteger(price)==true){
                        String str =  G.decimalNumeric(Long.parseLong(price));
                        txtValues.setText(G.farsiNumeric(str + " تومان "));
                    }else{
                        txtValues.setText(G.farsiNumeric(price));
                    }


                    viewPager();
                }


            }

            @Override
            public void onFailure(Call<ViewProductModel> call, Throwable t) {

            }
        });

    }


    public void viewPager() {

        ArrayList<String> ImagesArraySlide = new ArrayList<>();

        for (int i = 0; i < slidesArray.size(); i++) {
            if (slidesArray.get(i).length() > 4) {
                ImagesArraySlide.add(slidesArray.get(i));
            }
        }


        imageSlider_adapter = new ImageSlider_Adapter(ViewProduct.this, ImagesArraySlide);
        viewPager.setAdapter(imageSlider_adapter);
        indicator.setViewPager(viewPager);
        try {
            NUM_PAGES = ImagesArraySlide.size();
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    viewPager.setCurrentItem(currentPage++, true);
                }
            };
            if (swipeTimer == null) {
                swipeTimer = new Timer();
                swipeTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(Update);
                    }
                }, 3000, 3000);
            }
            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;
                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int pos) {
                }
            });
        } catch (Exception e) {
        }

    }


    public void viewPagerx() {

        ArrayList<String> ImagesArraySlide = new ArrayList<>();

        for (int i = 0; i < slidesArrayx.size(); i++) {
            if (slidesArrayx.get(i).length() > 10) {
                ImagesArraySlide.add(slidesArrayx.get(i));
            }
        }


        imageSlider_adapterx = new ImageSlider_Adapterx(ViewProduct.this, ImagesArraySlide);
        vpager.setAdapter(imageSlider_adapterx);

        indicatorx.setViewPager(viewPager);
        try {
            NUM_PAGESx = ImagesArraySlide.size();
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPagex == NUM_PAGESx) {
                        currentPagex = 0;
                    }
                    vpager.setCurrentItem(currentPagex++, true);
                }
            };
//            if (swipeTimer == null) {
//                swipeTimer = new Timer();
//                swipeTimer.schedule(new TimerTask() {
//                    @Override
//                    public void run() {
//                        handler.post(Update);
//                    }
//                }, 3000, 3000);
//            }
            indicatorx.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
//                    currentPagex = position;
//                    G.Log("currentPagex: " + currentPagex);
//                    G.IMAGE = slidesArray.get(currentPagex);
                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {
//                    G.Log("pos:" + pos);
//                    G.IMAGE = slidesArray.get(pos);
                }

                @Override
                public void onPageScrollStateChanged(int pos) {
                }
            });
        } catch (Exception e) {
        }

    }



    public void insertDB(){
        insertdbDialog();
    }

    String METRAJ="";
    String Nums="1";
    public void insertdbDialog(){

        final Dialog dialog = new Dialog(G.currentActivity);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.insert_db);
        final MyTextView Price = (MyTextView)dialog.findViewById(R.id.Price);
        final MyTextView TotalPrice = (MyTextView)dialog.findViewById(R.id.TotalPrice);
        final MyEditText metraj = (MyEditText)dialog.findViewById(R.id.metraj);
        MyTextView submit = (MyTextView)dialog.findViewById(R.id.submit);
        final MyTextView cancel = (MyTextView)dialog.findViewById(R.id.cancel);
        Typeface typeFace = Typeface.createFromAsset(G.context.getAssets(),"fonts/farsi2.ttf");
        metraj.setTypeface(typeFace);






        if(G.isInteger(price)==true){
            String str =  G.decimalNumericx(Float.parseFloat(price));
            Price.setText(G.farsiNumeric(str + " تومان"));
            TotalPrice.setText(G.farsiNumeric(str + " تومان"));
        }else{
            Price.setText(G.farsiNumeric(price));
            TotalPrice.setText(G.farsiNumeric(price));
        }

        metraj.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {  }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}


            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try{
                    if(s.toString().length()>0){

                        String str =  G.decimalNumericx(Float.parseFloat(price)*Float.parseFloat(s.toString()));
                        TotalPrice.setText(G.farsiNumeric(str + " تومان"));

                    }else {
                        String str =  G.decimalNumericx(Float.parseFloat(price));
                        TotalPrice.setText(G.farsiNumeric(str + " تومان"));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(metraj.getText().toString().length()>0){

                    METRAJ =   metraj.getText().toString();
                    G.myDB.execSQL("INSERT INTO `ShoppingBag` (`productID`,`title`,`desc`,`image`, `metraj` ,`price`,`color`) VALUES ('" + ID + "','" + TITLE + "','" + DESC+ "','" + G.IMAGE + "','" + METRAJ + "','" + price + "','" + G.COLOR + "')");
                    G.currentActivity.finish();
                    open(ShoppingBag.class , true);

                }else {
                    Toast("لطفا متراژ پرده مورد نظر را مشخص کنید. ");
                }



            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shoppingBag.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
        G.Log("LOG122222222222222222222");

    }


    public void showError(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                try{
                    error.setVisibility(View.VISIBLE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (G.isInternetAvailable() == false) {
                                showError();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), G.cl);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                }catch (Exception e){

                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (swipeTimer != null) {
            swipeTimer.cancel();
            swipeTimer = null;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        G.cl = this.getClass();
        G.currentActivity = this;
        if(G.isInternetAvailable()==false){
            showError();
        }
    }

    @Override
    public void onBackPressed() {
        if(slidingImage.getVisibility()==View.VISIBLE){
            slidingImage.setVisibility(View.GONE);
            shoppingBag.setVisibility(View.VISIBLE);
        }else {
            super.onBackPressed();
        }
    }
}
