package com.supectco.easyvisa.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.R;

public class CompleteSignUp extends Master {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complete_sign_up);

        initializeToolbarx();
        Initializelayout();
        setStatusBarColor();

    }


    RelativeLayout loading;
    CardView btnsubmit ;
    EditText state,city, address, postalCode, phone,phone2;

    private void Initializelayout() {


        state = (EditText)findViewById(R.id.state);
        city = (EditText)findViewById(R.id.city);
        address = (EditText)findViewById(R.id.address);
        postalCode = (EditText)findViewById(R.id.postalCode);
        phone = (EditText)findViewById(R.id.phone);
        phone2 = (EditText)findViewById(R.id.phone2);
        btnsubmit = (CardView)findViewById(R.id.btnsubmit);
        loading = (RelativeLayout)findViewById(R.id.loading);

        Typeface typeFace = Typeface.createFromAsset(G.context.getAssets(),"fonts/farsi2.ttf");
        state.setTypeface(typeFace);
        city.setTypeface(typeFace);
        address.setTypeface(typeFace);
        postalCode.setTypeface(typeFace);
        phone.setTypeface(typeFace);
        phone2.setTypeface(typeFace);

        if(G.prefs.getString("P_state","").length()>0){
            state.setText(G.prefs.getString("P_state"," "));
        }

        if(G.prefs.getString("P_city","").length()>0){
            city.setText(G.prefs.getString("P_city"," "));
        }

        if(G.prefs.getString("P_address","").length()>0){
            address.setText(G.prefs.getString("P_address"," "));
        }

        if(G.prefs.getString("P_postalCode","").length()>0){
            postalCode.setText(G.farsiNumeric(G.prefs.getString("P_postalCode"," ")));
        }

        if(G.prefs.getString("P_phone","").length()>0){
            phone.setText(G.farsiNumeric(G.prefs.getString("P_phone"," ")));
        }

        if(G.prefs.getString("P_phone2","").length()>0){
            phone2.setText(G.farsiNumeric(G.prefs.getString("P_phone2"," ")));
        }


        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkError();


            }
        });



    }

    public void checkError() {




            if (state.getText().toString().trim().length() == 0) {

                Toast("لطفاً نام استان را وارد کنید.");

            }else if (state.getText().toString().trim().length() > 0 && state.getText().toString().trim().length()<2) {

                Toast("لطفا نام استان را کامل وارد کنید.");

            } else if (city.getText().toString().trim().length() == 0) {

                Toast("لطفاً نام شهر را وارد کنید.");

            }else if (city.getText().toString().trim().length() > 0 && city.getText().toString().trim().length()<2) {

                Toast("لطفا نام شهر را کامل وارد کنید.");

            }else if (address.getText().toString().trim().length() == 0) {

                Toast("لطفاً آدرس را وارد کنید.");

            } else if (address.getText().toString().trim().length() > 0 && address.getText().toString().trim().length() < 8) {

                Toast("لطفاً آدرس را کامل وارد کنید.");

            } else if (postalCode.getText().toString().trim().length() < 10 ) {

                Toast("لطفاً کد پستی را به درستی وارد کنید.");

            } else if (phone.getText().toString().trim().length() == 0 ) {

                Toast("لطفا ًشماره همراه وارد کنید.");

            }else if ((phone.getText().toString().trim().length() >= 1) && (phone.getText().toString().trim().length() < 11)) {

                Toast("لطفاً شماره خود را کامل وارد کنید.");
            } else if (phone2.getText().toString().trim().length() == 0 ) {

                Toast("لطفا ًشماره تلفن ثابت خود را وارد کنید.");

            }  else {

                saveData();
            }


    }



    private void saveData(){

        SharedPreferences.Editor editor = G.prefs.edit();
        editor.putString("P_state", state.getText().toString().trim());
        editor.putString("P_city", city.getText().toString().trim());
        editor.putString("P_address", address.getText().toString().trim());
        editor.putString("P_postalCode", postalCode.getText().toString().trim());
        editor.putString("P_phone", phone.getText().toString().trim());
        editor.putString("P_phone2", phone2.getText().toString().trim());
        editor.commit();

        Intent intent=new Intent(getApplicationContext(), FinalConfirm.class);
        startActivity(intent);
        finish();

    }

}
