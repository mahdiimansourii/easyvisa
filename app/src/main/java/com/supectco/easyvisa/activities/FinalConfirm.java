package com.supectco.easyvisa.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.supectco.easyvisa.models.BuyRequest;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.models.BuyReqModel;
import com.supectco.easyvisa.views.MyEditText;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.models.DiscountModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FinalConfirm extends Master {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_confirm);

        Bundle extrass = getIntent().getExtras();
        if (extrass != null) {
            isCompleteSignUp = extrass.getBoolean("isCompleteSignUp");
            FINAL_PRICE = extrass.getString("finalPrice");
            if(extrass.getFloat("finalPricex")>0){
            FINAL_PRICEX = extrass.getFloat("finalPricex");
            SharedPreferences.Editor editor = G.prefs.edit();
            editor.putFloat("finalPricex", FINAL_PRICEX);
            editor.commit();
            }
        }

        initializeToolbar();
        initializeDrawer();
        Initializelayout();

    }

    CardView btnsubmit, crdConfirmInfo, crdEditInfo;
    RelativeLayout loading;
    TextView state, city, address,name,family, postalCode, phone, phone2, txtChangeData, txtConfirmData, sendPrice, Price;
    MyTextView txtDiscount;
    MyEditText edtCity, edtState, edtAddress, edtPostalCode, edtPhone, edtPhone2, edtDiscount, edtName, edtFamily;
    RetrofitProvider retrofitProvider;
    APIService apiService;
    String pr, prx = "";
    String IDS = "", METRAJ = "", IMAGE_NAMES = "", DISCOUNT = "", FINAL_PRICE = "", MESSAGE = "", disPrice = "";
    BuyRequest buyRequest;
    int PRICE;
    float FINAL_PRICEX;
    boolean isCompleteSignUp = false;
    DiscountModel discountModel;
    BuyReqModel buyReqModel;
    int orderID;
    Intent intent;

    private void Initializelayout() {

        state = (TextView) findViewById(R.id.state);
        city = (TextView) findViewById(R.id.city);
        address = (TextView) findViewById(R.id.address);
        name = (TextView) findViewById(R.id.name);
        family = (TextView) findViewById(R.id.family);
        postalCode = (TextView) findViewById(R.id.postalCode);
        phone = (TextView) findViewById(R.id.phone);
        phone2 = (TextView) findViewById(R.id.phone2);
        txtChangeData = (TextView) findViewById(R.id.txtChangeData);
        txtConfirmData = (TextView) findViewById(R.id.txtConfirmData);
        edtCity = (MyEditText) findViewById(R.id.edtCity);
        edtState = (MyEditText) findViewById(R.id.edtState);
        edtAddress = (MyEditText) findViewById(R.id.edtAddress);
        edtPostalCode = (MyEditText) findViewById(R.id.edtPostalCode);
        edtPhone = (MyEditText) findViewById(R.id.edtPhone);
        edtPhone2 = (MyEditText) findViewById(R.id.edtPhone2);
        edtDiscount = (MyEditText) findViewById(R.id.edtDiscount);
        edtName = (MyEditText) findViewById(R.id.edtName);
        edtFamily = (MyEditText) findViewById(R.id.edtFamily);
        sendPrice = (TextView) findViewById(R.id.sendPrice);
        Price = (TextView) findViewById(R.id.Price);
        txtDiscount = (MyTextView) findViewById(R.id.txtDiscount);
        loading = (RelativeLayout) findViewById(R.id.loading);
        btnsubmit = (CardView) findViewById(R.id.btnsubmit);
        crdConfirmInfo = (CardView) findViewById(R.id.crdConfirmInfo);
        crdEditInfo = (CardView) findViewById(R.id.crdEditInfo);

        Typeface typeFace = Typeface.createFromAsset(G.context.getAssets(), "fonts/farsi2.ttf");
        edtState.setTypeface(typeFace);
        edtCity.setTypeface(typeFace);
        edtAddress.setTypeface(typeFace);
        edtPostalCode.setTypeface(typeFace);
        edtPhone.setTypeface(typeFace);
        edtPhone2.setTypeface(typeFace);


        if (G.prefs.getString("P_state", "").length() > 0) {
            edtState.setText(G.prefs.getString("P_state", " "));
        }

        if (G.prefs.getString("P_city", "").length() > 0) {
            edtCity.setText(G.prefs.getString("P_city", " "));
        }

        if (G.prefs.getString("P_address", "").length() > 0) {
            edtAddress.setText(G.prefs.getString("P_address", " "));
        }

        if (G.prefs.getString("fullname", "").length() > 0) {
            edtName.setText(G.prefs.getString("fullname", " "));
        }

        if (G.prefs.getString("P_family", "").length() > 0) {
            edtFamily.setText(G.prefs.getString("P_family", " "));
        }

        if (G.prefs.getString("P_postalCode", "").length() > 0) {
            edtPostalCode.setText(G.farsiNumeric(G.prefs.getString("P_postalCode", " ")));
        }

        if (G.prefs.getString("mobile", "").length() > 0) {
            edtPhone.setText(G.farsiNumeric(G.prefs.getString("mobile", " ")));
        }

        if (G.prefs.getString("P_phone2", "").length() > 0) {
            edtPhone2.setText(G.farsiNumeric(G.prefs.getString("P_phone2", " ")));
        }


        if (isCompleteSignUp) {
            crdConfirmInfo.setVisibility(View.VISIBLE);
            crdEditInfo.setVisibility(View.GONE);
        } else {
            crdConfirmInfo.setVisibility(View.GONE);
            crdEditInfo.setVisibility(View.VISIBLE);
        }


        int totalValue = 0;
        String totalIds = "";
        String totalMetraj = "";
        String totalImageNames = "";
        Cursor c = G.myDB.rawQuery("SELECT * FROM `ShoppingBag`", null);
        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                String price = c.getString(c.getColumnIndex("price"));
                String metraj = c.getString(c.getColumnIndex("nums"));
                String imageNames = c.getString(c.getColumnIndex("image"));
                int id = c.getInt(c.getColumnIndex("productID"));

                try {
                    String ids = String.valueOf(id);
                    totalIds = totalIds + "," + ids;
                    totalMetraj = totalMetraj + "," + metraj;
                    totalImageNames = totalImageNames + "," + imageNames;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    int p = Integer.parseInt(price);
                    totalValue = totalValue + p;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } while (c.moveToNext());

            c.close();
        }

        PRICE = totalValue;
        pr = G.decimalNumeric(totalValue);
        IDS = totalIds;
        METRAJ = totalMetraj;
        IMAGE_NAMES = totalImageNames;
//        Price.setText(G.farsiNumeric(G.prefs.getInt("finalPricex" , 0) + " تومان"));

//        if(FINAL_PRICEX>0){
//            String str = G.decimalNumeric(FINAL_PRICEX);
//            Price.setText(G.farsiNumeric(str + " تومان"));
//        }



//        FINAL_PRICEX = Integer.parseInt(FINAL_PRICE);

        if(totalValue<=100000){

            sendPrice.setText(G.farsiNumeric("7,000 تومان"));
            PRICE = PRICE+7000;
        }else{
            sendPrice.setText(G.farsiNumeric("0 تومان"));
        }


        if(G.prefs.getFloat("finalPricex" , 0)>0){
            String str = G.decimalNumericx(G.prefs.getFloat("finalPricex" , 0));
            Price.setText(G.farsiNumeric(str + " تومان"));
        }



        if (G.prefs.getString("P_state", " ").length() > 0) {
            state.setText(G.prefs.getString("P_state", " "));
        }

        if (G.prefs.getString("P_city", " ").length() > 0) {
            city.setText(G.prefs.getString("P_city", " "));
        }

        if (G.prefs.getString("P_address", " ").length() > 0) {
            address.setText(G.prefs.getString("P_address", " "));
        }


        if (G.prefs.getString("fullname", " ").length() > 0) {
            name.setText(G.prefs.getString("fullname", " "));
        }


        if (G.prefs.getString("P_family", " ").length() > 0) {
            family.setText(G.prefs.getString("P_family", " "));
        }

        if (G.prefs.getString("P_postalCode", " ").length() > 0) {
            postalCode.setText(G.farsiNumeric(G.prefs.getString("P_postalCode", " ")));
        }

        if (G.prefs.getString("mobile", " ").length() > 0) {
            phone.setText(G.farsiNumeric(G.prefs.getString("mobile", " ")));
        }

        if (G.prefs.getString("P_phone2", " ").length() > 0) {
            phone2.setText(G.farsiNumeric(G.prefs.getString("P_phone2", " ")));
        }


        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isCompleteSignUp) {
                    if (getTxt(edtDiscount).length() > 0) {
                        DISCOUNT = getTxt(edtDiscount);
                    }
                    postData();
                } else {
                    Toast("لطفا اطلاعات تکمیلی را جهت ارسال سفارش کامل بفرمایید.");
                }
            }
        });

        txtConfirmData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkError();

            }
        });


        txtChangeData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                crdEditInfo.setVisibility(View.VISIBLE);
                crdConfirmInfo.setVisibility(View.GONE);


            }
        });


        txtDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edtDiscount.getText().toString().length() < 8) {
                    edtDiscount.setError("کد تخفیف نامعتبر می باشد.");
                } else {
                    getDiscount();
                }

            }
        });


        try {

            intent = getIntent();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            if (intent != null) {
//                if (intent.getAction().equals("com.partodesign.onlineparde.launchfrombrowser")) {
//                    Bundle bundle = intent.getExtras();
//                    if (bundle != null) {
//                        String msgFromBrowserUrl = bundle.getString("msg_from_browser");
//                        if (msgFromBrowserUrl.equals("1")) {
//                            loading.setVisibility(View.GONE);
//                            G.myDB.execSQL("DELETE  FROM `ShoppingBag`");
//                            try {
//                                open(Cong.class, true);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }else if (msgFromBrowserUrl.equals("5")) {
//                            isCompleteSignUp=true;
//                            saveData();
//                            btnsubmit.setVisibility(View.VISIBLE);
//                            G.Toast("پرداخت لغو گردید.");
//                        } else {
//                            isCompleteSignUp=true;
//                            saveData();
//                            btnsubmit.setVisibility(View.VISIBLE);
//                            G.Toast("خرید با مشکل مواجه شد");
//                        }
//                    }
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getDiscount() {
        loading.setVisibility(View.VISIBLE);
        loading.setOnClickListener(null);

        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();

        Call<DiscountModel> call = apiService.getDiscount(G.deviceID
                , G.md5(G.deviceID + "fjh374jf934j8894")
                , getTxt(edtDiscount), G.prefs.getString("mobile", ""));

        call.enqueue(new Callback<DiscountModel>() {
            @Override
            public void onResponse(Call<DiscountModel> call, Response<DiscountModel> response) {

                if (response.isSuccessful()) {
                    discountModel = response.body();

                    MESSAGE = discountModel.getMessage();
                    disPrice = discountModel.getPrice();


                    if (Integer.parseInt(disPrice) != 0) {
                        FINAL_PRICEX = FINAL_PRICEX - Integer.parseInt(disPrice);
                        pr = G.decimalNumericx(FINAL_PRICEX);
                        Price.setText(G.farsiNumeric(pr + " تومان"));
                        Toast(G.farsiNumeric("مبلغ " + disPrice + " تومان تخفیف اعمال گردید."));
                        txtDiscount.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast("شما در هر سفارش فقط یک بار می توانید از کد تخفیف استفاده کنید.");
                            }
                        });
                        loading.setVisibility(View.GONE);
                    } else {
                        Toast(MESSAGE);
                        loading.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<DiscountModel> call, Throwable t) {
                loading.setVisibility(View.GONE);
                Toast("خطا در برقرای ارتباط با سرور");
            }
        });
    }

    public void checkError() {



        if (edtState.getText().toString().trim().length() == 0) {

            Toast("لطفاً نام استان را وارد کنید.");

        } else if (edtState.getText().toString().trim().length() > 0 && edtState.getText().toString().trim().length() < 2) {

            Toast("لطفا نام استان را کامل وارد کنید.");

        } else if (edtCity.getText().toString().trim().length() == 0) {

            Toast("لطفاً نام شهر را وارد کنید.");

        } else if (edtCity.getText().toString().trim().length() > 0 && edtCity.getText().toString().trim().length() < 2) {

            Toast("لطفا نام شهر را کامل وارد کنید.");

        } else if (edtAddress.getText().toString().trim().length() == 0) {

            Toast("لطفاً آدرس را وارد کنید.");

        } else if (edtAddress.getText().toString().trim().length() > 0 && edtAddress.getText().toString().trim().length() < 5) {

            Toast("لطفاً آدرس را کامل وارد کنید.");

        }else if (edtName.getText().toString().trim().length() == 0) {

            Toast("لطفاً نام و نام خانوادگی را وارد کنید.");

        } else if (edtName.getText().toString().trim().length() > 0 && edtName.getText().toString().trim().length() < 2) {

            Toast("لطفاً نام و نام خانوادگی را کامل وارد کنید.");

        }else if (edtPostalCode.getText().toString().trim().length() == 0) {

            Toast("لطفاً کد پستی را وارد کنید.");

        } else if (edtPostalCode.getText().toString().trim().length() > 0 && edtPostalCode.getText().toString().trim().length() < 2) {

            Toast("لطفاً کد پستی را کامل وارد کنید.");

        }    else if (edtPhone.getText().toString().trim().length() == 0) {

            Toast("لطفا ًشماره همراه وارد کنید.");

        } else if ((edtPhone.getText().toString().trim().length() >= 1) && (edtPhone.getText().toString().trim().length() < 11)) {

            Toast("لطفاً شماره خود را کامل وارد کنید.");
        }
        else {
            saveData();
        }
    }

    private void saveData() {

        SharedPreferences.Editor editor = G.prefs.edit();
        editor.putString("P_state", edtState.getText().toString().trim());
        editor.putString("P_city", edtCity.getText().toString().trim());
        editor.putString("fullname", edtName.getText().toString().trim());
//        editor.putString("P_family", edtFamily.getText().toString().trim());
        editor.putString("P_address", edtAddress.getText().toString().trim());
        editor.putString("P_postalCode", edtPostalCode.getText().toString().trim());
        editor.putString("mobile", edtPhone.getText().toString().trim());
//        editor.putString("P_phone2", edtPhone2.getText().toString().trim());
        editor.commit();

        isCompleteSignUp = true;

        if (G.prefs.getString("P_state", " ").length() > 0) {
            state.setText(G.prefs.getString("P_state", " "));
        }

        if (G.prefs.getString("P_city", " ").length() > 0) {
            city.setText(G.prefs.getString("P_city", " "));
        }

        if (G.prefs.getString("P_address", " ").length() > 0) {
            address.setText(G.prefs.getString("P_address", " "));
        }

        if (G.prefs.getString("fullname", " ").length() > 0) {
            name.setText(G.prefs.getString("fullname", " "));
        }

        if (G.prefs.getString("P_family", " ").length() > 0) {
            family.setText(G.prefs.getString("P_family", " "));
        }

        if (G.prefs.getString("P_postalCode", " ").length() > 0) {
            postalCode.setText(G.farsiNumeric(G.prefs.getString("P_postalCode", " ")));
        }

        if (G.prefs.getString("mobile", " ").length() > 0) {
            phone.setText(G.farsiNumeric(G.prefs.getString("mobile", " ")));
        }

        if (G.prefs.getString("P_phone2", " ").length() > 0) {
            phone2.setText(G.farsiNumeric(G.prefs.getString("P_phone2", " ")));
        }

        crdEditInfo.setVisibility(View.GONE);
        crdConfirmInfo.setVisibility(View.VISIBLE);

    }


    private void postData() {


//         open(Cong.class , true);

        // congDialog();

        loading.setVisibility(View.VISIBLE);
        btnsubmit.setVisibility(View.GONE);
        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();

//        Call<BuyReqModel> call = apiService.req(G.deviceID
//                , G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd")
//                , getPrefs("fullname"), getPrefs("mobile"), getPrefs("email")
//                , getPrefs("P_state"), getPrefs("P_city"), getPrefs("P_address")
//                , getPrefs("P_postalCode"), getPrefs("P_phone"), getPrefs("P_phone2"), DISCOUNT, IDS, METRAJ, IMAGE_NAMES);
//


        Call<BuyReqModel> call = apiService.req(G.deviceID
                , G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd")
                , getPrefs("fullname"), getPrefs("mobile"), getPrefs("P_state"),
                getPrefs("P_city"), getPrefs("P_address") , getPrefs("P_postalCode")
                , DISCOUNT, IDS, METRAJ, IMAGE_NAMES);


        G.Log("call: " + call.request().url());


        call.enqueue(new Callback<BuyReqModel>() {
            @Override
            public void onResponse(Call<BuyReqModel> call, Response<BuyReqModel> response) {

                buyReqModel = response.body();
                int status = buyReqModel.getStatus();
                String Massage = buyReqModel.getMessage();
                orderID = buyReqModel.getOrderID();
                if (status == 200) {

                    loading.setVisibility(View.GONE);
                    G.myDB.execSQL("DELETE  FROM `ShoppingBag`");
                    try{

                        Intent intent = new Intent(getApplicationContext(), Cong.class);
                        intent.putExtra("basketNum" , buyReqModel.getBasketNum());
                        intent.putExtra("price" , buyReqModel.getBasketNum());
                        startActivity(intent);
                        finish();


                    }catch (Exception e){
                        e.printStackTrace();
                        G.Log("CRASH!");
                    }

//                    loading.setVisibility(View.GONE);
//                    btnsubmit.setVisibility(View.VISIBLE);
//                    String url = "http://onlineparde.ir/api/doPay.php?orderID=" + orderID + "&device=" + G.deviceID + "&code=" + G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd");
//                    Intent i = new Intent(Intent.ACTION_VIEW);
//                    i.setData(Uri.parse(url));
//                    startActivity(i);
                }else {
                    loading.setVisibility(View.GONE);
                    btnsubmit.setVisibility(View.VISIBLE);
                    Toast(G.farsiNumeric(Massage));
                }
            }


            @Override
            public void onFailure(Call<BuyReqModel> call, Throwable t) {
                loading.setVisibility(View.GONE);
                btnsubmit.setVisibility(View.VISIBLE);
                Toast("خطا در برقرای ارتباط با سرور");
            }
        });

    }


    public String getPrefs(String str) {

        String value = G.prefs.getString(str, "");
        return value;

    }


    private void congDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cong);
        TextView mypurchasebtn = (TextView) dialog.findViewById(R.id.mypurchasebtn);
        TextView homebtn = (TextView) dialog.findViewById(R.id.homebtn);
        LinearLayout MainBox2 = (LinearLayout) dialog.findViewById(R.id.MainBox2);


        homebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();

            }
        });


        mypurchasebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                Intent i = new Intent(getApplicationContext(), MyOrders.class);
                startActivity(i);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();

            }
        });


        dialog.setCancelable(false);
        dialog.show();


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(getApplicationContext(), ShoppingBag.class);
        startActivity(i);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
    }
}
