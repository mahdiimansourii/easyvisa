package com.supectco.easyvisa.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;

import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.adapters.PhotoAdapter;
import com.supectco.easyvisa.models.PhotoModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChooseFinalImage extends Master {


    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    private PhotoModel photoModel;
    RelativeLayout loading, error,noData;
    RecyclerView rv;
    private LinearLayoutManager llm;
    PhotoAdapter photoAdapter;
    int mPageNumber=1;
    MyTextView retrybtn;
    Handler handler=new Handler();
    MyTextView retry;
    int ID=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_final_image);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ID = extras.getInt("id");
//            IMAGE = extras.getString("image");
//            G.context = getApplicationContext();

        }

        initializeToolbarx();
        initializeDrawer();
        initializeLayout();
        initializeData(1);
        pageScrollListener();


    }

    private void initializeLayout() {

        rv = (RecyclerView) findViewById(R.id.rv);
        loading = (RelativeLayout) findViewById(R.id.loading);
        error = (RelativeLayout) findViewById(R.id.error);
        noData = (RelativeLayout) findViewById(R.id.noData);
        retrybtn = (MyTextView) findViewById(R.id.retrybtn);
        retry = (MyTextView) findViewById(R.id.retry);

        rv.setHasFixedSize(true);
//        llm = new GridLayoutManager(G.context , 2);
        llm = new LinearLayoutManager(G.context.getApplicationContext());
        rv.setLayoutManager(llm);
        photoAdapter = new PhotoAdapter();
        photoAdapter.clear();
        rv.setAdapter(photoAdapter);


    }


    private void initializeData(final int pageNumber) {


        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        if(pageNumber==1){
            loading.setVisibility(View.VISIBLE);
        }
        Call<PhotoModel> call = apiService.getPhoto(ID , G.deviceID, G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd"), String.valueOf(pageNumber));

        G.Log("url: " + call.request().url());
        call.enqueue(new Callback<PhotoModel>() {
            @Override
            public void onResponse(Call<PhotoModel> call, Response<PhotoModel> response) {
                if(response.body()==null){
                    hasMorePages=false;
                    return;
                }

                photoModel = response.body();
                if (response.body().photoList != null) {
                    for (PhotoModel.PhotoList item : response.body().photoList) {
                        photoAdapter.add(item);
                    }
                    if(pageNumber==1){
                        runLayoutAnimation(rv);
                    }
               ;
                }else {
                    if(pageNumber==1){
                        noData.setVisibility(View.VISIBLE);
                    }
                }
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<PhotoModel> call, Throwable t) {

            }
        });


    }



    boolean hasMorePages=true;
    public boolean hasMorePages(){
        return hasMorePages;
    }

    private void pageScrollListener() {
        rv.addOnScrollListener(new EndlessScrollListener());
    }

    private class EndlessScrollListener extends RecyclerView.OnScrollListener {

        private LinearLayoutManager mLinearLayoutManager;
        private int mCurrentPage;
        private boolean mLoading;
        private int mLastItemsCount;
        private int mVisibleThreshold = 1;

        private EndlessScrollListener() {
            mLoading = true;
            mCurrentPage = 1;
            mLastItemsCount = 0;
        }


        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (mLinearLayoutManager == null) {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null && layoutManager instanceof LinearLayoutManager) {
                    mLinearLayoutManager = (LinearLayoutManager) layoutManager;
                }
            } else {
                if (dy > 0) {
                    if (!hasMorePages()) return;
                    if (mLoading) {
                        if (mLastItemsCount < photoAdapter.getItemCount()) {
                            mLoading = false;
                            mLastItemsCount = photoAdapter.getItemCount();
                        }
                    } else {
                        int totalItemCount = mLinearLayoutManager.getItemCount();
                        int lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();
                        if (totalItemCount <= (lastVisibleItem + mVisibleThreshold)) {
                            mLoading = true;
                            mCurrentPage++;
                            mPageNumber = mCurrentPage;
                            initializeData(mCurrentPage);
                        }
                    }
                }
            }
        }
    }

    private void runLayoutAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }


    @Override
    protected void onResume() {
        super.onResume();
        G.currentActivity = this;
    }
}
