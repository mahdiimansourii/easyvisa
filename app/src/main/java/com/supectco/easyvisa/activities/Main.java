package com.supectco.easyvisa.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stephentuso.welcome.WelcomeHelper;
import com.supectco.easyvisa.R;
import com.supectco.easyvisa.adapters.BestsellerAdapter;
import com.supectco.easyvisa.adapters.ImageSlider_Adapter;
import com.supectco.easyvisa.adapters.MainCatAdapter;
import com.supectco.easyvisa.adapters.NewestAdapter;
import com.supectco.easyvisa.adapters.SpecialAdapter;
import com.supectco.easyvisa.models.MainModel;
import com.supectco.easyvisa.models.Update;
import com.supectco.easyvisa.models.UpdateModel;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main extends Master {

    WelcomeHelper welcomeScreen;
    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    //    String[] slides = new String[5];
    ArrayList<String> slidesArray = new ArrayList<>();
    ImageSlider_Adapter imageSlider_adapter;
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private int currentPage = 1, NUM_PAGES;
    private Timer swipeTimer = null;
    private MainModel mainModel;
    RelativeLayout loading, error;
    Handler handler = new Handler();
    MyTextView retry;
    CardView crdMore;
    Handler handlerx = new Handler();
    LocationManager locationManager;
    double Latitude, Longtitude;
    PackageManager manager;
    PackageInfo info;
    String VERSION_NAME;
    int VERSION_CODE;
    Update userInfoModel;
    boolean validUser, isShowMessage, mandatoryUpdate;
    String message, link, title, buttonText1, buttonText2;
    UpdateModel updateModel;
    String MODEL="" ,OS_VERSION="";
    int MIN_SDK=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        welcomeScreen = new WelcomeHelper(this, Intro.class);
        welcomeScreen.show(savedInstanceState);
        initializeToolbar();
        initializeDrawer();
        initializeLayout();
        initializeData();

        handlerx.postDelayed(new Runnable() {
            @Override
            public void run() {
                update();
            }
        }, 500);


    }


    public void initializeLayout() {


        loading = (RelativeLayout) findViewById(R.id.loading);
        error = (RelativeLayout) findViewById(R.id.error);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        retry = (MyTextView) findViewById(R.id.retry);
        crdMore = (CardView) findViewById(R.id.crdMore);


        crdMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Countries.class);
                startActivity(intent);

            }
        });

    }


    private void initializeData() {

        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        loading.setVisibility(View.VISIBLE);
        slidesArray.clear();

        Call<MainModel> call = apiService.getData(G.deviceID, G.md5(G.deviceID + "ncase8934f49909"));

        G.Log("call:" + call.request().url());

        call.enqueue(new Callback<MainModel>() {
            @Override
            public void onResponse(Call<MainModel> call, final Response<MainModel> response) {
                G.Log(" S request address : " + call.request().toString());
                if (response.isSuccessful()) {
                    G.Log("initilizeData  isSuccessful ");
                    mainModel = response.body();

                    loading.setVisibility(View.GONE);

                    try {
                        for (int i = 0; i < mainModel.getSlides().size(); i++) {
                            slidesArray.add(mainModel.getSlides().get(i).getImage());
                            G.Log(mainModel.getSlides().get(i).getImage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        G.Log("CRASH FROM VIEW PAGER");
                    }
                    viewPager();


                }

            }

            @Override
            public void onFailure(Call<MainModel> call, Throwable t) {
                showError();
            }
        });


    }


    public void viewPager() {

        ArrayList<String> ImagesArraySlide = new ArrayList<>();

        for (int i = 0; i < slidesArray.size(); i++) {
            if (slidesArray.get(i).length() > 5) {
                ImagesArraySlide.add(slidesArray.get(i));
            }
        }

        imageSlider_adapter = new ImageSlider_Adapter(Main.this, ImagesArraySlide);
        viewPager.setAdapter(imageSlider_adapter);
        indicator.setViewPager(viewPager);
        try {
            NUM_PAGES = ImagesArraySlide.size();
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    viewPager.setCurrentItem(currentPage++, true);
                }
            };
            if (swipeTimer == null) {
                swipeTimer = new Timer();
                swipeTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(Update);
                    }
                }, 3000, 3000);
            }
            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;
                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int pos) {
                }
            });
        } catch (Exception e) {
        }

    }


    public void showError() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    error.setVisibility(View.VISIBLE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (G.isInternetAvailable() == false) {
                                showError();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), G.cl);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });
    }





    private void update() {


        try {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            //Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (Latitude == 0 && Longtitude == 0) {
                Latitude = locationNet.getLatitude();
                Longtitude = locationNet.getLongitude();
                G.lat = Latitude;
                G.lng = Longtitude;
            }
            manager = getApplicationContext().getPackageManager();
            info = manager.getPackageInfo(getApplicationContext().getPackageName(), 0);
            VERSION_CODE = info.versionCode;
            VERSION_NAME = info.versionName;
            MIN_SDK = Build.VERSION.SDK_INT ;
            MODEL = Build.MODEL;
            OS_VERSION = Build.VERSION.RELEASE;


        } catch (Exception e) {
            e.printStackTrace();
        }


        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();



        G.Log("G.deviceID: " + G.deviceID);
        G.Log("MODEL: " + MODEL);
        G.Log("MIN_SDK: " + MIN_SDK);
        G.Log("OS_VERSION: " + OS_VERSION);
        G.Log("VERSION_CODE: " + VERSION_CODE);
        G.Log("VERSION_NAME: " + VERSION_NAME);
        G.Log("G.prefs.getString(\"mobile\" , \"\"): " + G.prefs.getString("mobile" , ""));
        G.Log("G.prefs.getString(\"token\" , \"\"): " + G.prefs.getString("token" , ""));
        G.Log("G.lat : " + G.lat );
        G.Log("G.lng : " + G.lng );


        Call<UpdateModel> call = apiService.update(G.deviceID , G.md5(G.deviceID + "ncase8934f49909" ), MODEL , OS_VERSION , MIN_SDK ,VERSION_CODE ,VERSION_NAME,1, G.prefs.getString("mobile" , ""), G.prefs.getString("token" , "")  , G.lat , G.lng );



        call.enqueue(new Callback<UpdateModel>() {
            @Override
            public void onResponse(Call<UpdateModel> call, Response<UpdateModel> response) {

                updateModel = response.body();
                if(updateModel!=null){

                    if(updateModel.isShowDialog()){

                        final Dialog dialog = new Dialog( G.currentActivity);
                        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.messagex);
                        MyTextView title = (MyTextView)dialog.findViewById(R.id.title);
                        MyTextView message = (MyTextView)dialog.findViewById(R.id.message);
                        MyTextView yes = (MyTextView)dialog.findViewById(R.id.yes);
                        MyTextView no = (MyTextView)dialog.findViewById(R.id.no);
                        ImageView image = (ImageView)dialog.findViewById(R.id.image);

                        title.setText(G.farsiNumeric(updateModel.getTitle()));
                        message.setText(G.farsiNumeric(updateModel.getContent()));
                        yes.setText(G.farsiNumeric(updateModel.getPositiveButton()));
                        no.setText(G.farsiNumeric(updateModel.getNegativeButton()));


                        if(updateModel.getImageDialog().length()>5){
                            image.setVisibility(View.VISIBLE);
                            G.loadImage(G.IMG_PATH , updateModel.getImageDialog() , image , false);
                        }else {
                            image.setVisibility(View.GONE);
                        }


                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                dialog.dismiss();

                            }
                        });

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(updateModel.getUrl().length()>5){
                                    String URL = updateModel.getUrl();
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(URL));
                                    startActivity(i);
                                }else {

                                    dialog.dismiss();
                                }

                            }
                        });




                        if(updateModel.isCanDismiss()){
                            dialog.setCancelable(true);
                        }else {
                            dialog.setCancelable(false);
                        }

                        dialog.show();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateModel> call, Throwable t) {

            }
        });
    }






    @Override
    protected void onPause() {
        super.onPause();

        if (swipeTimer != null) {
            swipeTimer.cancel();
            swipeTimer = null;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        G.cl = this.getClass();
        G.currentActivity = this;
        G.context = getApplicationContext();
        if (G.isInternetAvailable() == false) {
            showError();
        }

    }


    private long backPressedTime = 0;

    @Override
    public void onBackPressed() {
        try {
            long t = System.currentTimeMillis();
            if (t - backPressedTime > 2000) {    // 2 secs
                backPressedTime = t;
                Toast("لطفاً برای خارج شدن از برنامه مجدداً دکمه خروج را بزنید.");
            } else {

                super.onBackPressed();
            }
        } catch (Exception e) {

        }


    }


}
