package com.supectco.easyvisa.activities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by PartoDesign04 on 11/13/2017.
 */

public class DottedImageView extends ImageView {

    public static final int imgTypeAll =0;
    public static final int imgTypeOnlyDash =1;
    public static final int imgTypeOnlySolid =2;

    public DottedImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    private Paint dashPaint=new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint solidPaint =new Paint(Paint.ANTI_ALIAS_FLAG);
//    private boolean isDashed = false ;
    private int imageType = 0 ;


    {
        dashPaint.setStyle(Paint.Style.STROKE);
        dashPaint.setColor(Color.GRAY);
        dashPaint.setAlpha(500);
        dashPaint.setPathEffect(new DashPathEffect(new float[]{10, 10
        }, 0f));
        dashPaint.setStrokeWidth(3);
        solidPaint.setColor(Color.WHITE);


    }



    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        int rad=Math.min(canvas.getWidth(), canvas.getHeight())/2;
        rad-=dashPaint.getStrokeWidth();
        if (imageType==imgTypeOnlyDash){
        canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()/2,rad, dashPaint);
        }else if(imageType==imgTypeOnlySolid){
        canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()/2,rad, solidPaint);
        }else {
        canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()/2,rad+dashPaint.getStrokeWidth()*.5f , solidPaint);
        canvas.drawCircle(canvas.getWidth()/2, canvas.getHeight()/2,rad, dashPaint);
        }
    }







    public void setImageType(int type) {
        imageType = type;
        invalidate();
    }

    public void setSolidColor(int color) {
        solidPaint.setColor(color);
        invalidate();
    }


    public void setDottedColor(int color){

        dashPaint.setColor(color);
        invalidate();
    }


    public void setPathEffectDashPaint(float fl1 , float fl2){

        dashPaint.setPathEffect(new DashPathEffect(new float[]{fl1, fl2
        }, 0f));

    }

}
