package com.supectco.easyvisa.activities;

import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.supectco.easyvisa.adapters.ProductListAdapter;
import com.supectco.easyvisa.utils.G;
import com.supectco.easyvisa.views.MyTextView;
import com.supectco.easyvisa.models.ProductListModel;
import com.supectco.easyvisa.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResult extends Master {

    public static RetrofitProvider retrofitProvider;
    public static APIService apiService;
    private ProductListModel productListModel;
    RelativeLayout loading , error , noData;
    RecyclerView rv;
    private LinearLayoutManager llm ;
    ProductListAdapter productListAdapter;
    int page_number = 1,priorityID=0 , sortID=0;;
    String page = "1" , spinnerValue3="", spinnerValue2="", spinnerValue1="" , colorIds="" , typeIds="" , brandIds="" , Q="", QUARY="";
    Spinner spinner3 ,spinner2 ;
    LinearLayout lylFilter;
    private BottomSheetBehavior mBottomSheetBehavior;
    BottomSheetDialogFragment bottomSheetDialogFragment;
    View bottomSheet;
    int POSITION=0;
    boolean isFirstRun=false;
    MyTextView Quary , retrybtn;
    int mPageNumber=1;
    String str1="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result);

        Bundle extrass = getIntent().getExtras();
        if(extrass!=null){
            Q = extrass.getString("q");
        }

        initializeToolbarx();
        initializeLayout();
        initializeData(1);
        pageScrollListener();


    }




    public void initializeLayout(){

        rv = (RecyclerView) findViewById(R.id.rv);
        loading = (RelativeLayout) findViewById(R.id.loading);
        error = (RelativeLayout) findViewById(R.id.errorx);
        noData = (RelativeLayout) findViewById(R.id.noData);
        Quary = (MyTextView) findViewById(R.id.Quary);
        retrybtn = (MyTextView) findViewById(R.id.retrybtn);


        rv.setHasFixedSize(true);
//        llm = new LinearLayoutManager(G.context.getApplicationContext());
        llm = new GridLayoutManager(G.context , 2);
        rv.setLayoutManager(llm);
        productListAdapter = new ProductListAdapter();
        rv.setAdapter(productListAdapter);

        if(Q!=null && Q.length()>2){

            Quary.setText(G.farsiNumeric("نتایج جستجو برای : " + Q));
        }


        retrybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                open(SearchActivity.class , true);

            }
        });

    }


    private void initializeData(final int pageNumber) {


        colorIds="";
        typeIds="";
        brandIds="";

        for (int i = 0; i < G.colorIDs.size(); i++) {
            colorIds = colorIds +","+ G.colorIDs.get(i);
        }

        for (int i = 0; i < G.typeIDs.size(); i++) {
            typeIds = typeIds + "," + G.typeIDs.get(i);
        }

        for (int i = 0; i < G.brandIDs.size(); i++) {
            brandIds = brandIds + "," + G.brandIDs.get(i);
        }


        retrofitProvider = new RetrofitProvider();
        apiService = retrofitProvider.getAPIService();
        if(pageNumber==1){
            loading.setVisibility(View.VISIBLE);
        }
        Call<ProductListModel> call = apiService.getDataProduct(G.deviceID, G.md5(G.deviceID + "lkdfvmovm4958v49m94j98jldkfvmd"), String.valueOf(pageNumber), colorIds, typeIds, brandIds, sortID, priorityID, 0, 0,  Q);

        call.enqueue(new Callback<ProductListModel>() {
            @Override
            public void onResponse(Call<ProductListModel> call, Response<ProductListModel> response) {

                if(response.body()==null){
                    hasMorePages=false;
                    return;
                }

                productListModel = response.body();
//                productListAdapter.clear();
                if (response.body().productLists != null) {
                    for (ProductListModel.ProductList item : response.body().productLists) {
                        productListAdapter.add(item);
                    }
                }else {

                    if(pageNumber==1){
                    noData.setVisibility(View.VISIBLE);
                    }

                }
                isFirstRun=false;
                loading.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<ProductListModel> call, Throwable t) {

            }
        });

    }




    boolean hasMorePages=true;
    public boolean hasMorePages(){
        return hasMorePages;
    }


    private class EndlessScrollListener extends RecyclerView.OnScrollListener {

        private LinearLayoutManager mLinearLayoutManager;
        private int mCurrentPage;
        private boolean mLoading;
        private int mLastItemsCount;
        private int mVisibleThreshold = 1;

        private EndlessScrollListener() {
            mLoading = true;
            mCurrentPage = 1;
            mLastItemsCount = 0;
        }


        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (mLinearLayoutManager == null) {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null && layoutManager instanceof LinearLayoutManager) {
                    mLinearLayoutManager = (LinearLayoutManager) layoutManager;
                }
            } else {
                if (dy > 0) {
                    if (!hasMorePages()) return;
                    if (mLoading) {
                        if (mLastItemsCount < productListAdapter.getItemCount()) {
                            mLoading = false;
                            mLastItemsCount = productListAdapter.getItemCount();
                        }
                    } else {
                        int totalItemCount = mLinearLayoutManager.getItemCount();
                        int lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();
                        if (totalItemCount <= (lastVisibleItem + mVisibleThreshold)) {
                            mLoading = true;
                            mCurrentPage++;
                            mPageNumber = mCurrentPage;
                            initializeData(mCurrentPage);
                        }
                    }
                }
            }
        }
    }

    private void pageScrollListener() {

        rv.addOnScrollListener(new EndlessScrollListener());
    }



    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onResume() {
        super.onResume();
        G.currentActivity=this;
    }
}
