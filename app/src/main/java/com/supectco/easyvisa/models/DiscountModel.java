package com.supectco.easyvisa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 108 on 4/12/2017.
 */

public class DiscountModel {


    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("message")
    @Expose
    private String message;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}





