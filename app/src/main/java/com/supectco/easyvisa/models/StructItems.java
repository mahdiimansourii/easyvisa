package com.supectco.easyvisa.models;

/**
 * Created by Administrator on 10/15/2015.
 */
public class StructItems {
    public String title,weekday,hour,date,WeekDaysID,uri;
    public int ID,reminderID,type;

    public StructItems(int ID, String title, String weekday, String WeekDaysID, String hour, String date, int reminderID, int type , String uri) {
        this.ID = ID;
        this.title = title;
        this.weekday = weekday;
        this.hour = hour;
        this.date = date;
        this.reminderID = reminderID;
        this.type = type;
        this.WeekDaysID = WeekDaysID;
        this.uri = uri;
    }
}
