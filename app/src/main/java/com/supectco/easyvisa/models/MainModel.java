package com.supectco.easyvisa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 108 on 4/12/2017.
 */

public class MainModel {




    @SerializedName("banner1")
    @Expose
    private String banner1;


    @SerializedName("banner2")
    @Expose
    private String banner2;

    @SerializedName("slides")
    @Expose
    private List<Slide> slides = null;


    @SerializedName("newest")
    @Expose
    public List<Newest> newest = new ArrayList<>();


    @SerializedName("bestseller")
    @Expose
    public List<Bestseller> bestseller = new ArrayList<>();


    @SerializedName("specialOffers")
    @Expose
    public List<SpecialOffer> specialOffers = new ArrayList<>();


    @SerializedName("cats")
    @Expose
    public List<Cats> cats = new ArrayList<>();



    public List<Slide> getSlides() {
        return slides;
    }

    public void setSlides(List<Slide> slides) {
        this.slides = slides;
    }

    public List<Newest> getNewest() {
        return newest;
    }

    public void setNewest(List<Newest> newest) {
        this.newest = newest;
    }

    public List<Bestseller> getBestseller() {
        return bestseller;
    }

    public void setBestseller(List<Bestseller> bestseller) {
        this.bestseller = bestseller;
    }

    public List<SpecialOffer> getSpecialOffers() {
        return specialOffers;
    }

    public void setSpecialOffers(List<SpecialOffer> specialOffers) {
        this.specialOffers = specialOffers;
    }


    public String getBanner1() {
        return banner1;
    }

    public void setBanner1(String banner1) {
        this.banner1 = banner1;
    }

    public String getBanner2() {
        return banner2;
    }

    public void setBanner2(String banner2) {
        this.banner2 = banner2;
    }


    public static class SpecialOffer {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("oldPrice")
        @Expose
        private String oldPrice;
        @SerializedName("price")
        @Expose
        private String price;

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getOldPrice() {
            return oldPrice;
        }

        public void setOldPrice(String oldPrice) {
            this.oldPrice = oldPrice;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

    }



    public static class Cats {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("catLevel")
        @Expose
        private int catLevel;
        @SerializedName("isFinal")
        @Expose
        private int isFinal;


        public Integer getiD() {
            return iD;
        }

        public void setiD(Integer iD) {
            this.iD = iD;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getCatLevel() {
            return catLevel;
        }

        public void setCatLevel(int catLevel) {
            this.catLevel = catLevel;
        }

        public int getIsFinal() {
            return isFinal;
        }

        public void setIsFinal(int isFinal) {
            this.isFinal = isFinal;
        }




    }







    public static class Bestseller {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("oldPrice")
        @Expose
        private String oldPrice;
        @SerializedName("price")
        @Expose
        private String price;

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getOldPrice() {
            return oldPrice;
        }

        public void setOldPrice(String oldPrice) {
            this.oldPrice = oldPrice;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

    }




    public static class Newest {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("oldPrice")
        @Expose
        private String oldPrice;
        @SerializedName("price")
        @Expose
        private String price;

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getOldPrice() {
            return oldPrice;
        }

        public void setOldPrice(String oldPrice) {
            this.oldPrice = oldPrice;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

    }



    public static class Slide {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("image")
        @Expose
        private String image;

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

    }


}
