package com.supectco.easyvisa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 108 on 4/12/2017.
 */

public class UpdateModel {


    @SerializedName("showDialog")
    @Expose
    private boolean showDialog;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("imageDialog")
    @Expose
    private String imageDialog;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("positiveButton")
    @Expose
    private String positiveButton;
    @SerializedName("negativeButton")
    @Expose
    private String negativeButton;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("canDismiss")
    @Expose
    private boolean canDismiss;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageDialog() {
        return imageDialog;
    }

    public void setImageDialog(String imageDialog) {
        this.imageDialog = imageDialog;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPositiveButton() {
        return positiveButton;
    }

    public void setPositiveButton(String positiveButton) {
        this.positiveButton = positiveButton;
    }

    public String getNegativeButton() {
        return negativeButton;
    }

    public void setNegativeButton(String negativeButton) {
        this.negativeButton = negativeButton;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isCanDismiss() {
        return canDismiss;
    }

    public void setCanDismiss(boolean canDismiss) {
        this.canDismiss = canDismiss;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }


}
