package com.supectco.easyvisa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 108 on 4/12/2017.
 */

public class ProductListModel {



    @SerializedName("products")
    @Expose
    public List<ProductList> productLists = new ArrayList<>();



    public List<ProductList> getBestseller() {
        return productLists;
    }

    public void setBestseller(List<ProductList> bestseller) {
        this.productLists = bestseller;
    }





    public static class ProductList {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("desc")
        @Expose
        private String desc;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("oldPrice")
        @Expose
        private String oldPrice;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("color")
        @Expose
        private String color;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("brand")
        @Expose
        private String brand;

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getOldPrice() {
            return oldPrice;
        }

        public void setOldPrice(String oldPrice) {
            this.oldPrice = oldPrice;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

    }

}
