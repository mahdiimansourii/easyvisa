package com.supectco.easyvisa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 108 on 4/12/2017.
 */

public class FilterModel {


    @SerializedName("types")
    @Expose
    private List<Type> types = null;
    @SerializedName("brands")
    @Expose
    private List<Brand> brands = null;
    @SerializedName("colors")
    @Expose
    private List<Color> colors = null;

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public List<Color> getColors() {
        return colors;
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
    }



    public class Brand {

        @SerializedName("ID")
        @Expose
        private String iD;
        @SerializedName("title")
        @Expose
        private String title;

        public String getID() {
            return iD;
        }

        public void setID(String iD) {
            this.iD = iD;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }


    public class Color {

        @SerializedName("ID")
        @Expose
        private String iD;
        @SerializedName("title")
        @Expose
        private String title;

        public String getID() {
            return iD;
        }

        public void setID(String iD) {
            this.iD = iD;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }

    public class Type {

        @SerializedName("ID")
        @Expose
        private String iD;
        @SerializedName("title")
        @Expose
        private String title;

        public String getID() {
            return iD;
        }

        public void setID(String iD) {
            this.iD = iD;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }
}
