package com.supectco.easyvisa.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by PartoDesign04 on 2/8/2017.
 */

public class BuyRequest {


    public boolean isConfirm() {
        return isConfirm;
    }

    public void setConfirm(boolean confirm) {
        isConfirm = confirm;
    }

    @SerializedName("isConfirm")
    private boolean isConfirm;


}
