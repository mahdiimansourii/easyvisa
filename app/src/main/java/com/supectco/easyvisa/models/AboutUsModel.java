package com.supectco.easyvisa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 108 on 4/12/2017.
 */

public class AboutUsModel {



    @SerializedName("content")
    @Expose
    private String content;

    @SerializedName("message")
    @Expose
    private String message;


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}