package com.supectco.easyvisa.models;

/**
 * Created by Administrator on 10/15/2015.
 */
public class Item_category {
    public int ID;
    public String title;
    public int image;
    public int codeTime;


    public Item_category(int ID, String title, int image, int codeTime) {
        this.ID = ID;
        this.title = title;
        this.image = image;
        this.codeTime = codeTime;
    }
}
