package com.supectco.easyvisa.models;

/**
 * Created by 108 on 4/12/2017.
 */

public class SlidesModel {
    private int ID;
    private String image;
    private String title;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
