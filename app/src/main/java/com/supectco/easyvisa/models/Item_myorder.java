package com.supectco.easyvisa.models;

/**
 * Created by Administrator on 10/15/2015.
 */

public class Item_myorder {
    public int ID;
    public String state;
    public String city;
    public String address;
    public String price;
    public String date;
    public int isPayed;

     Item_myorder(int ID, String state, String city, String address, String price, String date, int isPayed) {
        this.ID = ID;
        this.state = state;
        this.city = city;
        this.address = address;
        this.price = price;
        this.date = date;
        this.isPayed = isPayed;

    }
}
