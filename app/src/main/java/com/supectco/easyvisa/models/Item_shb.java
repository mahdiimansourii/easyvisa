package com.supectco.easyvisa.models;

/**
 * Created by Administrator on 10/15/2015.
 */
public class Item_shb {
    public int ID;
    public int productID;
    public String title;
    public String desc;
    public String image;
    public String price;
    public String metraj;
    public String color;

    public Item_shb(int ID, int productID, String title, String desc, String image, String metraj, String price, String color) {
        this.ID = ID;
        this.productID = productID;
        this.title = title;
        this.desc = desc;
        this.image = image;
        this.price = price;
        this.metraj = metraj;
        this.color = color;
    }
}
