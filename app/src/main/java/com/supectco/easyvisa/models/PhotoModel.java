package com.supectco.easyvisa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 108 on 4/12/2017.
 */

public class PhotoModel {

    @SerializedName("photoList")
    @Expose
    public List<PhotoList> photoList = new ArrayList<>();

    public List<PhotoList> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<PhotoList> photoList) {
        this.photoList = photoList;
    }

    public class PhotoList {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("title")
        @Expose
        private Object title;

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        @SerializedName("color")
        @Expose
        private String color;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Object getTitle() {
            return title;
        }

        public void setTitle(Object title) {
            this.title = title;
        }
    }

}
