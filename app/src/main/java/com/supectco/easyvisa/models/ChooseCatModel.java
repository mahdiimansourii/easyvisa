package com.supectco.easyvisa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 108 on 4/12/2017.
 */

public class ChooseCatModel {


    @SerializedName("cats")
    @Expose
    private List<Cat> cats = new ArrayList<>();

    public List<Cat> getCats() {
        return cats;
    }

    public void setCats(List<Cat> cats) {
        this.cats = cats;
    }


    public class Cat {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("catLevel")
        @Expose
        private Integer catLevel;
        @SerializedName("isFinal")
        @Expose
        private Integer isFinal;

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getCatLevel() {
            return catLevel;
        }

        public void setCatLevel(Integer catLevel) {
            this.catLevel = catLevel;
        }

        public Integer getIsFinal() {
            return isFinal;
        }

        public void setIsFinal(Integer isFinal) {
            this.isFinal = isFinal;
        }

    }

}
