package com.supectco.easyvisa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 108 on 4/12/2017.
 */

public class BuyReqModel {

    @SerializedName("status")
    @Expose
    private Integer status;


    @SerializedName("orderID")
    @Expose
    private Integer orderID;


    @SerializedName("message")
    @Expose
    private String message;


    @SerializedName("basketNum")
    @Expose
    private String basketNum;



    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }



    public String getBasketNum() {
        return basketNum;
    }

    public void setBasketNum(String basketNum) {
        this.basketNum = basketNum;
    }

}